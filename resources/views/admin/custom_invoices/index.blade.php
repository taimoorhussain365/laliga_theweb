@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Custom Invoice For Player: {{$player->name}}
<br>Term:{{$term->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="text-right">
            <a href="" class="btn --btn-small bg-secondary fc-white mb-0">Back</a>
        </div>
    </div>
    <div class="box-body">
        {{ Form::open(['route'=>['admin.player.term.custom.invoice.store',$player->id,$term->id],'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) }}
        <div class="row">
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('subject',null, ['class'=>'form-field','placeholder' => 'Enter Subject']) !!}
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="use_wallet" id="use_wallet">
                    <span class="label"></span>
                    <span class="text">Use Wallet</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('amount',null, ['class'=>'form-field','placeholder' => 'Enter Amount','required'=>'required','autocomplete'=>'off']) !!}
                    @if ($errors->has('amount'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="checkbox">
                    <input type="checkbox" name="add_vat" id="add_vat">
                    <span class="label"></span>
                    <span class="text">Add VAT (5%)</span>
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('start_date',null, ['class'=>'form-field','placeholder' => 'Enter Invoice Date','id'=>'start_date','required'=>'required','autocomplete'=>'off']) !!}
                </div>
                
            </div>
        </div>
        <button type="submit" class="btn --btn-small bg-secondary fc-white">Generate Invoice</button>
        {!! Form::close() !!}
        <table class="table --bordered --hover">
            <tr>
                <th>Invoice No</th>
                <th>Player Details</th>
                <th>Training Details</th>
                <th>Payment Details</th>
                <th>Action</th>
            </tr>
            
            @foreach ($invoices as $invoice)
            @php $details=json_decode($invoice->session_details,true);@endphp
            
            <tr>
                <td>
                    <strong>LA-{{$invoice->invoice_no}}</strong>
                </td>
                
                <td>
                    @if($invoice->player_id==null)
                    @foreach($details as $key => $detail)
                    @php $player=$invoice->getPlayer($detail['player_id']); @endphp
                    <strong>PL: </strong>{{$player->id}}<br />
                    <strong>Name: </strong> {{$player->name}}<br />
                    @endforeach
                    @else
                    <strong>PL: </strong>{{$invoice->player->id}}<br />
                    <strong>Name: </strong> {{$invoice->player->name}}<br />
                    @endif
                </td>
                
                <td>
                    @if($invoice->term_id==null)
                    @foreach($details as $key => $detail)
                    @php $term=$invoice->getTerm($detail['term_id']); @endphp
                    {{$term->name}}
                    @endforeach
                    @else
                    {{$invoice->term->name}}<br/>
                    @endif
                </td>
                
                <td>
                   
                     <strong>Tax: </strong>{{$invoice->tax_amount}}<br />
                    @if(empty($invoice->total_amount))
                    <strong>Total Amount: </strong>{{$invoice->amount}}<br />
                    @else
                    <strong>Total Amount: </strong>{{$invoice->total_amount}}<br />
                    @endif
                    @if($invoice->payment_status==STATUS_PAID)
                    <strong>Status: </strong>PAID
                    @elseif($invoice->payment_status==STATUS_READY_TO_PAY)
                    <strong>Status: </strong>Ready to Pay
                    @elseif($invoice->payment_status==STATUS_PENDING)
                    <strong>Status: </strong>Pending
                    @endif
                </td>
                
                <td>
                    <a target="_blank" href="{{route('admin.player.custom.invoices.show',[$player->id,$term->id,$invoice->id])}}">
                        <button class="btn --btn-small bg-secondary fc-white btn-sm"
                        style="width: 150px;">Preview Invoice</button>
                    </a><br>
                    <a href="{{route('admin.invoices.send',[$player->id,$term->id,$invoice->id])}}">
                        <button class="btn --btn-small bg-primary fc-white btn-sm" style="width: 150px;">Send Invoice On Email</button>
                    </a><br>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $invoice->id }}" id="deleteItem" style="width: 150px;">Delete</button>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</section>
<script>
var dateToday = new Date();

$( "#start_date" ).datepicker({
minDate: dateToday,
});
</script>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection