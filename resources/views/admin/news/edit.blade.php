@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit New</h3>
<section class="box">
  <div class="box-body">
    {!! Form::model($news, ['route' => ['admin.news.update', $news->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.news._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
  </div>
</section>
@endsection