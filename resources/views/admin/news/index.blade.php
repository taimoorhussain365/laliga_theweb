@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">News</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
        <a href="{{route('admin.news.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add News</button></a>
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
       <tr>
          <th><center>S NO</center></th>
          <th><center>Title</center></th>
          <th><center>Session</center></th>
          <th><center>Status</center></th>
          <th><center>Action</center></th>
        </tr>
        @foreach ($news as $new)
        <tr>
          <td><center>{{ $no++ }}</center></td>
          <td><center>{{ $new->name_eng }}</center></td>
          <td><center>@foreach($sessions as $key => $session) 
           @if($new->session_id==$key){{ $session }} 
           @endif @endforeach</center></td>
          <td><center>
            @if($new->is_active)
            <p>Active</p>
            @else
            <p>InActive</p>
          @endif</center></td>
          <td><center><a href="{{ route('admin.news.edit',[$new->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $new->id }}" id="deleteItem">Delete</button></center></td>
      </tr>
      @endforeach
    </table>
    <ul class="pagination">
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-left"></i>
        </a>
      </li>
      <li>
        <a href="#">1</a>
      </li>
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-right"></i>
        </a>
      </li>
    </ul>
  </div>
  </section>
  {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url()->current() }}';
  $(document).ready(function () {
  
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
  @endsection