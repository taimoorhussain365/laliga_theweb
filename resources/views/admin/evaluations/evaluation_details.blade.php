@php
$playerPositions = [
"Goal Keeper",
"Right Back",
"Left Back",
"Central Back 1",
"Central Back 2",
"Midfielder 1",
"Midfielder 2",
"Midfielder 3",
"Left Winger",
"Right Winger",
"Striker",
];
$evaluation_id = 0;
if(isset($evaluation)){
$evaluation_id = $evaluation->id;
}
@endphp
@if(Auth::user()->type == USER_TYPE_ADMIN)
@php
$user_type = 'admin';
@endphp
@else
@php
$user_type = 'coach';
@endphp
@endif
@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Players</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="box-body">
            {!! Form::open(['route' => [$user_type.'.player.evaluations.store', $player->id, $evaluation_id], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"player-report-form"]) !!}
            <div class="row">
                <div class="col-md-3">
                
                    <span class="heading-title">Keys:</span>
                    <ul class="key-field">
                        <li>1 - Needs Improvement</li>
                        <li>2 - Average</li>
                        <li>3 - Above Average</li>
                        <li>4 - Good</li>
                        <li>5 - Excellent</li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <div class="top-fields">
                        <div class="control-group">
                            {!! Form::label('player', 'Player', ['class'=>'control-label']) !!}
                            {!! Form::text('player', $player->name, ['class'=>'top-field inputTextBox','readonly' => 'readonly']) !!}
                        </div>
                        <div class="control-group">
                            {!! Form::label('coach_id', 'Coach Name', ['class' => 'control-label']) !!}
                            <select class="top-field" name="coach_id"
                                required>
                                @foreach($coaches as $coach)
                                <option value="{{$coach->id}}" {{$coach_id==$coach->id ? 'selected':''}}>{{$coach->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="control-group">
                            {!! Form::label('category_id', 'Age Category', ['class'=>'control-label']) !!}
                            {{--{!! Form::text('category_id', $player->categories->name , ['class'=>'top-field inputTextBox','readonly' => 'readonly']) !!}--}}
                            {!! Form::select('category_id', $age_categories, isset($evaluation) ? $evaluation->age_category : null, ['class'=>'top-field', 'required' => 'required']) !!}
                        </div>
                        <div class="control-group">
                            {!! Form::label('location_id', 'Location', ['class'=>'control-label']) !!}
                            {{--{!! Form::text('location_id', $package->locations->name , ['class'=>'top-field inputTextBox','readonly' => 'readonly']) !!}--}}
                            {!! Form::select('location_id', $locations, isset($evaluation) ? $evaluation->location_id : null, ['class'=>'top-field', 'required' => 'required']) !!}
                        </div>
                        <div class="control-group">
                            {!! Form::label('term_id', 'Term', ['class'=>'control-label']) !!}
                            {{--{!! Form::text('term_id', $terms, ['class'=>'top-field inputTextBox','readonly' => 'readonly']) !!}--}}
                            {!! Form::select('term_id', $terms, isset($evaluation) ? $evaluation->term_id : null, ['class'=>'top-field', 'required' => 'required']) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <figure class="laliga-logo">
                        <img src="{{ asset('/assets-web/images/laliga-logo.png') }}"  alt="Laliga Logo">
                    </figure>
                </div>
            </div>
            <div class="row bottom-fields">
                <div class="col-md-9">
                    @foreach($categories as $cat_key => $cat_value)
                    <div class="bottom-fields evaluation{{$cat_key}}">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- Field One -->
                                <span class="heading-title">{{ $cat_value['label']}}</span>
                            </div>
                            <div class="col-md-6">
                                <div class="average-box">
                                    <div class="item">
                                        <span>1</span>
                                    </div>
                                    <div class="item">
                                        <span>2</span>
                                    </div>
                                    <div class="item">
                                        <span>3</span>
                                    </div>
                                    <div class="item">
                                        <span>4</span>
                                    </div>
                                    <div class="item">
                                        <span>5</span>
                                    </div>
                                    <div class="progress-bar"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @php
                            $skill_count = 0;
                            @endphp
                            @foreach($cat_value['sub_categories'] as $key => $value)
                            @php
                            $skill_count++;
                            @endphp
                            <div class="col-md-6">
                                <label class="detail-field">{{$value['label']}}</label>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox-fields">
                                    <div class="item">
                                        <label>
                                            @if($value['score'] == 1)
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 1, true )}}
                                            @else
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 1 )}}
                                            @endif
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <label>
                                            @if($value['score'] == 2)
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 2, true )}}
                                            @else
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 2 )}}
                                            @endif
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <label>
                                            @if($value['score'] == 3)
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 3, true )}}
                                            @else
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 3 )}}
                                            @endif
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <label>
                                            @if($value['score'] == 4)
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 4, true )}}
                                            @else
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 4 )}}
                                            @endif
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="item">
                                        <label>
                                            @if($value['score'] == 5)
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 5, true )}}
                                            @else
                                            {{ Form::radio('result['.$cat_key.']['.$key.']', 5 )}}
                                            @endif
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <script>
                    $(document).ready(function () {
                    <?php $evaluationClass = ".evaluation".$cat_key; ?>
                    $("<?php echo $evaluationClass; ?> .checkbox-fields input[type=radio]").click(function () {
                    generateProgressBar($('<?php echo $evaluationClass; ?>'), {{ $skill_count }});
                    });
                    generateProgressBar($('<?php echo $evaluationClass; ?>'), {{ $skill_count }});
                    });
                    </script>
                    @endforeach
                </div>
                <div class="col-md-3">
                    <figure class="football-field">
                        <img src="{{ asset('/assets-web/images/football-field.jpg') }}" alt="">
                        <span id="pos_goal-keeper" class="goal-keeper active"></span>
                        <span id="pos_right-back" class="right-back"></span>
                        <span id="pos_left-back" class="left-back"></span>
                        <span id="pos_central-back-1" class="central-back-1"></span>
                        <span id="pos_central-back-2" class="central-back-2"></span>
                        <span id="pos_midfielder-1" class="midfielder-1"></span>
                        <span id="pos_midfielder-2" class="midfielder-2"></span>
                        <span id="pos_midfielder-3" class="midfielder-3"></span>
                        <span id="pos_left-winger" class="left-winger"></span>
                        <span id="pos_right-winger" class="right-winger"></span>
                        <span id="pos_striker" class="striker"></span>
                    </figure>
                    <div class="field-places">
                        {!! Form::label('position_1', 'Position 1', ['class'=>'']) !!}
                        <select class="top-field field-positions" name="position_1" required>
                            @foreach ($playerPositions as $position)
                            <option value="{{ $position }}"
                                @if(isset($evaluation) && $evaluation->position_1 == $position)
                                selected="selected"
                                @endif
                            id="<?php echo 'pos_' . str_replace(' ', '-', strtolower($position)); ?>"><?php echo $position ?></option>
                            @endforeach
                        </select>
                        @if ($errors->has('position_1'))
                        <span class="error-msg">
                            <strong>{{ $errors->first('position_1') }}</strong>
                        </span>
                        @endif
                    </div>
                    <br>
                    <div class="field-places">
                        {!! Form::label('position_2', 'Position 2', ['class'=>'']) !!}
                        <select class="top-field field-positions" name="position_2" required>
                            @foreach ($playerPositions as $position)
                            <option value="{{ $position }}"
                                @if(isset($evaluation) && $evaluation->position_2 == $position)
                                selected="selected"
                                @endif
                            id="<?php echo 'pos_' . str_replace(' ', '-', strtolower($position)); ?>"><?php echo $position ?></option>
                            @endforeach
                        </select>
                        @if ($errors->has('position_2'))
                        <span class="error-msg">
                            <strong>{{ $errors->first('position_2') }}</strong>
                        </span>
                        @endif
                    </div>
                    <figure class="laliga-academy-logo">
                        <img src="{{ asset('/assets-web/images/laliga-academy-logo.png') }}"  alt="Laliga Academy Logo">
                    </figure>
                </div>
            </div>
            <div class="comment-area">
                {!! Form::label('observation', 'Observation', ['class'=>'title']) !!}
                {!! Form::textarea('observation', isset($evaluation) ? $evaluation->observation : null, ['class'=>'top-field','required' => 'required', 'rows' => 4]) !!}
                @if ($errors->has('observation'))
                <span class="error-msg">
                    <strong>{{ $errors->first('observation') }}</strong>
                </span>
                @endif
            </div>
            <div class="form-group">
                <div class="control-group{{ $errors->has('is_final') ? ' has-error' : '' }}">
                    {!! Form::checkbox('is_final', 1, isset($evaluation) ? $evaluation->is_final : false) !!}
                    {!! Form::label('is_final', 'Checked it, if its the final evaluation ( You will not be able to edit it ).', ['class' => 'control-label']) !!}
                    @if ($errors->has('is_final'))
                    <span class="error-msg">
                        <strong>{{ $errors->first('is_final') }}</strong>
                    </span>
                    @endif
                </div>
            </div>
            <center>
            <button type="submit" class="btn btn-primary" style="width: 400px;">Submit Evaluation
            </button>
            </center>
            {!! Form::hidden('player_id',$player->id) !!}
            {{--{!! Form::hidden('package_id',$package->id) !!}--}}
            {!! Form::close() !!}
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
@section('footer-scripts')
<script>
//*****************************
// Field Places Function
//*****************************
$('select.field-positions').change(function () {
updateMapPosition();
});
function updateMapPosition() {
$('.football-field span').removeClass('active');
$('select.field-positions').each(function () {
console.log($(this).val().replace(/\s+/g, '-').toLowerCase());
$('#pos_' + $(this).val().replace(/\s+/g, '-').toLowerCase()).addClass('active');
});
}
$(document).ready(function () {
updateMapPosition();
});
function generateProgressBar($cssClass, totalCount) {
var total = 0;
$cssClass.find(".checkbox-fields input[type=radio]:checked").each(function () {
total += parseFloat($(this).val());
});
var average = total / totalCount;
var progressval = average / 0.05;
$cssClass.find('.progress-bar').css("width", progressval + '%');
if (parseInt(progressval) <= 20) {
$cssClass.find('.progress-bar').css("background-color", "#ed2024");
} else if (parseInt(progressval) <= 40) {
$cssClass.find('.progress-bar').css("background-color", "#f57e20");
} else if (parseInt(progressval) <= 60) {
$cssClass.find('.progress-bar').css("background-color", "#942683");
} else if (parseInt(progressval) <= 80) {
$cssClass.find('.progress-bar').css("background-color", "#4687c7");
} else {
$cssClass.find('.progress-bar').css("background-color", "#1dad4b");
}
}
</script>
@endsection