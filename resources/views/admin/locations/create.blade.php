@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Add Location</h3>

<section class="box">

  <div class="box-body">

    <!-- /.box-header -->

    {!! Form::open(['route' => 'admin.locations.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.locations._form',['submitBtnText' => 'Add Location'])

    {!! Form::close() !!}

  </div>

</section>

@endsection