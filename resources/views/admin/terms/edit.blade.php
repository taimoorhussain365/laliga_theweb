@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Term</h3>
<section class="box">
	<section class="box">
		<div class="box-header">
			<div class="row align-items-center">
				<div class="col-8">
				</div>
				
				<div class="col-4">
					<a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
				</div>
			</div>
		</div>
		<div class="box-body">
			{!! Form::model($term, ['route' => ['admin.season.terms.update', $term->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
			@include('admin.terms._form',['submitBtnText' => 'Update'])
			{!! Form::close() !!}
		</div>
	</section>
	@endsection