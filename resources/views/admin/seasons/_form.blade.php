<div class="box-body">
    
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::label('season_name', 'Season Name', ['class'=>'control-label']) !!}
        </div>
        <div class="col-sm-9">
            {!! Form::text('name', null, ['class'=>'form-field','required' => 'required','maxlength'=>'100','placeholder'=>'Name']) !!}
        </div>
        @if ($errors->has('name'))
        <span class="error-msg">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
        <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
            <div class="col-sm-12">
                {!! Form::checkbox('is_active', 1, isset($season) ? $season->is_active : true) !!}
                {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
                @if ($errors->has('is_active'))
                <span class="error-msg">
                    <strong>{{ $errors->first('is_active') }}</strong>
                </span>
                @endif
            </div>
        </div>
    
    <div class="control-group">
        <center>
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
        </center>
    </div>
</div>