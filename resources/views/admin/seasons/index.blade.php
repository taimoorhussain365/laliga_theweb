@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Seasons</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
        <a href="{{route('admin.seasons.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add Season</button></a>
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    
    <table class="table --bordered --hover">
      
      <tr>
        <th><center>S NO</center></th>
        <th><center>Season</center></th>
        <th><center>Terms</center></th>
        <th><center>Is Active</center></th>
        <th><center>Action</center></th>
      </tr>
      <tr>
        
        <tr>
          @foreach ($seasons as $key=>$season)
          <td><center>{{ $seasons->firstItem() + $key }}</center></td>
          <td><center>{{ $season->name }}</center></td>
          <td><center><a href="{{ route('admin.season.terms.index',[$season->id]) }}" class="btn --btn-small --btn-primary mb-0">Terms</a></center></td>
          <td><center>
          @if($season->is_active)
          <p>Active</p>
          @else
          <p>InActive</p>
          @endif</center></td>
          <td><center><a href="{{ route('admin.seasons.edit',[$season->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white"> Edit</button></a>
        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $season->id }}" id="deleteItem">Delete</button></center></td></td>
        
      </tr>
      @endforeach
      <tr>
        
      </tr>
    </table>
     <div class="">
    {{$seasons->links()}}
    </div>
  </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection