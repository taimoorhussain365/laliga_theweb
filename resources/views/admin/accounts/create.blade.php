@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add User Account</h3>
<section class="box">
  <div class="box-body">
    {!! Form::open(['route' => 'admin.accounts.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.accounts._form',['submitBtnText' => 'Add Account'])
    {!! Form::close() !!}
  </div>
</section>
@endsection