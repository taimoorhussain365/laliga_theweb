@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">User Accounts</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
                <h4 class="box-title">
               <a href="{{route('admin.accounts.create')}}" class="btn --btn-small bg-primary fc-white">Add Account</a>
                </h4>
            </div>
            
            <div class="col-4">
                <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>S NO</th>
                <th>User Name</th>
                <th>Details</th>
                <th>User Type</th>
                <th>Action</th>
            </tr>
            <tr>
                
                @foreach ($users as $key=>$user)
                
                <td>{{ $users->firstItem() + $key }}</td>
                <td>{{ $user->name }}</td>
                
                <td>
                    <strong>Email:</strong> {{ $user->email }} <br />
                    <strong>Secondary Email:</strong> {{ $user->email_secondary }} <br />
                    <strong>Mobile:</strong> {{ $user->mobile }} <br />
                </td>

                <td>
                   @foreach ($user_types as $key=>$type)
                   @if($key==$user->type)
                   {{$type}}
                   @endif
                   @endforeach
                </td>
                
                <td>
                    <center>
                    <a href="{{ route('admin.accounts.edit',[$user->id]) }}">
                        <button type="button" class="btn --btn-small bg-info fc-white" style="width: 150px;">Edit</button>
                    </a> <br />
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $user->id }}" id="deleteItem" style="width: 150px;">Delete</button>
                </center>
                </td>
            </tr>
            @endforeach
            
        </table>
        
        <ul class="pagination">
            {{$users->links()}}
        </ul>
        
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection