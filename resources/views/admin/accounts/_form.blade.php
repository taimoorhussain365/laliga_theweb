@inject('arrays','App\Http\Utilities\Arrays')
<div class="box-body">
    
    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">

        {!! Form::label('name', 'Full Name', ['class' => 'form-label']) !!}

        {!! Form::text('name', null, ['class'=>'form-field','maxlength'=>'50','placeholder' => ' Full Name']) !!}

        @if ($errors->has('name'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'User Email', ['class' => 'form-label']) !!}

        {!! Form::text('email', null, ['class'=>'form-field','placeholder' => ' Email']) !!}

        @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
        @endif
    </div>

     <div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">

        {!! Form::label('mobile', 'User Mobile', ['class' => 'form-label']) !!}

        {!! Form::text('mobile', null, ['class'=>'form-field','maxlength'=>'10','pattern'=>'\d*','placeholder' => 'Mobile']) !!}

        @if ($errors->has('mobile'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('mobile') }}</strong>
        </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('type') ? ' has-error' : '' }}">

        {!! Form::label('type', 'User Type', ['class' => 'form-label']) !!}
        
        {{ Form::select('type',$user_types,null,['class' => 'form-field','placeholder'=>"Select Option"]) }}
        

        @if ($errors->has('type'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('type') }}</strong>
        </span>
        @endif
    </div>


    <div class="control-group{{ $errors->has('password') ? ' has-error' : '' }}">

        {!! Form::label('password', 'Password', ['class' => 'form-label']) !!}

       <input id="password" type="password" class="form-field" name="password" autocomplete="current-password">

        @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>

     <div class="control-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">

        {!! Form::label('confirm-password', 'Confirm Password', ['class' => 'form-label']) !!}

        <input id="password" type="password" class="form-field" name="password_confirmation" autocomplete="current-password">

        @if ($errors->has('password_confirmation'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password_confirmation') }}</strong>
        </span>
        @endif
    </div>

  

    <div class="control-group mb-0">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    </div>
</div>