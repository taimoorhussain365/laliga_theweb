@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit User Account</h3>
<section class="box">

	{!! Form::model($user, ['route' => ['admin.accounts.update', $user->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	
	@include('admin.accounts._form',['submitBtnText' => 'Update Account'])
	
	{!! Form::close() !!}
  	
</section>
@endsection