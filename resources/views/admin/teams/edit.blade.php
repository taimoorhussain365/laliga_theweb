@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Team</h3>
<section class="box">
	<div class="box-body">
		{!! Form::model($team, ['route' => ['admin.teams.update', $team->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.teams._form',['submitBtnText' => 'Update Team'])
		{!! Form::close() !!}
	</div>
</section>
@endsection