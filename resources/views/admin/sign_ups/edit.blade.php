@extends('layouts.admin')

@section('content')

<h3 class="pagetitle">Register User</h3>

<section class="box">

  <div class="box-body">

    {!! Form::model($signUp, ['route' => ['admin.signups.user.register',$signUp->id], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('admin.sign_ups._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection