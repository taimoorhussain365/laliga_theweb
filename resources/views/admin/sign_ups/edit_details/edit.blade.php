@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Edit Form</h3>
<section class="box">
	<div class="box-body">
		
    {!! Form::model($signUp, ['route' => ['admin.signups.updates',$signUp->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

		@include('admin.sign_ups.edit_details._form',['submitBtnText' => 'Update'])
		{!! Form::close() !!}
	</div>
</section>
@endsection