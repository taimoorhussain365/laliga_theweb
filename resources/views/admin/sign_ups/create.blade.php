@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add</h3>
<section class="box">
    {!! Form::open(['route'=>['admin.signups.store',$id],'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.sign_ups._form',['submitBtnText' => 'Add'])
    {!! Form::close() !!}
</section>
@endsection