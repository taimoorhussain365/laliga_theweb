@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">{{$heading}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        
    </div>
    <div class="box-body">
        {!! Form::open(['route' => ['admin.signups.search',$id], 'files' => 'true', 'method' => 'GET', 'class' => 'default-form', 'enctype'=>"multipart/form-data"]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="control-group">
                {!! Form::text('keyword', $request->keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    {!! Form::text('created_at',$request->created_at ?? null, ['class'=>'form-field','placeholder' => 'Search By Created Date','id'=>'created_at','autocomplete'=>'off']) !!}
                </div>
                <div class="control-group">
                    <select class="form-field days" name="category_id">
                        <option value="">Multiple Category Selection</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" {{!empty($request->category_id) && $category->id == $request->category_id ? "selected" : "" }}>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="control-group">
                    {!! Form::text('track_no',$request->track_no ?? null, ['class'=>'form-field','placeholder' => 'Search By Track No']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    <select class="form-field" name="source_id" >
                        <option value="">Multiple Source Selection</option>
                        @foreach($sources as $key=> $source)
                        <option value="{{$key}}" {{!empty($request->source_id) && $key == $request->source_id ? "selected" : "" }}>{{$source}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="control-group">
                    <select class="form-field" name="status_id" >
                        <option value="">Multiple Status Selection</option>
                        @foreach($status as $key=> $currentStatus)
                        <option value="{{$key}}" {{!empty($request->status_id) && $key == $request->status_id ? "selected" : "" }}>{{$currentStatus}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="control-group">
                    <select class="form-field" name="comments">
                        <option value="">Select Post Comments</option>
                        <option value="1" {{!empty($request->comments) && $request->comments == 1 ? "selected" : "" }}>Yes</option>
                        <option value="2" {{!empty($request->comments) && $request->comments == 2 ? "selected" : "" }}>No</option>
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="control-group">
                    <select class="form-field" name="user_id">
                        <option value="">Multiple Users</option>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" {{!empty($request->user_id) && $user->id == $request->user_id ? "selected" : "" }}>{{$user->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="control-group">
                    <select class="form-field" name="term_id" >
                        <option value="">Multiple Interest Selection</option>
                        @foreach($terms as $term)
                        <option value="{{$term->id}}" {{!empty($request->term_id) && $term->id == $request->term_id ? "selected" : "" }}>{{$term->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="control-group">
                    {!! Form::text('created_to',$request->created_to ?? null, ['class'=>'form-field','id' => 'start_date','placeholder' => 'Created To','id'=>'created_to','autocomplete'=>'off']) !!}
                </div>
            </div>
        </div>
        <button type="submit" class="btn --btn-small --btn-primary fc-white">Search</button>
        <a href="{{route('admin.signups.index',[$id])}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>

        {!! Form::close() !!}
        <table class="table --bordered --hover mt-30">
            <tr>
                <th>Track NO</th>
                <th>Details</th>
                <th>Status</th>
                <th>Source</th>
                <th>Team Member</th>
                <th>User Status</th>
                <th>Comments</th>
                <th>Action</th>
            </tr>
            
            
            @foreach ($sign_ups as $sign_up)
            <tr>
                <td>{{ $no++ }}</td>
                <td>
                    Name:{{ $sign_up->name }}<br>
                    Email:{{ $sign_up->email }}<br>
                    Mobile:{{ $sign_up->mobile }}<br>
                   
                </td>
                <td>
                    @foreach ($status as $key=>$currentStatus)
                    @if($key==$sign_up->status_id)
                    {{$currentStatus}}
                    @endif
                    @endforeach
                </td>
                <td>
                    @foreach ($sources as $key=>$source)
                    @if($key==$sign_up->source_id)
                    {{$source}}
                    @endif
                    @endforeach
                </td>
                <td>
                    @if(!empty($sign_up->user_id))
                    {{$sign_up->user->name}}
                    @endif
                </td>
                <td>
                    @if($sign_up->is_registered==0)
                      <a href" class="btn --btn-small bg-warning fc-white" style="width: 100px;">Not Registered</a>
                    <br/>
                    @else
                      <a href" class="btn --btn-small bg-success fc-white" style="width: 100px;">Already Registered</a>
                    <br/>
                    @endif
                </td>
             <td><center>
                 @if(!empty($sign_up->comments))
                     @php $comments=json_decode($sign_up->comments,true); @endphp
                     @foreach($comments as $key => $comment)
                       @if($comment['id']==$sign_up->id)
                          {{ $comment['comments']}}
                           <b>Posted By:</b>{{ $comment['admin_name']}}<br>
                           <b>Updated At:</b>{{$comment['posted_on']}}<br>
                          @endif
                        @endforeach
                        
                        @endif
                </center>
                </td>
                
                <td>
                    @if($sign_up->form_type_id !=TYPE_ENQUIRY && $sign_up->is_registered==0)

                    <a href="{{ route('admin.signups.register',[$sign_up->id]) }}" class="btn --btn-small bg-secondary fc-white" style="width: 150px;">Register User</a>
                    <br/>
                    @endif
                     <a href="{{ route('admin.signups.edit',[$sign_up->id]) }}" class="btn --btn-small bg-secondary fc-white" style="width: 150px;">Edit</a>
                      <br/>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $sign_up->id }}" id="deleteItem" style="width: 150px;">Delete</button>
                </td>
                
            </tr>
            @endforeach
          </table>
        
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteResourceUrl = '{{ url()->current() }}';
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
var dateToday = new Date();
$( "#created_at" ).datepicker({
});
$( "#created_to" ).datepicker({
});
</script>
@endsection