@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Attendance</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
   
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Player ID</center></th>
        <th><center>Category</center></th>
        <th><center>DOB</center></th>
        <th><center>Advance Payment</center></th>
        <th><center>Payment Status</center></th>
        <th><center>Present</center></th>
        <th><center>Coach Comment</center></th>
      </tr>
      @foreach ($attendance as $key=> $attend)
      <tr>
        <td>
          {{ $key+1  }}
        </td>
        <td>
          
          {{ $attend->player->name  }}
        </td>
        
        <td>
          {{ $attend->player->category->name }}
        </td>
        <td>
          {{ $attend->player->dob }}
        </td>
        <td>

        </td>
        <td>
         
        </td>
        <td>
        @if($attend->is_present==1){{ "Present" }}
                    @else {{"Absent" }}@endif
        </td>
        <td>
        {{ $attend->coach_comments }}
        </td>
      </tr>
      @endforeach
    </table>
    <ul class="pagination">
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-left"></i>
        </a>
      </li>
      <li>
        <a href="#">1</a>
      </li>
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-right"></i>
        </a>
      </li>
    </ul>
  </div>
</section>
<script>
var dateToday = new Date();
$( "#date" ).datepicker();
</script>
@endsection