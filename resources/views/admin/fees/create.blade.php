@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Fee</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' =>['admin.fee.sessions.store'], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.fees._form',['submitBtnText' => 'Add Fees'])
		{!! Form::close() !!}
	</div>
	<!-- Modal -->
</section>
@endsection