@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Wallet: {{$parent->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <a href="{{route('admin.wallets.create',[$parent])}}" class="btn --btn-small bg-primary fc-white mb-0">Add Amount</a>
            </div>
            <div class="col-4">
                <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        @if($wallet_amount < 0)
        <div class="alert --danger" role="alert">
        <h3>Wallet Amount:{{$wallet_amount}}</h3>
        </div>
        @else
        <div class="alert --success" role="alert">
        <h3>Wallet Amount:{{$wallet_amount}}</h3>
    </div>
        @endif
        <table class="table --bordered --hover">
            <tr>
                <th>
                    <center>S NO</center>
                </th>
                <th>
                    <center>Debit</center>
                </th>
                <th>
                    <center>Credit</center>
                </th>
                <th>
                    <center>Remarks</center>
                </th>
                <th>
                    <center>Status</center>
                </th>
                <th>
                    <center>Date/time</center>
                </th>
                <th>
                    <center>Balance</center>
                </th>
                <th>
                    <center>Action</center>
                </th>
            </tr>
            
            @php 
             $debit_amount=0;
             $credit_amount=0;
             $total=0;
             @endphp
            @foreach ($wallets as $wallet)
             @php
            $debit_amount+=$wallet->debit_amount;
            $credit_amount+=$wallet->credit_amount;
            $total+=$wallet->total;
           
            @endphp
            <tr>
                <td>
                    <center>{{ $no++ }}</center>
                </td>
                <td>
                    <center>{{ $wallet->debit_amount }}</center>
                </td>
                <td>
                    <center>{{ $wallet->credit_amount }}</center>
                </td>
                <td>
                    <center>{{ $wallet->remarks }}</center>
                </td>
                <td>
                    <center>
                    @foreach($types as $key=> $type)
                    @if($key==$wallet->status)
                    {{$type}}
                    @endif
                   
                    @endforeach
                    </center>
                </td>
                <td>
                    <center>{{date('d/m/Y - H:i',strtotime($wallet->created_at))}}</center>
                </td>
                <td>
                    <center>{{ $wallet->total }} AED</center>
                </td>
               <td> <center>
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $wallet->id }}" id="deleteItem">Delete</button>
                 </center></td>
            </tr>
            
            @endforeach
             <tr>
                <th>
                    <center></center>
                </th>
                <th>
                    <center>{{ $debit_amount }}</center>
                </th>
                <th>
                    <center>{{ $credit_amount }}</center>
                </th>
                <th>
                    <center></center>
                </th>
                <th>
                    <center></center>
                </th>
                <th>
                    <center></center>
                </th>
                <th>
                    <center></center>
                </th>
                <th>
                    <center></center>
                </th>
            </tr>
            
        </table>
       
        
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}

<script>
    var deleteResourceUrl = '{{ url()->current() }}';
    $(document).ready(function () {
        $('.deleteItem').click(function (e) {
        e.preventDefault();
        var deleteId = parseInt($(this).data('deleteId'));
        var userId = $(this).attr("data-user-id");
        $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
        if (confirm('Are you sure?')) {
            $('#deleteForm').submit();
        }
        });
    });
</script>

@endsection