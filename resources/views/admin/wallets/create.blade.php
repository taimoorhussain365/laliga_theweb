@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Wallet Amount</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' => 'admin.wallets.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	  		@include('admin.wallets._form',['submitBtnText' => 'Add Wallet'])
	  	{!! Form::close() !!}
	</div>
</section>
@endsection