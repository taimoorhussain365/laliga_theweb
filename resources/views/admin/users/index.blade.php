@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Parents</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
                <h4 class="box-title">
                Search by Filters
                </h4>
            </div>
            
            <div class="col-4">
                <a href="{{ url()->previous() }}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>
    <div class="box-body">
        {!! Form::open(['route' => 'admin.parents.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('keyword',$keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword' ]) !!}
                </div>
                
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('pr',$pr ?? null, ['class'=>'form-field','placeholder' => 'Search By PR Number' ]) !!}
                </div>
                
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('name',$name ?? null, ['class'=>'form-field','placeholder' => 'Search By Name']) !!}
                </div>
                
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('email',$email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('mobile',$mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="control-group">
                    <select name="nationality" class="form-field">
                        <option value="">Select Nationality</option>
                        @foreach($arrays::countries() as $country)
                        <option value="{{ $country }}" @if(!empty($nationality) && $nationality == $country) selected="selected" @endif>{{ $country }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <button type="submit" class="btn --btn-small --btn-primary fc-white">Search</button>
        <a href="{{route('admin.parents.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
        {!! Form::close() !!}
    </div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>S NO</th>
                <th>Parent Name</th>
                <th>Parent Details</th>
                <th>Action</th>
            </tr>
            <tr>
                
                @foreach ($parents as $key=>$parent)
                
                <td>{{ $parents->firstItem() + $key }}</td>
                <td>
                    
                    Name:{{ $parent->name }}</td>
                
                <td>
                    <strong>PR Number:</strong>{{ $parent->id}}<br>
                    <strong>Email:</strong> {{ $parent->email }} <br />
                    <strong>Secondary Email:</strong> {{ $parent->email_secondary }} <br />
                    <strong>Mobile:</strong> {{ $parent->mobile }} <br />
                    <strong>Other Contact:</strong> {{ $parent->other_contact_no }} <br />
                    <strong>Nationality:</strong> {{ $parent->nationality }}
                </td>
                
                <td>
                    <a href="{{ route('admin.parents.edit',[$parent->id]) }}">
                        <button type="button" class="btn --btn-small bg-info fc-white">Edit</button>
                    </a> <br />
                    <a href="{{ route('admin.wallets.index',[$parent->id]) }}">
                        <button type="button" class="btn --btn-small bg-secondary fc-white">Wallet</button>
                    </a> <br />
                    <a href="{{ route('admin.account.switch',[$parent->id])}}" target="_blank">
                        <button type="button" class="btn --btn-small bg-success fc-white">Access Account</button>
                    </a> <br />
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $parent->id }}" id="deleteItem">Delete</button>
                </td>
            </tr>
            @endforeach
            
        </table>
        
        <ul class="pagination">
            {{$parents->links()}}
        </ul>
        
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));

$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection