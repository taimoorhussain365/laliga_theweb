@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Gallery</h3>
<section class="box">
  <div class="box-body">
    {!! Form::open(['route' => 'admin.galleries.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.galleries._form',['submitBtnText' => 'Add New'])
    {!! Form::close() !!}
  </div>
</section>
@endsection