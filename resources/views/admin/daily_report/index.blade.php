@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Daily Report</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    {!! Form::open(['route' => 'admin.daily.report.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    <div class="row">
      <div class="col-sm-4">
        <div class="control-group">
          {!! Form::text('payment_date_from', $payment_from ?? null, ['class'=>'form-field','placeholder' => 'Payment Date From ','id'=>'payment_date_from','autocomplete'=>'off', 'required'=>'required']) !!}
        </div>
      </div>
      <div class="col-sm-4">
        <div class="control-group">
          {!! Form::text('payment_date_to', $payment_to ?? null, ['class'=>'form-field','placeholder' => 'Paymnet Date To','id'=>'payment_date_to','autocomplete'=>'off','required'=>'required']) !!}
        </div>
      </div>
    </div>
    <button type="submit" class="btn --btn-small --btn-primary fc-white">Search</button>
    <a href="{{route('admin.daily.report.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
    {!! Form::close() !!}
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th>S NO</th>
        <th><br/>Date</th>
        <th>Total<br /> Parents</th>
        <th>Total <br />Players</th>
        <th>Total<br /> Paid</th>
        <th>Total Partially<br /> Paid</th>
        <th>Refunded</th>
      </tr>
      
      @foreach($reports as $key=>$report)
      <tr>
        <td>{{ $reports->firstItem()+$key}}</td>
        <td>{{$report->date}}</td>
        <td>{{$report->total_parent}}</td>
        <td>{{$report->total_player}}</td>
        <td>{{$report->total_paid}}</td>
        <td>{{$report->total_partial_paid }}</td>
        <td>{{$report->total_refund}}</td>
      </tr>
      @endforeach
    </table>
    <ul class="pagination">
      {{$reports->links()}}
    </ul>
    
  </div>
</section>
<script>
var dateToday = new Date();
$( "#payment_date_from" ).datepicker({
});
var dateToday = new Date();
$( "#payment_date_to" ).datepicker({
});
</script>
@endsection