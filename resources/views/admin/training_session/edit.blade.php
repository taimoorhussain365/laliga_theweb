@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Training Session-{{$term->name}}</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-8">
        <a href="{{route('admin.training.sessions.create',[$term])}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add Traning Session</button></a>
      </div>
      <div class="col-4">
        <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
      </div>
    </div>
  </div>
  <div class="box-body">
    
    {!! Form::model($term, ['route' => ['admin.training.sessions.edit',$term->id], 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    
    <div class="row">
      <div class="col-md-4">
        <div class="control-group">
          
          {!! Form::label('category_id', 'Select category', ['class' => 'form-label']) !!}
          {!! Form::select('category_id',$categories,$request->category_id ?? null, ['class'=>'form-field','placeholder' => 'Select category']) !!}
    
          
         </div> 
        </div>
        <div class="col-md-4">
        <div class="control-group">
          
          {!! Form::label('location_id', 'Select location', ['class' => 'form-label']) !!}
          {!! Form::select('location_id',$locations,$request->location_id ?? null, ['class'=>'form-field','placeholder' => 'Select location']) !!}
       </div>
     </div>
     <div class="col-md-4">
        <div class="control-group">
          
          {!! Form::label('day_id', 'Select day', ['class' => 'form-label']) !!}
          {!! Form::select('day_id',$days,$request->day_id ?? null, ['class'=>'form-field','placeholder' => 'Select Day']) !!}
       </div>
      </div>
    </div>
    <button type="submit" class="btn --btn-small --btn-primary fc-white" name="is_submit" value="1">Search</button>
    
    <a href="{{route('admin.training.sessions.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear</button></a>
    {!! Form::close() !!}
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th><center>S NO</center></th>
          <th><center>Details</center></th>
          <th><center>Day/time</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($sessions as $key =>$session)
        <tr>
          <td><center>{{$sessions->firstItem() + $key }}</center></td>
          <td><center>Category:{{ $session->category->name}}
            <br>Term:@if(!empty($session->term->name)){{ $session->term->name}}@endif
            <br>Location:@if(!empty($session->locations->name)){{ $session->locations->name}}@endif
          </center></td>
          <td><center>{{ $session->day->name }}<br>
            {{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}
          </center></td>
          <td><center><a href="{{ route('admin.training.sessions.edit.details',[$session->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
        <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $session->id }}" id="deleteItem">Delete</button></center></td>
      </tr>
      @endforeach
    </table>
    <ul class="pagination">
      {{$sessions->links()}}
    </ul>
  </div>
</section>
<!-- /.content-wrapper -->
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection