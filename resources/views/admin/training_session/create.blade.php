@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Add Training Session-{{$term->name}}</h3>
<section class="box">
	{!! Form::open(['route' =>  ['admin.season.term.training.sessions.store', $term], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
	<div class="box-body">
		
		<div class="control-group{{ $errors->has('categories') ? ' has-error' : '' }}">
			{!! Form::label('categories', 'Select Categories', ['class' => 'form-label']) !!}
			
			@foreach($categories as $category)
			{!! Form::checkbox('categories[]',$category->id,null) !!}{!!$category->name!!}
			@endforeach
			@if ($errors->has('categories'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('categories') }}</strong>
			</span>
			@endif
		</div>
		<div class="control-group{{ $errors->has('locations') ? ' has-error' : '' }}">
			{!! Form::label('locations', 'Select locations', ['class' => 'form-label']) !!}
			@foreach($locations as $location)
			{!! Form::checkbox('locations[]',$location->id,null) !!}{!!$location->name!!}
			
			@endforeach
			@if ($errors->has('locations'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('locations') }}</strong>
			</span>
			@endif
		</div>
		<div class="control-group{{ $errors->has('days') ? ' has-error' : '' }}">
			{!! Form::label('days', 'Select days', ['class' => 'form-label']) !!}
			@foreach($days as $day)
			{!! Form::checkbox('days[]',$day->id,null) !!}{!!$day->name!!}
			
			@endforeach
			@if ($errors->has('days'))
			<span class="invalid-feedback" role="alert">
				<strong>{{ $errors->first('days') }}</strong>
			</span>
			@endif
			@include('admin.training_session._form',['submitBtnText' => 'Add Training Session'])
			{!! Form::close() !!}
		</div>
	</section>
	@endsection