<div class="bootstrap-timepicker">
    <div class="control-group{{ $errors->has('start_time') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::label('time', 'Start Time', ['class'=>'control-label']) !!}
        </div>
        <div class="col-sm-9">
            <div class='input-group'>
                {!! Form::time('start_time',$session->start_time ?? null,['class'=>'form-field start-timepicker','required' => 'required','id'=>'start_time']) !!}
                <div class="input-group-addon"><i class="fa fa-clock-o"></i>
                </div>
            </div>
            
            @if ($errors->has('start_time'))
            <span class="error-msg">
                <strong>{{ $errors->first('start_time') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
<div class="bootstrap-timepicker">
    <div class="control-group{{ $errors->has('end_time') ? ' has-error' : '' }}">
        <div class="col-sm-3">
            {!! Form::label('end_time','End Time', ['class'=>'control-label']) !!}
        </div>
        <div class="col-sm-9">
            <div class='input-group'>
                {!! Form::time('end_time',$session->end_time ?? null,['class'=>'form-field end-timepicker','required' => 'required','id'=>'end_time']) !!}
                <div class="input-group-addon"><i class="fa fa-clock-o"></i>
                </div>
            </div>
            
            @if($errors->has('end_time'))
            <span class="error-msg">
                <strong>{{ $errors->first('end_time') }}</strong>
            </span>
            @endif
        </div>
    </div>
</div>
@if(!empty($session))
<div class="control-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('category_id', 'Category Id', ['class'=>'control-label']) !!}
    </div>
    <div class="col-sm-9">
        {{ Form::select('category_id',$categories,null, ['class' => 'form-field','placeholder'=>'Select Location']) }}
        
        @if ($errors->has('category_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('category_id') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('location_id') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('location_id', 'Location Id', ['class'=>'control-label']) !!}
    </div>
    <div class="col-sm-9">
        {{ Form::select('location_id',$locations,null, ['class' => 'form-field','placeholder'=>'Select Location']) }}
        
        @if ($errors->has('location_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('location_id') }}</strong>
        </span>
        @endif
    </div>
</div>
@endif
<div class="control-group{{ $errors->has('cap_a') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('cap_a', 'Cap A', ['class'=>'control-label']) !!}
    </div>
    <div class="col-sm-9">
        <div class='input-group'>
            {!! Form::text('cap_a',null,['class'=>'form-field']) !!}
            <div class="input-group-addon"><i class="fa fa-clock-o"></i>
            </div>
        </div>
        
        @if ($errors->has('cap_a'))
        <span class="error-msg">
            <strong>{{ $errors->first('cap_a') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('cap_b') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('cap_b', 'Cap B', ['class'=>'control-label']) !!}
    </div>
    <div class="col-sm-9">
        <div class='input-group'>
            {!! Form::text('cap_b',null,['class'=>'form-field']) !!}
            <div class="input-group-addon"><i class="fa fa-clock-o"></i>
            </div>
        </div>
        
        @if ($errors->has('cap_b'))
        <span class="error-msg">
            <strong>{{ $errors->first('cap_b') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('cap_c') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::label('cap_c', 'Cap C', ['class'=>'control-label']) !!}
    </div>
    <div class="col-sm-9">
        <div class='input-group'>
            {!! Form::text('cap_c',null,['class'=>'form-field']) !!}
            <div class="input-group-addon"><i class="fa fa-clock-o"></i>
            </div>
        </div>
        
        @if ($errors->has('cap_c'))
        <span class="error-msg">
            <strong>{{ $errors->first('cap_c') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('for_goal_keeper') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::checkbox('for_goal_keeper', 1, isset($session) ? $session->for_goal_keeper : true) !!}
        {!! Form::label('for_goal_keeper', 'For Goal Keeper', ['class' => 'control-label']) !!}
        @if ($errors->has('for_goal_keeper'))
        <span class="error-msg">
            <strong>{{ $errors->first('for_goal_keeper') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
    <div class="col-sm-3">
        {!! Form::checkbox('is_active', 1, isset($session) ? $session->is_active : true) !!}
        {!! Form::label('is_active', 'Activate', ['class' => 'control-label']) !!}
        @if ($errors->has('is_active'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group">
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
    
</div>
<!---<script>
$('.start-timepicker, .end-timepicker').wickedpicker({
twentyFour: true,
});---->
</script>