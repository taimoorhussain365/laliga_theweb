@extends('layouts.admin')
@section('content')
<h3 class="pagetitle">Audit</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th>Id</th>
        <th>Details</th>
       
       
        <th>New <br/>Values</th>
        <th>Created At</th>
      </tr>
      
      @foreach($audits as $audit)
      <tr>
        
        <td>{{$audit->id}}</td>
        <td>User Name:{{$audit->user->name}}<br>
          Event:{{$audit->event}}<br>
        Audit Type:{{$audit->auditable_type}}</td>
        
        <td><center>
          @if(!empty($audit->new_values))
          @php $values=json_decode($audit->new_values,true); @endphp
          @foreach($values as $key => $value)
            {{$key}}:{{$value}}<br>
        
          @endforeach
          
          @endif
        </center></td>
      
        <td>{{$audit->created_at}}</td>
      </tr>
      @endforeach
    </table>
    
  </div>
</section>
@endsection