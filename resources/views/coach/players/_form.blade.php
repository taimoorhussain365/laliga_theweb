<div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
    
    {!! Form::label('name', 'Player Name', ['class'=>'form-label',]) !!}
    
    {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
    
    @if ($errors->has('name'))
    <span class="error-msg">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
    
    {!! Form::label('dob', 'Player DOB', ['class'=>'form-label']) !!}
    {!! Form::date('dob', null, ['class'=>'form-field dob','required' => 'required','id'=>'dob','max'=>"2018-12-31"]) !!}
    @if ($errors->has('dob'))
    <span class="error-msg">
        <strong>{{ $errors->first('dob') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
    {!! Form::label('category_id', 'Category Id', ['class'=>'form-label']) !!}
    {!! Form::text('category_id',$player->category->name, ['class'=>'form-field','required' => 'required','id'=>'category_id','readonly' => 'true']) !!}
    @if ($errors->has('category_id'))
    <span class="error-msg">
        <strong>{{ $errors->first('category_id') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('admin_category_id') ? ' has-error' : '' }}">
    {!! Form::label('admin_category_id', 'Admin Category Id', ['class'=>'form-label']) !!}
    {{ Form::select('admin_category_id',$categories,null, ['class' => 'form-field','placeholder'=>'Select Category']) }}
    @if ($errors->has('admin_category_id'))
    <span class="error-msg">
        <strong>{{ $errors->first('admin_category_id') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('gender') ? ' has-error' : '' }}">
    
    {!! Form::label('gender','Gender', ['class'=>'form-label']) !!}
    {!! Form::select('gender', array('1' => 'Male', '2' => 'Female'),null,['class' => 'form-field']); !!}
    @if ($errors->has('gender'))
    <span class="error-msg">
        <strong>{{ $errors->first('gender') }}</strong>
    </span>
    @endif
</div>

<div class="control-group{{ $errors->has('previous_football_academy') ? ' has-error' : '' }}">
    {!! Form::label('previous_football_academy', 'Previous Football Academy', ['class'=>'form-label',]) !!}
    {!! Form::text('previous_football_academy', null, ['class'=>'form-field']) !!}
    @if ($errors->has('previous_football_academy'))
    <span class="error-msg">
        <strong>{{ $errors->first('previous_football_academy') }}</strong>
    </span>
    @endif
</div>

<div class="control-group">
    
    @if(!empty($player->admin_comments))
    @php $comments=json_decode($player->admin_comments,true); @endphp
    
    
    @foreach($comments as $key => $comment)
    @if($comment['id']==$player->id)
    {{ $comment['comments']}}
    <b>Posted By:</b>{{ $comment['admin_name']}}<br>
    <b>Updated At:</b>{{$comment['posted_on']}}<br>
    @endif
    @endforeach
    
    @endif
    
</div>
<div class="control-group{{ $errors->has('admin_comments') ? ' has-error' : '' }}">
    
    {!! Form::label('admin_comments', 'Admin Comments', ['class'=>'form-label',]) !!}
    {!! Form::textarea('admin_comment', null, ['class'=>'form-field']) !!}
    @if ($errors->has('admin_comments'))
    <span class="error-msg">
        <strong>{{ $errors->first('admin_comments') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('is_goal_keeper') ? ' has-error' : '' }}">
    <div class="checkbox">
        {!! Form::checkbox('is_goal_keeper', 1, isset($player) ? $player->is_goal_keeper : false) !!}
        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_goal_keeper', 'Is Goal Keeper', ['class' => 'form-label']) !!}
            @if ($errors->has('is_goal_keeper'))
        </span>
        <span class="error-msg">
            <strong>{{ $errors->first('is_goal_keeper') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('is_hpc_player') ? ' has-error' : '' }}">
    <div class="checkbox">
        {!! Form::checkbox('is_hpc_player', 1, isset($player) ? $player->is_hpc_player : true) !!}
        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_hpc_player', 'Is HPC Player', ['class' => 'form-label']) !!}
        </span>
        
        @if ($errors->has('is_hpc_player'))
        <span class="error-msg">
            <strong>{{ $errors->first('is_hpc_player') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">
    <div class="checkbox">
        {!! Form::checkbox('is_active', 1, isset($category) ? $category->is_active : true) !!}
        <span class="label"></span>
        <span class="text">
            {!! Form::label('is_active', 'Activate', ['class' => 'form-label']) !!}
            @if ($errors->has('is_active'))
        </span>
        
        <span class="error-msg">
            <strong>{{ $errors->first('is_active') }}</strong>
        </span>
        @endif
    </div>
</div>
<div class="control-group mn-0">
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white" id="submit">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
</div>
<!-- <script>
var dateToday = new Date();
$( "#dob" ).datepicker({
maxDate: new Date(2017,12,31)
});
</script> -->
<script type="text/javascript">
$(document).ready(function(){
$('.dob').change(function(){
var gender=$('#gender').val();
var date=$('#dob').val();
$.ajaxSetup({
headers: {
'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});
$.ajax({
type: 'POST',
url: '/admin/players/getCategory',
data: {date:date,gender:gender},
dataType: 'json',
success: function (data) {
$('#category_id').val(data.dataResult);
alert(data.dataResult);
},
error: function (data) {
console.log(data);
}
});
});
});
</script>