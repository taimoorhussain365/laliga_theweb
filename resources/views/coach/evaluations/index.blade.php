@extends('layouts.admin')
@section('content')
@if(Auth::user()->type == USER_TYPE_ADMIN)
@php
$user_type = 'admin';
@endphp
@else
@php
$user_type = 'coach';
@endphp

@endif
<h3 class="pagetitle">Player Evaluation:{{$player->name}}</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-md-6">
                <a href="{{route('admin.player.evaluations.create',[$player->id])}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add</button></a>
            </div>
            <div class="col-md-6">
                <div class="default-form">
                    <div class="control-group --search-group mb-0 float-right">
                        <input type="text" placeholder="Search" class="search-field">
                        <div class="search-button">
                            <button type="submit" class="form-button">
                            <i class="xxicon icon-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-body">
        <table class="table --bordered --hover">
            <tr>
                <th>
                    <center>S NO</center>
                </th>
                <th>
                    <center>Term</center>
                </th>
                <th>
                    <center>Location</center>
                </th>
                <th>
                    <center>Age Category</center>
                </th>
                <th>
                    <center>Action</center>
                </th>
            </tr>
            @foreach ($player->evaluations as $evaluation)
            <tr>
                <td>
                    <center>{{ $no++ }}</center>
                </td>
                <td>
                    <center>{{ $evaluation->term->name }}</center>
                </td>
                <td>
                    <center>{{ $evaluation->location->name }}</center>
                </td>
                <td>
                    <center>{{ $evaluation->category->name }}</center>
                </td>
                <td>
                    <center>
                    @if($evaluation->is_final)
                    <a href="{{ url('/storage/evaluation_report/evaluation_'.$player->id.'_'.$evaluation->id.'.pdf') }}"
                        target="_blank">
                        <button type="button" class="btn --btn-small bg-secondary fc-white"
                        style="width: 150px;">Evaluation PDF
                        </button>
                    </a><br/>
                    <a href="javascript: w=window.open('{{ url('/storage/evaluation_report/evaluation_'.$player->id.'_'.$evaluation->id.'.pdf') }}'); w.print();">​​​​​​​​​​​​​​​​​
                        <button type="button" class="btn --btn-small bg-warning fc-white"
                        style="width: 150px;">Print PDF
                        </button>
                    </a><br/>
                    <a href="{{ route($user_type.'.player.evaluations.send',[$player->id,$evaluation->id]) }}">
                        <button type="button" class="btn --btn-small bg-success fc-white" style="width: 150px;">Send Evaluation Email</button>
                    </a><br/>
                    @endif
                    <a href="{{ route($user_type.'.player.evaluations.edit',[$player->id,$evaluation->id]) }}"><button type="button" class="btn --btn-small --btn-primary" style="width: 150px;">Evaluation Details</button></a>
                    
                    </center>
                </td>
            </tr>
            @endforeach
            
        </table>
        <ul class="pagination">
            <li>
                <a href="#">
                    <i class="xxicon icon-chevrons-left"></i>
                </a>
            </li>
            <li>
                <a href="#">1</a>
            </li>
            <li>
                <a href="#">
                    <i class="xxicon icon-chevrons-right"></i>
                </a>
            </li>
        </ul>
    </div>
</section>
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection