@extends('layouts.coach')
@section('content')
<h3 class="pagetitle">Attendance</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    {!! Form::open(['route' => 'coach.attendance.search', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    <div class="row">
      <div class="col-md-3">
        <div class="control-group">
          <select class="form-field" name="team_id" required="required">
            <option value="">Select Team</option>
            @foreach($teams as $team)
            <option value="{{$team->id}}" @if(!empty($request->team_id) && $request->team_id == $team->id) selected="selected" @endif>{{$team->name}}</option>
            @endforeach
          </select>
        </div>
        
      </div>
      <div class="col-md-3">
        <div class="control-group{{ $errors->has('date') ? ' has-error' : '' }}">
          
          
          {!! Form::text('date',$request->date ?? null, ['class'=>'form-field dob','required' => 'required','id'=>'date','placeholder'=>'Select Date','autocomplete'=>'off']) !!}
          @if ($errors->has('date'))
          <span class="error-msg">
            <strong>{{ $errors->first('date') }}</strong>
          </span>
          @endif
        </div>
      </div>
      
    </div>
    <button type="submit" class="btn --btn-small --btn-primary fc-white" name="submit" value="1">Search</button>
    
   <button type="submit" class="btn --btn-small --btn-primary fc-white" name="submit" value="3">View All record</button>
   
    <a href="{{route('coach.attendance.index')}}"><button type="button" class="btn --btn-small bg-danger fc-white clear">Clear Search</button></a>
   
    
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <tr>
        <th><center>S NO</center></th>
        <th><center>Player ID</center></th>
        <th><center>Category</center></th>
        <th><center>DOB</center></th>
        <th><center>Advance Payment</center></th>
        <th><center>Payment Status</center></th>
        <th><center>Present</center></th>
        <th><center>Coach Comment</center></th>
      </tr>
      @foreach ($players as $key=>$player)
      <tr>
        <td>
          {{ $key+1  }}
        </td>
        <td>
          
          {{ $player->name  }}
        </td>
        
        <td>
          {{ $player->category->name }}
        </td>
        <td>
          {{ $player->dob }}
        </td>
        <td>

        </td>
        <td>
         
        </td>
        <td>
        <div class="form-check">
            {!! Form::checkbox('is_present[]',$player->id) !!}

          </div>
        </td>
        <td>
           <div class="control-group{{ $errors->has('coach_comments') ? ' has-error' : '' }}">
           {!! Form::text('coach_comments[]', null, ['class'=>'form-field']) !!}
          </div>
        </td>
      </tr>
      @endforeach
    </table>
    <button type="submit" name="submit" class="btn --btn-small --btn-primary fc-white" value="2">Save</button>
     {!! Form::close() !!}
    <ul class="pagination">
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-left"></i>
        </a>
      </li>
      <li>
        <a href="#">1</a>
      </li>
      <li>
        <a href="#">
          <i class="xxicon icon-chevrons-right"></i>
        </a>
      </li>
    </ul>
  </div>
</section>
<script>
var dateToday = new Date();
$( "#date" ).datepicker();
</script>
@endsection