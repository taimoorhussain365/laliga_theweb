<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="description" content="LaLiga Football Academy [Dubai, Sharjah, Abu Dhabi] is the only scouting soccer school in the UAE. Register today to get scouted by LaLiga clubs or Earn US football college scholarships.">
    <meta name="keywords" content="academy, sports, football, football for Young people, Sport experience">
    <meta property="og:title" content="LaLiga Academy UAE" />
    <meta property="og:description" content="LaLiga Football Academy [Dubai, Sharjah, Abu Dhabi] is the only scouting soccer school in the UAE. Register today to get scouted by LaLiga clubs or Earn US football college scholarships." />
    <meta property="og:image" content="{{ asset('assets-web/images/laligauae.png') }}"/>
    <title>Best Football Academy in Dubai, Sharjah & Abu Dhabi, UAE | LaLiga Football Academy UAE</title>
    <meta name="google-site-verification" content="yxWus9AzZTpenc4MLtVdAye_Pj7Krc7ahgxV2lvysJQ" />
    <link rel="shortcut icon" href="{{ asset('assets-web/images/favicon.ico') }}">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <noscript id="deferred-styles">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets-web/css/style.css') }}"/>
    </noscript>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <script>
    var loadDeferredStyles = function() {
    var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
    replacement.innerHTML = addStylesNode.textContent;
    document.body.appendChild(replacement)
    addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
    webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
    </script>
    <style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .pre-loading {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset('/assets-web/images/loader.gif') }}') center no-repeat #24282b;
    background-size: 30%;
    }
    </style>
    
    <script type="text/javascript" src="{{ asset('assets-web/js/functions.js') }}"></script>
    
    @yield('head-scripts')
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="pre-loading"></div>
    <main class="app-container">
        @include('includes.primary_navigation')
        
        <div id="app">
            <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/coach-banner.jpg') }}')">
                <div class="inner-wrapper">
                    <h1 class="title">Dashboard</h1>
                </div>
            </section>
            
            @yield('content')
        </div>
        <footer class="primary-footer">
            <div class="container-wrapper">
                <ul class="unstyled inline footer-link nomar">
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('pages.about') }}">{{ __('Who We Are') }}</a>
                    </li>
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('register') }}">{{ __('Registration') }}</a>
                    </li>
                    @guest
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @endguest
                    <li>
                        <a href="{{ route('pages.faq') }}">{{ __('FAQs') }}</a>
                    </li>
                    <li class="d-none d-md-inline-block">
                        <a href="#">{{ __('Contact Us') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.privacy_policy') }}">{{ __('Privacy Policy') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.terms_conditions') }}">{{ __('Terms & Conditions') }}</a>
                    </li>
                </ul>
                <ul class="unstyled inline social-media nomar">
                    <li>
                        <a href="https://www.facebook.com/laligaacademyuae/" target="_blank">
                            <i class="xicon xicon-facebook facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/laligaacademyuae/" target="_blank">
                            <i class="xicon xicon-instagram instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/laligacademyuae/" target="_blank">
                            <i class="xicon xicon-twitter twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCpVY9k5KPP4kvrG4uF66TNg/" target="_blank">
                            <i class="xicon xicon-youtube youtube"></i>
                        </a>
                    </li>
                </ul>
                <p class="copyright">Copyright &copy; LaLiga Academy UAE. All Rights Reserved.</p>
            </div>
        </footer>
    </main>
    <!-- <div class="overlay-bg"></div> -->
    <script type="text/javascript" src="{{ asset('assets-web/js/script.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets-web/js/checkout.js') }}"></script>
    <script>
    $(window).load(function() {
    // Animate loader off screen
    $(".pre-loading").fadeOut("slow");;
    });

    setTimeout(function() {
    $('#successMessage').fadeOut('fast');
     }, 3000); // <-- time in milliseconds

    setTimeout(function() {
    $('#errorMessage').fadeOut('fast');
     }, 3000);
    </script>
    @yield('footer-scripts')
</body>
</html>