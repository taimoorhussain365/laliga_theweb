<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="description" content="LaLiga Football Academy [Dubai, Sharjah, Abu Dhabi] is the only scouting soccer school in the UAE. Register today to get scouted by LaLiga clubs or Earn US football college scholarships.">
    <meta name="keywords" content="academy, sports, football, football for Young people, Sport experience">
    <meta property="og:title" content="LaLiga Academy UAE" />
    <meta property="og:description" content="LaLiga Football Academy [Dubai, Sharjah, Abu Dhabi] is the only scouting soccer school in the UAE. Register today to get scouted by LaLiga clubs or Earn US football college scholarships." />
    <meta property="og:image" content="{{ asset('assets-web/images/laligauae.png') }}"/>
    <title>Best Football Academy in Dubai, Sharjah & Abu Dhabi, UAE | LaLiga Football Academy UAE</title>
    <meta name="google-site-verification" content="yxWus9AzZTpenc4MLtVdAye_Pj7Krc7ahgxV2lvysJQ" />
    <link rel="shortcut icon" href="{{ asset('assets-web/images/favicon.ico') }}">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <noscript id="deferred-styles">
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets-web/css/style.css') }}"/>
    </noscript>
    <script>
    var loadDeferredStyles = function() {
    var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
    replacement.innerHTML = addStylesNode.textContent;
    document.body.appendChild(replacement)
    addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
    webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
    else window.addEventListener('load', loadDeferredStyles);
    </script>
    <style>
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .pre-loading {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('{{ asset('/assets-web/images/loader.gif') }}') center no-repeat #24282b;
    background-size: 30%;
    }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets-web/js/functions.js') }}"></script>

    @yield('head-scripts')
   <script src="https://www.google.com/recaptcha/api.js" async defer></script>
   
</head>
<body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="pre-loading"></div>
    <main class="app-container">
        <header class="primary-header">
            <section class="primary-header__topbar">
                <div class="container-wrapper">
                    <div class="row align-items-center">
                        <div class="col-md-3">
                            <ul class="unstyled inline social-links text-md-left text-center">
                                <li>
                                    <a href="https://www.facebook.com/laligaacademyuae/" target="_blank">
                                        <i class="xicon xicon-facebook facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/laligaacademyuae/" target="_blank">
                                        <i class="xicon xicon-instagram instagram"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/laligacademyuae/" target="_blank">
                                        <i class="xicon xicon-twitter twitter"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/channel/UCpVY9k5KPP4kvrG4uF66TNg/" target="_blank">
                                        <i class="xicon xicon-youtube youtube"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-9">
                            <ul class="unstyled toplinks text-md-right text-center">
                                <li>
                                    <a href="tel:+971555099278">
                                        <i class="xicon xicon-mobile"></i>
                                        &nbsp; Abu Dhabi: +971 55 509 9278
                                    </a>
                                </li>
                                <li>
                                    <a href="tel:+971555099489">
                                        <i class="xicon xicon-mobile"></i>
                                        &nbsp; Dubai: +971 55 509 9489
                                    </a>
                                </li>
                                <li>
                                    <?php /*<a href="#">LaLiga Online</a>*/ ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section class="primary-header__logobar">
                <div class="container-wrapper">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-4 col-lg-3 col-md-4 col-8">
                            <a href="/" class="header-logo">
                                <img src="{{ asset('assets-web/images/laliga-academy-logo.png') }}">
                            </a>
                        </div>
                        <div class="col-xl-8 col-lg-9 col-md-8 col-4">
                            <div class="primary-navigation">
                                <ul class="unstyled">
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">
                                            Who we are
                                        </a>
                                        <a href="javascript:;" class="d-block d-lg-none mobile-click">
                                            Who we are
                                        </a>
                                        <ul class="unstyled dropdown">
                                            <li>
                                                <a href="/about">
                                                    laliga academy
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/coaches">
                                                    coaches
                                                </a>
                                            </li>
                                            <?php /* <li>
                                                <a href="/gallery">
                                                    gallery
                                                </a>
                                            </li> */ ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">
                                            Schedule
                                        </a>
                                        <a href="javascript:;" class="d-block d-lg-none mobile-click">
                                            Schedule
                                        </a>
                                        <ul class="unstyled dropdown">
                                            <li>
                                                <a href="/development-schedule">
                                                    development teams
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/advanced-schedule">
                                                    advanced teams
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/training-locations">
                                                    training locations
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">
                                            Program
                                        </a>
                                        <a href="javascript:;" class="d-block d-lg-none mobile-click">
                                            Program
                                        </a>
                                        <ul class="unstyled dropdown">
                                            <li>
                                                <a href="/methodology">
                                                    Methodology
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/development-program">
                                                    development
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/advanced-development-program">
                                                    advanced
                                                </a>
                                            </li>
                                            <?php /* <li>
                                                <a href="/goal-keeper-program">
                                                    goal keeper program
                                                </a>
                                            </li> */ ?>
                                            <li>
                                                <a href="/laliga-hpc-program">
                                                    high performance
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/summer-program">
                                                    summer program
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">
                                            Scouting
                                        </a>
                                        <a href="javascript:;" class="d-block d-lg-none mobile-click">
                                            Scouting
                                        </a>
                                        <ul class="unstyled dropdown">
                                            <li>
                                                <a href="/laliga-trials">
                                                    laliga trials
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/contract-awards">
                                                    contract awards
                                                </a>
                                            </li>
                                            <?php /* <li>
                                                <a href="/scouting-camp">
                                                    us soccer scholarships
                                                </a>
                                            </li> */ ?>
                                            <li>
                                                <a href="/soccer-scholarship">
                                                    dubai international soccer id camp
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">
                                            News
                                        </a>
                                        <a href="javascript:;" class="d-block d-lg-none mobile-click">
                                            News
                                        </a>
                                        <ul class="unstyled dropdown">
                                            <li>
                                                <a href="/news">
                                                    print
                                                </a>
                                            </li>
                                            <li>
                                                <a href="/news/broadcast">
                                                    broadcast
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="/fees">Fees</a>
                                    </li>
                                    <li>
                                        <a href="javascript:;" class="d-none d-lg-block">Registration</a>
                                        <a href="/register" class="d-block d-lg-none">Registration</a>
                                        <ul class="unstyled dropdown">
                                            <li class="d-none d-lg-block">
                                                <a href="/register">
                                                    register now
                                                </a>
                                            </li>
                                            <li class="d-none d-lg-block">
                                                @guest
                                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                                @else
                                                @if(App\User::getAuthenticatedUser()->type == USER_TYPE_ADMIN)
                                                <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>
                                                @elseif(App\User::getAuthenticatedUser()->type == USER_TYPE_PARENT)
                                                <a href="{{ route('account.dashboard') }}">{{ __('Account Area') }}</a>
                                                @endif
                                                @endguest
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="d-lg-none">
                                        @guest
                                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                                        @else
                                        @if(App\User::getAuthenticatedUser()->type == USER_TYPE_ADMIN)
                                        <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>
                                        @elseif(App\User::getAuthenticatedUser()->type == USER_TYPE_PARENT)
                                        <a href="{{ route('account.dashboard') }}">{{ __('Account Area') }}</a>
                                        @endif
                                        @endguest
                                    </li>
                                </ul>
                            </div>
                            <!-- Mobile Navigation Button Start-->
                            <div class="mobile-nav-btn d-lg-none">
                                <span class="lines"></span>
                                <span class="lines"></span>
                                <span class="lines"></span>
                                <span class="lines"></span>
                            </div>
                            <!-- Mobile Navigation Button End-->
                        </div>
                    </div>
                </div>
            </section>
        </header>
        
        <div id="app">
            @yield('content')
        </div>
        <footer class="primary-footer">
            <div class="container-wrapper">
                <ul class="unstyled inline footer-link nomar">
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('pages.about') }}">{{ __('Who We Are') }}</a>
                    </li>
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('register') }}">{{ __('Registration') }}</a>
                    </li>
                    @guest
                    <li class="d-none d-md-inline-block">
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @endguest
                    <li>
                        <a href="{{ route('pages.faq') }}">{{ __('FAQs') }}</a>
                    </li>
                    <li class="d-none d-md-inline-block">
                        <a href="#">{{ __('Contact Us') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.privacy_policy') }}">{{ __('Privacy Policy') }}</a>
                    </li>
                    <li>
                        <a href="{{ route('pages.terms_conditions') }}">{{ __('Terms & Conditions') }}</a>
                    </li>
                </ul>
                <ul class="unstyled inline social-media nomar">
                    <li>
                        <a href="https://www.facebook.com/laligaacademyuae/" target="_blank">
                            <i class="xicon xicon-facebook facebook"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/laligaacademyuae/" target="_blank">
                            <i class="xicon xicon-instagram instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/laligacademyuae/" target="_blank">
                            <i class="xicon xicon-twitter twitter"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCpVY9k5KPP4kvrG4uF66TNg/" target="_blank">
                            <i class="xicon xicon-youtube youtube"></i>
                        </a>
                    </li>
                </ul>
                <p class="copyright">Copyright &copy; LaLiga Academy UAE. All Rights Reserved.</p>
            </div>
        </footer>
    </main>
    <!-- <div class="overlay-bg"></div> -->
    
    <script type="text/javascript" src="{{ asset('assets-web/js/script.js') }}"></script>
    <script>
    $(window).load(function() {
    // Animate loader off screen
    $(".pre-loading").fadeOut("slow");;
    });
    </script>
    @yield('footer-scripts')
</body>
</html>