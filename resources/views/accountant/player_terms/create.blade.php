@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Add Player Term: {{$player->name}}</h3>
<section class="box">
	<div class="box-body">
		{!! Form::open(['route' =>['admin.player.term.store',$player->id], 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		 @include('admin.player_terms._form',['submitBtnText' => 'Add'])
		{!! Form::close() !!}
	</div>
</section>
@endsection