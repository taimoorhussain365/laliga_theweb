@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Installments
@if(!empty($invoice->total_amount))
Total Amount:{{$invoice->total_amount}}
@else
Total Amount:{{$invoice->amount}}
@endif
</h3>
<section class="box">
	@include('includes.message')
	<div class="box-body">
		{!! Form::model($invoice, ['route' => ['admin.invoices.installments.update', $invoice->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		
		@foreach($installments as $installment)
		
			<div class="control-group{{ $errors->has('installments') ? ' has-error' : '' }}">
				<div class="col-sm-3">
					{!! Form::label('installments', 'Installment', ['class'=>'control-label',]) !!}
				</div>
			</div>
				<div class="row">
					<div class="col-sm-6">
						{!! Form::text('installments[]',$installment->amount, ['class'=>'form-field','required' => 'required']) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::select('status[]',$paymentStatus,$installment->status, ['class'=>'form-field','required' => 'required']) !!}
					</div>
				</div>
				@if ($errors->has('installments'))
				<span class="error-msg">
					<strong>{{ $errors->first('installments') }}</strong>
				</span>
				@endif
			</div>
		
		@endforeach
		<div class="form-group">
			<center>
			<input type="submit" value="Update" class="btn --btn-small bg-secondary fc-white">
			<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
			
		</div>
	</div>
	{!! Form::close() !!}
</div>
</section>
@endsection