@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Payment Method</h3>

<section class="box">
	<div class="box-body">
		{!! Form::model($invoice, ['route' => ['admin.invoices.payment.method.update', $invoice->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

		<div class="control-group{{ $errors->has('payment_method') ? ' has-error' : '' }}">

			{!! Form::label('payment_method', 'Payment Method', ['class'=>'form-label',]) !!}

			{!! Form::select('payment_method',$paymentMethods,null, ['class'=>'form-field','required' => 'required']) !!}

			@if ($errors->has('payment_method'))
			<span class="error-msg">
				<strong>{{ $errors->first('payment_method') }}</strong>
			</span>
			@endif

		</div>

		<div class="control-group{{ $errors->has('payment_status') ? ' has-error' : '' }}">

			{!! Form::label('payment_status', 'Payment Status', ['class'=>'form-label',]) !!}

			{!! Form::select('payment_status',$paymentStatus,null, ['class'=>'form-field','required' => 'required','id'=>'paymentStatus','onchange'=>"checkPayment()"]) !!}

			@if ($errors->has('payment_status'))
			<span class="error-msg">
				<strong>{{ $errors->first('payment_status') }}</strong>
			</span>
			@endif
		</div>

		<div style="display: none;" id="partialPaymentType">
			<div class="control-group{{ $errors->has('partial_payment_type') ? ' has-error' : '' }}">

				{!! Form::label('partial_payment_type', 'Parital Payment Type', ['class'=>'form-label']) !!}

				{!! Form::select('partial_payment_type',$paritalPaymentType,null, ['class'=>'form-field','required' => 'required']) !!}

				@if ($errors->has('partial_payment_type'))
				<span class="error-msg">
					<strong>{{ $errors->first('partial_payment_type') }}</strong>
				</span>
				@endif
			</div>
		</div>

		<div class="control-group mb-0">
			<input type="submit" value="Update" class="btn --btn-small bg-secondary fc-white">
			<a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
		</div>

		{!! Form::close() !!}
	</div>
</section>

<script type="text/javascript">
	function checkPayment() {
	 	
	 	var paymentStatus = document.getElementById("paymentStatus").value;
	   	var x = document.getElementById("partialPaymentType");
	   	
	   	if(paymentStatus==2){
	   		x.style.display = "block";
		}
		else {
	    	x.style.display = "none";
	  	}
	}
</script>
@endsection