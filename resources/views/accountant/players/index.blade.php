@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.manager')
@section('content')

<h3 class="pagetitle">Players</h3>

@include('includes.message')

<section class="box">
    <div class="box-header">
        <div class="row align-items-center">
            <div class="col-8">
                <h4 class="box-title">
                    Search by Filters
                </h4>
            </div>
            
            <div class="col-4">
                <a href="{{route('admin.dashboard')}}" class="btn --btn-small bg-secondary fc-white mb-0 float-right">Back</a>
            </div>
        </div>
    </div>

    <div class="box-body">
        {!! Form::open(['route' => 'admin.players.serach', 'files' => 'true', 'method' => 'GET', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
        <div class="row">
            <div class="col-md-12">
                <div class="control-group">
                    {!! Form::text('keyword', $keyword ?? null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('parent_no', $parent_no ?? null, ['class'=>'form-field','placeholder' => 'Parent No']) !!}
                </div>

                <div class="control-group">
                    {!! Form::text('player_mobile',$player_mobile ?? null, ['class'=>'form-field','placeholder' => 'Search By Mobile']) !!}
                </div>

                <div class="control-group">              
                    <select name="nationality" class="form-field">
                        <option value="">Select Country</option>
                        @foreach($arrays::countries() as $country)
                        <option value="{{ $country }}"  @if(!empty($nationality) && $nationality == $country) selected="selected" @endif>{{ $country }}</option>
                        @endforeach
                    </select>
                </div>

               <!--  <div class="control-group">
                    <select class="form-field" name="coach_id">
                        <option value="">Select Coaches</option>
                        @foreach($coaches as $coach)
                        <option value="{{$coach->id}}">{{$coach->name}}</option>
                        @endforeach
                    </select>
                </div> -->

            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('player_no',$player_no ?? null, ['class'=>'form-field','placeholder' => 'Player No']) !!}
                </div>

                <div class="control-group">
                    {!! Form::select('gender', array(''=>'Select Gender','1' => 'Male', '2' => 'Female'),$gender ?? null,['class' => 'form-field']); !!}
                </div>

                <div class="control-group">
                    <select class="form-field" name="team_id">
                        <option value="">Select Team</option>
                        @foreach($teams as $team)
                        <option value="{{$team->id}}" @if(!empty($team_id) && $team_id == $team->id) selected="selected" @endif>{{$team->name}}</option>
                        @endforeach
                    </select>
                </div>

            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('player_name', $player_name ?? null, ['class'=>'form-field','placeholder' => 'Player Name']) !!}
                </div>

                <div class="control-group">
                    <select class="form-field" name="category_id">
                        <option value="">Select Age Category</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(!empty($category_id) && $category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="control-group">
                    {!! Form::select('is_hpc_player', array(''=>'Select HPC Player','1' => 'Yes', '2' => 'No'),$is_hpc_player ?? null,['class' => 'form-field']); !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="control-group">
                    {!! Form::text('player_email',$email ?? null, ['class'=>'form-field','placeholder' => 'Search By Email']) !!}
                </div>

                <div class="control-group">
                    <select class="form-field" name="admin_category_id">
                        <option value="">Management Category System</option>
                        @foreach($categories as $category)
                        <option value="{{$category->id}}" @if(!empty($admin_category_id) && $admin_category_id == $category->id) selected="selected" @endif>{{$category->name}}</option>
                        @endforeach
                    </select>
                </div>
                
                <div class="control-group">
                    {!! Form::select('is_goal_keeper', array(''=>'Select Goal Keeper','1' => 'Yes', '2' => 'No'),$is_goal_keeper ?? null,['class' => 'form-field']); !!}
                </div>
                
            </div>
        </div>

        <button type="submit" class="btn --btn-primary">Search</button>
        {!! Form::close() !!}
        <table class="table --bordered --hover mt-40">
            <tr>
              <th>S NO</th>
              <th>PR No</th>
              <th>PL No</th>
              <th>Player Details</th>
              <th>Guardian Details</th>
              <th>Admin Comments</th>
              <th>Action</th>
            </tr>

            @foreach ($players as $key=>$player)
            <tr>
                <td>
                    {{ $players->firstItem() + $key  }}
                </td>
                <td>
                    PR-{{ $player->user_id  }}
                </td>
                <td>
                    PL-{{ $player->id  }}
                </td>
                  <td>
                    <strong>Name:</strong> {{ $player->name }}<br />
                    <strong>DOB:</strong> {{ $player->dob }} <br />
                    <strong>Gender:</strong> @if($player->gender==GENDER_MALE){{ "Male" }}
                    @else {{"Female" }}@endif <br />
                    <strong>Category:</strong>@if(!empty($player->orginalCategory->name))
                    {{ $player->orginalCategory->name }} @endif<br />
                    <strong>Admin Category:</strong>@if(!empty($player->changeCategory->name))
                    {{ $player->changeCategory->name }}@endif<br />
                    <strong>HPC:</strong> 
                    @if($player->is_hpc_player==HPC_PLAYER){{ "YES" }}
                    @else {{"NO" }}@endif <br />
                    <strong>Goal Keeper:</strong> @if($player->is_goal_keeper==GOAL_KEEPER){{ "YES" }}
                    @else {{"NO" }}@endif<br>
                </td>
                <td>
                    <strong>Name:</strong> {{ $player->user->name }}<br />
                    <strong>Email:</strong> {{ $player->user->email }} <br />
                    <strong>Secondary Email:</strong> {{ $player->user->email_secondary }} <br />
                    <strong>Mobile:</strong> {{ $player->user->mobile }} <br />
                    <strong>Secondary Mobile:</strong> {{ $player->user->other_contact_no }}
                </td>
              
                <td><center>
                 @if(!empty($player->admin_comments))
                     @php $comments=json_decode($player->admin_comments,true); @endphp
        
                       
                       @foreach($comments as $key => $comment)
                       @if($comment['id']==$player->id)
                          {{ $comment['comments']}}
                           <b>Posted By:</b>{{ $comment['admin_name']}}<br>
                           <b>Updated At:</b>{{$comment['posted_on']}}<br>
                          @endif
                        @endforeach
                        
                        @endif
                </center>
                </td>
                <td>
                    <a href="{{ route('admin.players.edit',[$player->id]) }}" class="btn --btn-small bg-info fc-white">Edit</a> <br />
                    <a href="{{ route('admin.players.term',[$player->id]) }}" class="btn --btn-small bg-black fc-white">Terms</a> <br />
                    @if(!empty($player->team_id))
                    <a href="{{ route('admin.player.evaluations.index',[$player->id]) }}" class="btn --btn-small bg-grey fc-white">Evaluations</a><br />
                    @endif
                    <a href="{{ route('admin.wallets.index',[$player->user->id]) }}" class="btn --btn-small bg-warning fc-white">Wallet</a><br />
                    <a href="{{ route('admin.players.invoices.index',[$player->id]) }}" class="btn --btn-small bg-success fc-white">Invoices</a><br />
                    <a href="{{route('admin.account.switch',[$player->user_id]) }}" class="btn --btn-small bg-spgrey fc-white">Access Parent Account</a><br />
                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $player->id }}" id="deleteItem">Delete</button>
                </td>
            </tr>
            @endforeach
      </table>

        <ul class="pagination">
            {{$players->links()}}
        </ul>
    </div>
</section>

{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}

<script>
    var deleteResourceUrl = '{{ url()->current() }}';

    $(document).ready(function () {
        $('.deleteItem').click(function (e) {
        e.preventDefault();
        var deleteId = parseInt($(this).data('deleteId'));
        $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
            if (confirm('Are you sure?')) {
                $('#deleteForm').submit();
            }
        });
    });
    
    var dateToday = new Date();
    $( "#start_date" ).datepicker();
    
    $( "#end_date" ).datepicker();

</script>

@endsection