@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Edit Fee Location:{{$fee->location->name}}</h3>
<section class="box">
  <div class="box-body">
    {!! Form::model($fee,['route' => ['admin.fee.sessions.update',$fee->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
    @include('admin.fees._form',['submitBtnText' => 'Update'])
    {!! Form::close() !!}
  </div>
</section>
@endsection