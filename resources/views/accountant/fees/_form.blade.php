
<div class="control-group{{ $errors->has('term') ? ' has-error' : '' }}">
    
    {!! Form::label('term', 'Select term', ['class' => 'form-label']) !!}
    {{ Form::select('term',$terms,$fee->term->id ?? null, ['class' => 'form-field','placeholder'=>"Select Option"]) }}
    @if ($errors->has('term'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('term') }}</strong>
    </span>
    @endif
</div>
<div class="control-group{{ $errors->has('category') ? ' has-error' : '' }}">
    {!! Form::label('category', 'Select Categories', ['class' => 'form-label']) !!}
    
    @foreach($categories as $category)
    {!! Form::checkbox('categories[]',$category->id,!empty($cateIds) && in_array($category->id, $cateIds)) !!}{!!$category->name!!}
    
    @endforeach
    @if ($errors->has('category'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('category') }}</strong>
    </span>
    @endif
</div>

@if(empty($fee))
<div class="control-group{{ $errors->has('category') ? ' has-error' : '' }}">
    {!! Form::label('locations', 'Select Locations', ['class' => 'form-label']) !!}
    
    @foreach($locations as $location)
    {!! Form::checkbox('locations[]',$location->id,!empty($locIds) &&in_array($location->id, $locIds)) !!}{!!$location->name!!}
    
    @endforeach
    @if ($errors->has('locations'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('locations') }}</strong>
    </span>
    @endif
</div>
@endif
<table class="table table-striped">
    <tr>
        @foreach($feeSession as $fee)
        <th><center>{{$fee}}</center></th>
        @endforeach
        
    </tr>
    
    
    <tr>
        @if(!empty($details))
        @foreach($details as $key=>$detail)
        <td>
            
            
            <div class="control-group">
                {!! Form::text('feeAmount[]', $detail, ['class'=>'form-field']) !!}
            </div>
            {!! Form::hidden('feeSessionId[]',$key, ['class'=>'form-field']) !!}
            
            
        </td>
        @endforeach
        @else
        @foreach($feeSession as $key=>$fee)
        <td>
            
            
            <div class="control-group">
                {!! Form::text('feeAmount[]', null, ['class'=>'form-field']) !!}
            </div>
            {!! Form::hidden('feeSessionId[]',$fee, ['class'=>'form-field']) !!}
            
            
        </td>
        @endforeach
        @endif
    </tr>
    
    
    
</table>

<div class="control-group">
    <center>
    <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
    <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a></center>
    
</div>