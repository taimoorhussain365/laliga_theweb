@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Account Area') }}
            </h2>
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <h3 class="maintitle --white --small mb-0 text-center">Wallet: {{ $wallet }} AED</h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">Profile</h4>
                        {{ Form::model(App\User::getAuthenticatedUser(), ['route' => ['account.profile.update'], 'method' => 'POST', 'class'=>'default-form']) }}
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Full Name</label>
                                    {!! Form::text('name', null, ['class'=>'form-field inputTextBox','required' => 'required']) !!}
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Email Address</label>
                                    <p class="fs-default fc-grey mt-15">{{ App\User::getAuthenticatedUser()->email }}</p>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                            </div>
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Secondary Email Address</label>
                                    {!! Form::email('parent_2_email', null, ['class'=>'form-field inputTextBox', 'id' => 'email_secondary']) !!}
                                    @error('parent_2_email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Mobile Number</label>
                                    {!! Form::text('mobile', null, ['class'=>'form-field inputTextBox','required' => 'required','maxlength'=>10]) !!}
                                    @error('mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Other Contact Number</label>
                                    {!! Form::text('parent_2_mobile', null, ['class'=>'form-field inputTextBox','maxlength'=>10]) !!}
                                    @error('parent_2_mobile')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-group">
                                    <label class="form-label">Nationality</label>
                                    {{ Form::select('nationality', $arrays::countries(), null, ['class' => 'form-field', 'placeholder' => 'Select country']) }}
                                    @error('nationality')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="control-group mb-0 mt-20">
                                    <button type="submit" class="btn --btn-secondary --btn-small">Update Profile</button>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
@endsection
@section('footer-scripts')
