@php
if(count($player_discounts)>0){
foreach($player_discounts as $discount){
$discountdata[]=$discount->id;
}
}
@endphp
@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Player Discounts') }}
            </h2>
            @include('account._partials._alerts')
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <center> <h3 class="maintitle --white --small">Wallet:{{ $wallet }} Dirham</h3></h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">{{ __('Player Discounts') }}</h4>
                        <div class="text-right">
                            
                            <a href="{{ route('account.dashboard') }}" class="btn --btn-small --btn-secondary">Back</a>
                            
                        </div>
                        {{ Form::open(['route'=>['account.players.discount.update',$player->id,$term], 'class'=>"default-form"]) }}
                        <div class="Rtable --collapse --2cols">
                            
                            <div class="Rtable-cell --head">
                                S No.
                            </div>
                            <div class="Rtable-cell --head">
                                Name
                            </div>
                            
                            @php($ii = 1)
                            @foreach($discounts as $discount)
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        S No.
                                    </div>
                                    <div class="content">
                                        {{ $ii++ }}
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Name
                                    </div>
                                    <div class="content">
                                        <div class="control-group">
                                            <div class="checkbox">
                                                <input type="checkbox" name="discounts[]" value="{{$discount->id}}" {{ isset($discountdata) && in_array($discount->id,$discountdata) ? 'checked':''}}>
                                                <span class="label"></span>
                                                <span class="text">
                                                    {{$discount->name}}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn --btn-small bg-secondary fc-white" value="update" name="submit">Submit</button>
                            
                            <button type="submit" class="btn --btn-small bg-danger fc-white" value="delete" name="submit">Remove All</button>
                        </div>
                        
                        {!! Form::close() !!}
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
@endsection