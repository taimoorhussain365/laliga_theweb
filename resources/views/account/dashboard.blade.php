@extends('layouts.parent')
@section('content')

<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Account Area') }}
            </h2>
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                      <h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">Account Details</h4> 
                        <div class="default-form">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Full Name</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->name }}</p>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Email Address</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->email }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Secondary Email Address</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->parent_2_email }}</p>
                                    </div>
                                </div>
                                <?php /*<div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Gender</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->gender == GENDER_MALE ? 'Male' : 'Female' }}</p>
                                    </div>
                                </div>*/?>
                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Mobile Number</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->mobile }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Other Contact Number</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->parent_2_mobile }}</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Nationality</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->nationality }}</p>
                                    </div>
                                </div>
                                <?php /*<div class="col-lg-6">
                                    <div class="control-group">
                                        <label class="form-label">Address</label>
                                        <p class="fs-default fc-grey">{{ App\User::getAuthenticatedUser()->address }}</p>
                                    </div>
                                </div>*/?>
                                <div class="col-lg-6">
                                    <div class="control-group mb-0 mt-20">
                                        <a href="{{ route('account.profile.edit') }}" class="btn --btn-secondary --btn-small">Edit Profile</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
@endsection