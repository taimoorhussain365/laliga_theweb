@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <div class="dashboard-panel mb-40">
            <h2 class="maintitle --black --small">
            Generate Invoices here
            </h2>
            <h6 class="">Please select the players you want to pay for as part of one invoice.</h6>
            <h4 class="fc-primary mt-10 mb-20">For separate invoices, select the player(s) and click "GENERATE INVOICE".</h4>
        </div>
        <article class="inner-content">
            <div class="row">
                
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <center> <h3 class="maintitle --white --small">Wallet:{{ $wallet }} Dirham</h3></center>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">{{ __('Invoice') }}</h4>
                        <div class="text-right">
                            <a href="{{ route('account.invoices.request') }}" class="btn --btn-small --btn-secondary">Back</a>
                        </div>
                        
                        
                        @include('account._partials._alerts')
                        @php $date=\Carbon\Carbon::today()->format('m/d/Y'); @endphp
                        {{ Form::open(['route'=>['account.submit.invoice.request'], 'class'=>'default-form','method'=>'POST']) }}
                        
                        
                        
                        
                        <div class="Rtable --collapse --5cols">
                            
                            
                            <div class="Rtable-cell --head">
                                Player Name
                            </div>
                            <div class="Rtable-cell --head">
                                Term  Details
                            </div>
                            <div class="Rtable-cell --head">
                                Days
                            </div>
                            <div class="Rtable-cell --head">
                                Amount
                            </div>
                            <div class="Rtable-cell --head">
                                Player Term Discount
                            </div>
                            
                            @foreach($data as $key=> $value)
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Player Details</div>
                                    
                                    <div class="content">
                                        <strong>Name:</strong> {{$value['name']}} <br />
                                        <strong>Guardian Name:</strong> {{$value['guardian_name']}} <br />
                                        <strong>DOB:</strong> {{$value['guardian_name']}} <br />
                                        <strong>Category:</strong> {{$value['category']}} <br />
                                        <strong>HPC:</strong> @if($value['is_hpc_player']==HPC_PLAYER){{ "YES" }}
                                        @else {{"NO" }}@endif <br />
                                        <strong>Goal Keeper:</strong> @if($value['is_goal_keeper']==GOAL_KEEPER){{ "YES" }}
                                        @else {{"NO" }}@endif
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Location</div>
                                    
                                    <div class="content">
                                        <div class="content">
                                            {{$value['term_name']}} <br />
                                            <strong>Location:</strong> {{$value['location']}}<br />
                                            <strong>Total Sessions:</strong> {{$value['player_remaining_training_session_count']}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Days</div>
                                    <div class="content">
                                        @foreach($value['start_end_time'] as $key=>$time)
                                        
                                        
                                        {{$key}}: {{$time}}<br />
                                        
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Pro rates</div>
                                    <div class="content">
                                        Price/session: {{$value['pro_rates']}}</br>
                                        Pro rate:{{$value['player_fee']}}<br>
                                        @if(!empty($value['discount_amount']))
                                        After discount:{{$value['discount_amount']}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">Discounts On Player Terms</div>
                                    <div class="content">
                                        @if(!empty($value['discount']))
                                        @foreach($value['discount'] as $key=>$discount)
                                        {{$discount}}<br />
                                        
                                        @endforeach
                                        
                                        @endif

                                    </div>
                                </div>
                            </div>
                            
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-4 offset-8">
                                
                                <table class="table --bordered">
                                    
                                    <tr>
                                        <td>
                                            Kit Amount:
                                        </td>
                                        <td>
                                            {{$kit_amount}}
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            VAT: 5%
                                        </td>
                                        <td>
                                            {{$vat}}
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                            <div class="alert --defualt" role="alert">
                                <h6 id="total_sum" class="text-right">Total:{{$total_sum}}</h6>
                            </div>
                        
                        
                            <div class="text-right">
                                <button type="submit" class="btn --btn-primary add mt-40">Submit</button>
                                
                            </div>
                        
                        {!! Form::close() !!}
                    </div>
                </section>
            </div>
        </div>
    </article>
</div>
</section>
<script>
$('#wallet_amount').hide();
$('#is_wallet_amount').click(function () {
var $this = $(this);
if ($this.is(':checked')) {
$('#wallet_amount').show();
} else {
$('#wallet_amount').hide();
}
});
</script>
@endsection