@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.parent')
@section('content')

<section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
    <div class="container-wrapper">
        <article class="inner-content">
        <h2 class="maintitle --black --small">
            {{ __('Edit Player') }}
            </h2>
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                            <h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
                        </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">{{ __('Edit Player') }} <span class="fc-primary">{{ $player->name }}</span></h4>
                        {{ Form::model($player,['route' => ['account.players.update', $player->id], 'files' =>true, 'method' => 'PUT', 'class'=>'default-form'])}}
                        @include('account.players._form')
                        <div class="control-group mb-0">
                            <a href="{{ route('account.players.index') }}" class="btn --btn-secondary">
                                {{ __('Cancel') }}
                            </a>
                            <button type="submit" class="btn --btn-primary">
                            {{ __('Update') }}
                            </button>
                        </div>
                        {{ Form::close() }}
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
@endsection