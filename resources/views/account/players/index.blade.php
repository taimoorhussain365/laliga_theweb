@inject('arrays','App\Http\Utilities\Arrays')
@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Players') }}
            </h2>
            @include('account._partials._alerts')
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        <h4 class="title">{{ __('Players List') }}</h4>
                        <div class="text-right">
                            
                            <a href="{{ route('account.players.create') }}" class="btn --btn-small --btn-secondary">Add Player</a>
                            
                        </div>
                        
                        <div class="Rtable --collapse --5cols">
                            <div class="Rtable-cell --head">
                                S No.
                            </div>
                            <div class="Rtable-cell --head">
                                Name
                            </div>
                            <div class="Rtable-cell --head">
                                DOB
                            </div>
                            <div class="Rtable-cell --head">
                                Category
                            </div>
                            <div class="Rtable-cell --head">
                                Actions
                            </div>
                            @php($ii = 1)
                            @foreach($players as $player)
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        S No.
                                    </div>
                                    <div class="content">
                                        {{ $ii++ }}
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Name
                                    </div>
                                    <div class="content">
                                        {{ $player->name }}
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        DOB
                                    </div>
                                    <div class="content">
                                        {{ $player->dob }}
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Category
                                    </div>
                                    <div class="content">
                                        
                                        {{ $player->category->name }}
                                    </div>
                                </div>
                            </div>
                            <div class="Rtable-cell">
                                <div class="body">
                                    <div class="header">
                                        Actions
                                    </div>
                                    <div class="content">
                                        <a href="{{ route('account.players.edit', $player->id) }}" class="btn --btn-small --btn-secondary">Edit</a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
@endsection