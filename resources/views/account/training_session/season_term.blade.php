@php
if(!empty($sessions)){
foreach($sessions as $session){
foreach($session->players as $player)
{
$playerdata[]=$player->pivot->training_session_id.'-'.$player->pivot->day_id.'-'.$player->pivot->location_id.'-'.$player->pivot->weeks;
}
}
}
@endphp
@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
	<div class="container-wrapper">
		<article class="inner-content">
			<h2 class="maintitle --black --small">
			{{ __('Training Session ') }}
			</h2>
			
			<div class="row">
				<div class="col-lg-3">
					@include('account._partials.sidebar_menu')
				</div>
				<div class="col-lg-9">
					<div class="alert --warning" role="alert">
						<h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
					</div>
					<section class="box --shadow-box dashboard-content-detail">
						
						<p class="maindesc">
							<strong>Player Name:</strong><br>
							{{$player->name}}
						</p>
						<p class="maindesc">
							<strong>Term Name:</strong><br>
							{{$term->name}}
						</p>
						
						@include('account._partials._alerts')
						{{ Form::open(['route'=>['account.player.training.session.update',$player->id,$term,$emirate], 'class'=>'default-form','id'=>'formDays']) }}
						
						@error('days')
						<span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
						@enderror
						@php $weeks=0; @endphp
						<div class="table-responsive">
							<table class="table --header-color">
								<tr>
									<th>Location</th>
									@foreach($days as $day)
									<th class="text-center">{{$day->name}}</th>
									@endforeach
								</tr>
								
								@foreach($locations as $location)
								
								<tr>
									<td>
										{{$location->name}}<br>
										
									</td>
									
									@foreach($days as $day)
									<td class="text-center">
										@foreach($sessions as $session)
										@if($session->day_id==$day->id && $session->location_id==$location->id)
										@php $data=$session->id.'-'.$day->id.'-'.
										$location->id.'-'.$weeks; @endphp
										<div class="checkbox --center">
											<input type="checkbox" name="days[]" value="{{$session->id}}-{{$day->id}}-{{$location->id}}-{{$weeks}}" {{!empty($playerdata) && in_array($data,$playerdata) ? 'checked':''}}>
											<span class="label"></span>
											<span class="text">
												{{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}
											</span>
										</div>
										
										@endif
										@endforeach
									</td>
									@endforeach
								</tr>
								
								@endforeach
								
								
							</table>
						</div>
						
						
						<button type="submit" class="btn --btn-primary add">{{ __('Save') }}</button>
						<a href="{{ route('account.terms') }}" class="btn --btn-secondary">Cancel</a>
						
						{!! Form::close() !!}
					</section>
				</div>
			</div>
		</article>
	</div>
</section>
<script>
$('.full').click(function () {
var $this = $(this);
if ($this.is(':checked')) {
var week=$(this).attr("data-id");
$(".day-"+week).attr("checked",true);
}
else{
$(':checkbox').prop('checked', false).removeAttr('checked');
}
});

</script>
@endsection