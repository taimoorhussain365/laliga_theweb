@php
if(!empty($sessions)){
foreach($sessions as $session){
foreach($session->players as $player)
{
$playerdata[]=$player->pivot->training_session_id.'-'.$player->pivot->day_id.'-'.$player->pivot->location_id.'-'.$player->pivot->weeks;
}
}
}
@endphp
@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
    <div class="container-wrapper">
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            {{ __('Training Session ') }}
            </h2>
            
            <div class="row">
                <div class="col-lg-3">
                    @include('account._partials.sidebar_menu')
                </div>
                <div class="col-lg-9">
                    <div class="alert --warning" role="alert">
                        <h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
                    </div>
                    <section class="box --shadow-box dashboard-content-detail">
                        
                        <p class="maindesc">
                            <strong>Player Name:</strong><br>
                            {{$player->name}}
                        </p>
                        <p class="maindesc">
                            <strong>Term Name:</strong><br>
                            {{$term->name}}
                        </p>
                        
                        @include('account._partials._alerts')
                        {{ Form::open(['route'=>['account.player.training.session.update',$player->id,$term,$emirate], 'class'=>'default-form']) }}
                        
                        
                        
                        <div class="table-responsive">
                            <table class="table --header-color">
                                <tr>
                                    <th>Location</th>
                                    @foreach($days as $day)
                                    <th class="text-center">{{$day->name}}</th>
                                    @endforeach
                                    <th>Full Week Day</th>
                                </tr>
                                @for($i=1; $i<=$term->weeks;$i++)
                                @foreach($locations as $location)
                                
                                <tr>
                                    <td>
                                        {{$location->name}}<br>
                                        Week-{{$i}}
                                    </td>
                                    
                                    @foreach($days as $day)
                                    <td class="text-center">
                                        @foreach($sessions as $session)
                                        @if($session->day_id==$day->id && $session->location_id==$location->id)
                                        @php $data=$session->id.'-'.$day->id.'-'.
                                        $location->id.'-'.$i; @endphp
                                        <div class="checkbox --center">
                                            <input type="checkbox" name="days[]" class="day-{{$i}}" value="{{$session->id}}-{{$day->id}}-{{$location->id}}-{{$i}}" {{!empty($playerdata) && in_array($data,$playerdata) ? 'checked':''}}>
                                            <span class="label"></span>
                                            <span class="text">
                                                {{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}
                                            </span>
                                        </div>
                                        
                                        @endif
                                        @endforeach
                                    </td>
                                    
                                    @endforeach
                                    <td>
                                        <div class="checkbox --center">
                                            <input type="checkbox" name="is_full_week[]" value="{{$session->id}}-{{$day->id}}-{{$location->id}}-{{$i}}" class="full" data-id={{$i}}>
                                            <span class="label"></span>
                                            <span class="text">
                                                
                                            </span>
                                        </div>
                                        
                                    </td>
                                </tr>
                                
                                @endforeach
                                @endfor
                                
                            </table>
                        </div>
                        
                        
                        <button type="submit" class="btn --btn-primary">{{ __('Save') }}</button>
                        <a href="{{ route('account.terms') }}" class="btn --btn-secondary">Cancel</a>
                        
                        {!! Form::close() !!}
                    </section>
                </div>
            </div>
        </article>
    </div>
</section>
<script>
$('.full').click(function () {
var $this = $(this);
if ($this.is(':checked')) {
var week=$(this).attr("data-id");
$(".day-"+week).attr("checked",true);
}
else{
$(':checkbox').prop('checked', false).removeAttr('checked');

}
});
</script>
@endsection