@extends('layouts.parent')
@section('content')
<section class="sec-padding bg-spgrey">
	<div class="container-wrapper">
		<article class="inner-content">
			<h2 class="maintitle --black --small">
			{{ __('Terms') }}
			</h2>
			<div class="row">
				<div class="row">
					<div class="col-lg-3">
						@include('account._partials.sidebar_menu')
					</div>
					<div class="col-lg-9">
						<div class="alert --warning" role="alert">
							<h3 class="maintitle --white --small text-center mb-0">Wallet: {{ $wallet }} AED</h3>
						</div>
						<section class="box --shadow-box dashboard-content-detail default-form">
							<h4 class="title">{{ __('Terms') }}</h4>
							@include('account._partials._alerts')
							{{ Form::open(['route'=>['account.player.term.store','class'=>'default-form']])}}
							
							{{-- <div class="text-right">
								<a href="{{ route('account.terms') }}" class="btn --btn-small --btn-secondary">Back</a>
							</div> --}}
							
							<div class="Rtable --collapse --5cols">
								<div class="Rtable-cell --head">
									Select Player
								</div>
								<div class="Rtable-cell --head">
									Select Term
								</div>
								<div class="Rtable-cell --head">
									Select Kit Size
								</div>
								<div class="Rtable-cell --head">
									Select  Emirate
								</div>
								<div class="Rtable-cell --head">
									Actions
								</div>
								
								
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Select Player
										</div>
										<div class="content">
											<div class="control-group mb-0">
												<select class="form-field" name="player" id="player" required="required">
													<option value="">--Select Player---</option>
													@foreach($players as $player)
													<option value="{{$player->id}}">{{$player->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Select Term
										</div>
										<div class="content">
											<div class="control-group mb-0">
												<select class="form-field" name="term" required="required">
													<option value="">---Select Term---</option>
													@foreach($terms as $term)
													<option value="{{$term->id}}">{{$term->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Select kit size .
										</div>
										<div class="content">
											<div class="control-group mb-0">
												<select class="form-field" name="kit_size" required="required">
													<option value="">---Select Kit Size---</option>
													@foreach($kit_size as $key => $size)
													<option value="{{$key}}">{{$size}}</option>
													@endforeach
													
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Emirate
										</div>
										<div class="content">
											<div class="control-group mb-0">
												<select class="form-field" name="emirate" required="required">
													<option value="">---Select Emirate---</option>
													@foreach($emirates as $emirate)
													<option value="{{$emirate->id}}">{{$emirate->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Action
										</div>
										<div class="content">
											<button type="submit" class="btn --btn-small --btn-secondary">
											Create Term
											</button>
										</div>
									</div>
								</div>
								
								
							</div>
							
							
							{{ Form::close() }}
							
							
							<div class="Rtable --collapse --6cols">
								<div class="Rtable-cell --head">
									S No.
								</div>
								<div class="Rtable-cell --head">
									Player Name
								</div>
								<div class="Rtable-cell --head">
									Player Category
								</div>
								<div class="Rtable-cell --head">
									Term
								</div>
								<div class="Rtable-cell --head">
									Emirate
								</div>
								<div class="Rtable-cell --head">
									Actions
								</div>
								
								
								@php($i = 1)
								@foreach($players as $player)
								@foreach($player->terms as $term)
								@if($term->pivot->deleted_at==null)
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											S No.
										</div>
										<div class="content">
											{{ $i++ }}
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Player Name
										</div>
										<div class="content">
											{{ $player->name }}
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Category Name
										</div>
										<div class="content">
											{{ $player->category->name }}
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Term Name.
										</div>
										<div class="content">
											{{ $term->name }}
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Emirate
										</div>
										<div class="content">
											{{$player->getEmirate($term->pivot->emirate_id)}}
										</div>
									</div>
								</div>
								<div class="Rtable-cell">
									<div class="body">
										<div class="header">
											Action
										</div>
										<div class="content">
											
											@if($player->trainingSessions()->wherePivot('deleted_at','=',null)->where('term_id',$term->id)->exists())
											<a href="{{route('account.player.training.session.edit',[$player,$term->id,$term->pivot->emirate_id])}}"><button class="btn --btn-small --btn-secondary">Edit Class</button></a>
											@elseif($term->type_id != CAMP_WITHOUTDAYS)
											<a href="{{route('account.player.training.session.edit',[$player,$term->id,$term->pivot->emirate_id])}}"><button class="btn --btn-small --btn-secondary">Add Class</button></a>
											@endif
											
											<button class="btn --btn-small bg-danger fc-white btn-sm" onclick="deleteTerm(
											{{$player->id}},{{$term->id}},{{$term->pivot->id}})">Delete</button>
										</div>
									</div>
								</div>
								@endif
								@endforeach
								@endforeach
								
							</div>
						</section>
					</div>
				</div>
			</article>
		</div>
	</section>
	<script type="text/javascript">
	function deleteTerm(player,term,id)
	{
	if(confirm('Are you sure?')){
	location.href="https://thewebagency.me/account/player/"+player +"/term/"+term+"/id/"+id;
	}else{
	return false;
	}
		}
	</script>
	@endsection