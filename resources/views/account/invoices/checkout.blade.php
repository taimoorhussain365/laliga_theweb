<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <title>Laliga Academy Online</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800,900|Oswald:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://laligacademyonline.com/assets-web/css/style.css"/>

    <style>
        .no-js #loader {
            display: none;
        }

        .js #loader {
            display: block;
            position: absolute;
            left: 100px;
            top: 0;
        }

        .pre-loading {
            position: fixed;
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: 9999;
           
        }
    </style>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!--- Loader ------->

<div class="pre-loading"></div>

<main class="app-container" id="app">

    <section class="registration-layout --main-screen bg-grey">
        <div class="container">
            <section class="form-container">
                <header class="form-header">
                    <picture>
                        <img src="https://laligacademyonline.com/assets-web/images/laliga-academy-logo.png" alt="" class="m-auto">
                    </picture>
                </header>

                <h2 class="form-title text-center">Check Out</h2>

            
                <h6 class="maindesc mb-30">Amount : {{$invoice->amount}} AED</h6>
            

                <form id="frm_payfort_payment_merchant_page2" class="default-form">

                    <div class="control-group">
                        <label class="form-label" for="payfort_fort_mp2_card_holder_name">Name on Card</label>

                        
                        <input type="text" class="form-field" name="card_holder_name" id="payfort_fort_mp2_card_holder_name" placeholder="Card Holder's Name" maxlength="50">

                    </div>

                    <div class="control-group">
                        <label class="form-label" for="payfort_fort_mp2_card_number">Card Number</label>
                        
                        <input type="text" class="form-field" name="card)number" id="payfort_fort_mp2_card_number" placeholder="Debit/Credit Card Number" maxlength="16">
                        
                    </div>
                    <div class="control-group">
                        <label class="form-label" for="payfort_fort_mp2_expiry_month">Expiration Date</label>
                        
                        <div class="row">
                            <div class="col-6">
                                <select class="form-field" name="expiry_month" id="payfort_fort_mp2_expiry_month">
                                    <option value="01">Jan (01)</option>
                                    <option value="02">Feb (02)</option>
                                    <option value="03">Mar (03)</option>
                                    <option value="04">Apr (04)</option>
                                    <option value="05">May (05)</option>
                                    <option value="06">June (06)</option>
                                    <option value="07">July (07)</option>
                                    <option value="08">Aug (08)</option>
                                    <option value="09">Sep (09)</option>
                                    <option value="10">Oct (10)</option>
                                    <option value="11">Nov (11)</option>
                                    <option value="12">Dec (12)</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <select class="form-field" name="expiry_year" id="payfort_fort_mp2_expiry_year">
                                    <?php
                                    $today = getdate();
                                    $year_expire = array();
                                    for ($i = $today['year']; $i < $today['year'] + 11; $i++) {
                                        $year_expire[] = array(
                                            'text'  => strftime('%Y', mktime(0, 0, 0, 1, 1, $i)),
                                            'value' => strftime('%y', mktime(0, 0, 0, 1, 1, $i))
                                        );
                                    }
                                    ?>
                                    <?php
                                    foreach($year_expire  as $year) {
                                        echo "<option value={$year['value']}>{$year['text']}</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="form-label" for="payfort_fort_mp2_cvv">Card CVV</label>
                        
                        <input type="text" class="form-field" name="cvv" id="payfort_fort_mp2_cvv" placeholder="Security Code" maxlength="4">
                        
                    </div>

                    <div class="control-group">
                        <!-- <a class="pull-left btn continue" id="btn_continue" href="javascript:;">Continue</a> -->
                        <button class="btn --btn-primary" id="btn_continue" type="button">Continue</button>

                    </div>
                </form>

        </div>
    </section>
    <script type="text/javascript" src="{{ asset('assets-web/js/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets-web/js/jquery.creditCardValidator.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets-web/js/laligacademyOnlineCheckout.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input:radio[name=payment_option]').click(function () {
                $('input:radio[name=payment_option]').each(function () {
                    if ($(this).is(':checked')) {
                        $(this).addClass('active');
                        $(this).parent('li').children('label').css('font-weight', 'bold');
                        $(this).parent('li').children('div.details').show();
                    }
                    else {
                        $(this).removeClass('active');
                        $(this).parent('li').children('label').css('font-weight', 'normal');
                        $(this).parent('li').children('div.details').hide();
                    }
                });
            });
            $('#btn_continue').click(function (event) {
                // event.preventDefault();
                var paymentMethod = 'cc_merchantpage2';

                var isValid = payfortFortMerchantPage2.validateCcForm();
                if(isValid) {
                    getPaymentPage(paymentMethod);
                } else {
                    return false;
                }
            });
        });
    </script>

</main>
<script>
    $(window).load(function () {
        // Animate loader off screen
        $(".pre-loading").fadeOut("slow");
    });
</script>
</body>
</html>

