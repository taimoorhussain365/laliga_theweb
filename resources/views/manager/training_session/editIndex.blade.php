@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Edit Training Session</h3>
@include('includes.message')
<section class="box">
  <div class="box-header">
    <div class="row">
      <div class="col-md-6">
        <a href="{{route('manager.traning.sessions.create',[$term])}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add Traning Session</button></a>
      </div>
      <div class="col-md-6">
        <div class="default-form">
          <div class="control-group --search-group mb-0 float-right">
            <input type="text" placeholder="Search" class="search-field">
            <div class="search-button">
              <button type="submit" class="form-button">
              <i class="xxicon icon-search"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-body">
    <table class="table --bordered --hover">
      <thead>
        <tr>
          <th><center>S NO</center></th>
          <th><center>Details</center></th>
          <th><center>Day/time</center></th>
          <th><center>Action</center></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($sessions as $key =>$session)
        <tr>
          <td><center>{{$sessions->firstItem() + $key }}</center></td>
          <td><center>Category:{{ $session->category->name}}
            <br>Term:{{ $session->term->name}}
            <br>Location:@if(!empty($session->locations->name)){{ $session->locations->name}}@endif
          </center></td>
          <td><center>{{ $session->day->name }}<br>
            {{date('H:i',strtotime($session->start_time))}}-{{date('H:i',strtotime($session->end_time))}}
          </center></td>
          <td><center><a href="{{ route('manager.training.sessions.edit.details',[$session->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
           <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $session->id }}" id="deleteItem">Delete</button></center></td>
        </tr>
        @endforeach
      </table>
      <ul class="pagination">
            {{$sessions->links()}}
        </ul>
    </div>
  </section>
  <!-- /.content-wrapper -->
  {!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
  {!! Form::close() !!}
  <script>
  var deleteResourceUrl = '{{ url()->current() }}';
  $(document).ready(function () {
  $('.deleteItem').click(function (e) {
  e.preventDefault();
  var deleteId = parseInt($(this).data('deleteId'));
  $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
  if (confirm('Are you sure?')) {
  $('#deleteForm').submit();
  }
  });
  });
  </script>
  @endsection