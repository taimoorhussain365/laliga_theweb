@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Age Category</h3>

<section class="box">

  <div class="box-body">

    {!! Form::model($data, ['route' => ['manager.coach.assignments.update', $id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('manager.coach_assignments._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection