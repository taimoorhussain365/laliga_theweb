@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Add Discount</h3>

<section class="box">

  <div class="box-body">

    {!! Form::open(['route' => 'manager.discounts.store', 'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('manager.discounts._form',['submitBtnText' => 'Add Discount'])

    {!! Form::close() !!}

  </div>

</section>

@endsection