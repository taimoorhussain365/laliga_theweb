@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Discount</h3>

<section class="box">

 	<div class="box-body">

    	{!! Form::model($discount, ['route' => ['manager.discounts.update', $discount->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    		@include('manager.discounts._form',['submitBtnText' => 'Update'])

    	{!! Form::close() !!}

  	</div>

</section>

@endsection