@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Emirates</h3>
@include('includes.message')
<section class="box">
    <div class="box-header">
        <div class="row">
            <div class="col-md-6">
                
                <a href="{{route('admin.emirates.create')}}"><button type="button" class="btn --btn-small --btn-primary"><i class="fa fa-plus"></i>Add Emirate</button></a>
                
                <a href="{{ url()->previous() }}" class="btn --btn-small bg-primary fc-white">Back</a>
            
            </div>

            <div class="col-md-6">
                <div class="default-form">
                    <div class="control-group --search-group mb-0 float-right">
                        <input type="text" placeholder="Search" class="search-field">
                        
                        <div class="search-button">
                            <button type="submit" class="form-button">
                                <i class="xxicon icon-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="box-body">
        <table class="table --bordered --hover">
      
        <tr>
            <th><center>S NO</center></th>
            <th><center>Name</center></th>
            <th><center>Is Active</center></th>
            <th><center>Action</center></th>
        </tr>
        
        @foreach($emirates as $emirate)
        <tr>
            <td><center>{{ $no++ }}</center></td>
            <td><center>{{ $emirate->name }}</center></td>
            <td><center>
                @if($emirate->is_active)
                <p>Active</p>
                @else
                <p>InActive</p>
                @endif</center>
            </td>
            <td>
                <center><a href="{{ route('admin.emirates.edit',[$emirate->id]) }}"><button type="button" class="btn --btn-small bg-secondary fc-white">Edit</button></a>
                <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $emirate->id }}" id="deleteItem">Delete</button>
            </td>
        </tr>
        @endforeach    
        </table>

    </div>
</section>
<!-- /.content-wrapper -->
{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}
{!! Form::close() !!}
<script>
var deleteResourceUrl = '{{ url()->current() }}';
$(document).ready(function () {
$('.deleteItem').click(function (e) {
e.preventDefault();
var deleteId = parseInt($(this).data('deleteId'));
$('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);
if (confirm('Are you sure?')) {
$('#deleteForm').submit();
}
});
});
</script>
@endsection