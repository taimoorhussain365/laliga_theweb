@extends('layouts.manager')
@section('content')
<h3 class="pagetitle">Edit Age Category</h3>
<section class="box">
	<div class="box-body">
		{!! Form::model($category, ['route' => ['manager.age.categories.update', $category->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}
		@include('admin.age_category._form',['submitBtnText' => 'Update'])
		{!! Form::close() !!}
	</div>
</section>
@endsection