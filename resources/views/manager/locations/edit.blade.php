@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Location</h3>

<section class="box">

  <div class="box-body">

    <!-- /.box-header -->

    {!! Form::model($location, ['route' => ['manager.locations.update', $location->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('manager.locations._form',['submitBtnText' => 'Update'])

    {!! Form::close() !!}

  </div>

</section>

@endsection