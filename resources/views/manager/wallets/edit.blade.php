@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Edit Amount</h3>



<section class="box">

	<div class="box-body">

    	{!! Form::model($wallet,['route' => ['manager.wallets.update',$wallet->id], 'files' => 'true', 'method' => 'PUT', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    		@include('manager.wallets._form',['submitBtnText' => 'Update'])

    	{!! Form::close() !!}

 	</div>

</section>

@endsection