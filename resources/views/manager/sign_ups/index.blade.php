@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">{{$heading}}</h3>

@include('includes.message')



<section class="box">

    <div class="box-header">

        <a href="{{route('manager.signups.create',$id)}}" class="btn --btn-small --btn-primary mb-0">Add New Enquiry</a>

    </div>



    <div class="box-body">

        {!! Form::open(['route' => ['manager.signups.search',$id], 'files' => 'true', 'method' => 'GET', 'class' => 'default-form', 'enctype'=>"multipart/form-data"]) !!}



        <div class="row">

            <div class="col-md-12">

                <div class="control-group">

                    {!! Form::text('keyword', null, ['class'=>'form-field','placeholder' => 'Search By Keyword']) !!}

                </div>

            </div>



            <div class="col-md-4">

                <div class="control-group">

                    {!! Form::text('created_at', null, ['class'=>'form-field','placeholder' => 'Search By Created Date','id'=>'created_at']) !!}

                </div>



                <div class="control-group">

                    <select class="form-field days" name="category_id">

                        <option value="">Multiple Category Selection</option>

                        @foreach($categories as $category)

                            <option value="{{$category->id}}">{{$category->name}}</option>

                        @endforeach

                    </select>

                </div>



                <div class="control-group">

                    {!! Form::text('track_no', null, ['class'=>'form-field','placeholder' => 'Search By Track No']) !!}

                </div>

            </div>



            <div class="col-md-4">

                <div class="control-group">

                    <select class="form-field" name="source_id" >

                        <option value="">Multiple Source Selection</option>

                        @foreach($sources as $key=> $source)

                        <option value="{{$key}}">{{$source}}</option>

                        @endforeach

                    </select>

                </div>





                <div class="control-group">

                    <select class="form-field" name="status_id" >

                        <option value="">Multiple Status Selection</option>

                        @foreach($status as $key=> $currentStatus)

                        <option value="{{$key}}">{{$currentStatus}}</option>

                        @endforeach

                    </select>

                </div>



                <div class="control-group">

                    <select class="form-field" name="comments">

                        <option value="">Select Post Comments</option>

                        <option value="1">Yes</option>

                        <option value="2">No</option>

                    </select>

                </div>

            </div>



            <div class="col-md-4">

                <div class="control-group">

                    <select class="form-field" name="user_id">

                        <option value="">Multiple Users</option>

                        @foreach($users as $user)

                        <option value="{{$user->id}}">{{$user->name}}</option>

                        @endforeach

                    </select>

                </div>



                <div class="control-group">

                    <select class="form-field" name="term_id" >

                        <option value="">Multiple Interest Selection</option>

                        @foreach($terms as $term)

                        <option value="{{$term->id}}">{{$term->name}}</option>

                        @endforeach

                    </select>

                </div>



                <div class="control-group">

                    {!! Form::text('created_to', null, ['class'=>'form-field','id' => 'start_date','placeholder' => 'Created To','id'=>'created_to']) !!}

                </div>



            </div>

        </div>



        <button type="submit" class="btn --btn-primary">Submit</button>

        {!! Form::close() !!}



        <table class="table --bordered --hover mt-30">

            <tr>

                <th>Track NO</th>

                <th>Details</th>

                <th>Status</th>

                <th>Source</th>

                <th>Team Member</th>

                <th>Comments</th>

                <th>Action</th>

            </tr>

            

            @if(!empty($search_query))

            @foreach ($search_query as $sign_up)

            <tr>

                <td>{{ $no++ }}</td>

                <td>

                    Name:{{ $sign_up->name }}<br>

                    Email:{{ $sign_up->email }}<br>

                    Mobile:{{ $sign_up->mobile }}<br>

                    Category:{{ $sign_up->category->name }}<br>

                </td>

                <td>

                    @foreach ($status as $key=>$currentStatus)

                    @if($key==$sign_up->status_id)

                        {{$currentStatus}}

                    @endif

                    @endforeach

                </td>

                <td>

                    @foreach ($sources as $key=>$source)

                    @if($key==$sign_up->source_id)

                        {{$source}}

                    @endif

                    @endforeach

                </td>

                <td>

                    {{$sign_up->user->name}}

                </td>

                <td>

                    {{$sign_up->comments}}

                </td>

                <td>

                    <a href="{{ route('manager.signups.edit',[$sign_up->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>

                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $sign_up->id }}" id="deleteItem">Delete</button>

                </td>

            </tr>

            @endforeach

            @else

            @foreach ($sign_ups as $sign_up)

            <tr>

                <td>{{ $no++ }}</td>

                <td>

                    Name:{{ $sign_up->name }}<br>

                    Email:{{ $sign_up->email }}<br>

                    Mobile:{{ $sign_up->mobile }}<br>

                    Category:{{ $sign_up->category->name }}<br>

                </td>

                <td>

                    @foreach ($status as $key=>$currentStatus)

                    @if($key==$sign_up->status_id)

                        {{$currentStatus}}

                    @endif

                    @endforeach

                </td>

                <td>

                    @foreach ($sources as $key=>$source)

                    @if($key==$sign_up->source_id)

                        {{$source}}

                    @endif

                    @endforeach

                </td>

                <td>

                    {{$sign_up->user->name}}

                </td>

                <td>

                    {{$sign_up->comments}}

                </td>

                <td>

                    <a href="{{ route('manager.signups.edit',[$sign_up->id]) }}" class="btn --btn-small bg-secondary fc-white">Edit</a>

                    <button type="button" class="deleteItem btn --btn-small bg-danger fc-white" data-delete-id="{{ $sign_up->id }}" id="deleteItem">Delete</button>

                </td>

            </tr>

            @endforeach

            @endif

        </table>

    

    </div>

</section>



{!! Form::open(['method' => 'delete', 'id' => 'deleteForm']) !!}

{!! Form::close() !!}



<script>



    $(document).ready(function () {



        $('.deleteItem').click(function (e) {

            e.preventDefault();

            var deleteResourceUrl = '{{ url()->current() }}';

            var deleteId = parseInt($(this).data('deleteId'));



            $('#deleteForm').attr('action', deleteResourceUrl + '/' + deleteId);

            if (confirm('Are you sure?')) {

                $('#deleteForm').submit();

            }

        });

    });



    var dateToday = new Date();

    $( "#created_at" ).datepicker({

    });

    $( "#created_to" ).datepicker({ 

    });



</script>

@endsection