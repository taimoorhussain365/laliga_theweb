@extends('layouts.manager')

@section('content')

<h3 class="pagetitle">Add</h3>

<section class="box">

    {!! Form::open(['route'=>['manager.signups.store',$id],'files' => 'true', 'method' => 'POST', 'enctype'=>"multipart/form-data",'class'=>"default-form"]) !!}

    @include('manager.sign_ups._form',['submitBtnText' => 'Add'])

    {!! Form::close() !!}

</section>

@endsection