<div class="box-body">

    <div class="control-group{{ $errors->has('name') ? ' has-error' : '' }}">
        {!! Form::label('name', 'Name', ['class'=>'form-label',]) !!}

        {!! Form::text('name', null, ['class'=>'form-field','required' => 'required']) !!}
           
        @if ($errors->has('name'))
            <span class="error-msg">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>

    <div class="control-group{{ $errors->has('dob') ? ' has-error' : '' }}">
        {!! Form::label('dob', 'DOB', ['class'=>'form-label']) !!}

        {!! Form::text('dob', null, ['class'=>'form-field','required' => 'required','id'=>'dob','autocomplete'=>'off']) !!}
           
        @if ($errors->has('dob'))
            <span class="error-msg">
                <strong>{{ $errors->first('dob') }}</strong>
            </span>
        @endif
    </div>
    
    
    <div class="control-group{{ $errors->has('email') ? ' has-error' : '' }}">
        {!! Form::label('email', 'Email', ['class'=>'form-label',]) !!}

        {!! Form::text('email', null, ['class'=>'form-field','required' => 'required']) !!}
           
        @if ($errors->has('email'))
            <span class="error-msg">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('mobile') ? ' has-error' : '' }}">
        {!! Form::label('mobile', 'Mobile', ['class'=>'form-label',]) !!}

        {!! Form::text('mobile', null, ['class'=>'form-field','required' => 'required']) !!}

        @if ($errors->has('mobile'))
            <span class="error-msg">
                <strong>{{ $errors->first('mobile') }}</strong>
            </span>
        @endif
    </div>
 
    <div class="control-group{{ $errors->has('term_id') ? ' has-error' : '' }}">
        {!! Form::label('term_id', 'Term', ['class'=>'form-label']) !!}

        <select class="form-field" name="term_id" required>
            <option value="">Select Term</option>
            @foreach ($terms as $term)
            <option value="{{$term->id}}" {{isset($signUp->term_id) && $signUp->term_id==$term->id ? 'selected':''}}>{{$term->name}}</option>
            @endforeach
        </select>

        @if ($errors->has('term_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('term_id') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
        {!! Form::label('category_id', 'Category', ['class'=>'form-label']) !!}

        <select class="form-field" name="category_id" required>
            <option value="">Select Category</option>
            @foreach ($categories as $category)
            <option value="{{$category->id}}" {{isset($signUp->category_id) && $signUp->category_id==$category->id ? 'selected':''}} >{{$category->name}}</option>
            @endforeach
        </select>

        @if ($errors->has('category_id'))
        <span class="error-msg">
            <strong>{{$errors->first('category_id') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('status_id') ? ' has-error' : '' }}">

        {!! Form::label('status_id', 'Status', ['class'=>'form-label']) !!}

        <select class="form-field" name="status_id" required>
            <option value="">Select Status</option>
            @foreach ($status as $key=> $currStatus)
            <option value="{{$key}}" {{isset($signUp->status_id) && $signUp->status_id==$key ? 'selected':''}}>{{$currStatus}}</option>
            @endforeach
        </select>

        @if ($errors->has('status_id'))
        <span class="error-msg">
            <strong>{{$errors->first('status_id') }}</strong>
        </span>
        @endif
    </div>
       
    <div class="control-group{{ $errors->has('source_id') ? ' has-error' : '' }}">

        {!! Form::label('source_id', 'Source', ['class'=>'form-label']) !!}

        <select class="form-field" name="source_id" required>
            <option value="">Select Source</option>
            @foreach ($sources as $key=>$source)
            <option value="{{$key}}" {{isset($signUp->source_id) && $signUp->source_id==$key ? 'selected':''}}>{{$source}}</option>
            @endforeach
        </select>

        @if ($errors->has('source_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('source_id') }}</strong>
        </span>
        @endif
    </div>
         
    <div class="control-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
        {!! Form::label('user_id', 'Team Member', ['class'=>'form-label']) !!}

        <select class="form-field" name="user_id" required>
            <option value="">Select User</option>
            @foreach ($users as $user)
            <option value="{{$user->id}}" {{isset($signUp->user_id) && $signUp->user_id==$user->id ? 'selected':''}}>{{$user->name}}</option>
            @endforeach
        </select>

        @if ($errors->has('user_id'))
        <span class="error-msg">
            <strong>{{ $errors->first('user_id') }}</strong>
        </span>
        @endif
    </div>
    
    <div class="control-group{{ $errors->has('comments') ? ' has-error' : '' }}">

        {!! Form::label('comments', 'Admin Comments', ['class'=>'form-label',]) !!}

        {!! Form::textarea('comments', null, ['class'=>'form-field']) !!}

        @if ($errors->has('comments'))
            <span class="error-msg">
                <strong>{{ $errors->first('comments') }}</strong>
            </span>
        @endif
    </div>
     
    <div class="control-group{{ $errors->has('is_active') ? ' has-error' : '' }}">

        <div class="checkbox">
            {!! Form::checkbox('is_active', 1, isset($signUp) ? $signUp->is_active : true) !!}

            <span class="label"></span>
            <span class="text">{!! Form::label('is_active', 'Activate', ['class' => 'form-label']) !!}</span>
        </div>
        
        @if ($errors->has('is_active'))
            <span class="error-msg">
                <strong>{{ $errors->first('is_active') }}</strong>
            </span>
        @endif
    </div>

    <div class="control-group">
        <input type="submit" value="{{ $submitBtnText }}" class="btn --btn-small bg-secondary fc-white">
        <a href="{{ url()->previous() }}" class="btn --btn-small bg-danger fc-white">Cancel</a>
    </div>

</div>

<script> 

    var dateToday = new Date();
        $( "#dob" ).datepicker({    
    });

</script>