@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Scholarship</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    US Soccer Scholarships
                </h2>

                <p class="maindesc">
                    At LaLiga Academy, we want to take part in the development of our athletes, helping them to <strong>optimize their opportunities for success in life through football & education.</strong> This is why we have officially partnered with AGM Education and Mooxye the digital Scholarship Search Platform. <strong>AGM Education is the biggest athletic recruiting and consulting company in Europe,</strong> with 15 years experience helping international athletes in finding universities and scholarships in the US. Together we can provide educational opportunities, through football, to all players who are part of the <strong>LaLiga Academy Family.</strong> <br><br>

                    AGM Education is the <strong>exclusive partner of LaLiga</strong> and all 42 professional teams under their umbrella. AGM Education offers an end-to-end solution for players looking to pursue their academic and athletic endeavours in the United States. Through their comprehensive <strong>recruiting and consulting platform,</strong> and their state of the art Digital Mooxye Scholarship Recruitment and Education platform, they have had success with over <strong>1,800 student-athletes</strong> and continuously help more every year. <br><br>

                    How does an athlete begin the pathway to obtaining a US football scholarship?
                </p>

                <figure class="mb-30">
                    <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/scouting/skill-path.png') }}" alt="LaLiga Academy" class="lazy">
                </figure>

                <div class="Rtable --collapse --3cols text-center scholarship-table">
                    <div class="Rtable-cell --head">
                        Organization
                    </div>

                    <div class="Rtable-cell --head">
                        Number of Universities
                    </div>

                    <div class="Rtable-cell --head">
                        Maximum Number of Scholarships Offered per Team

                        <div class="row two-box mt-10">
                            <div class="col-6 female bg-black">Female</div>
                            <div class="col-6 male bg-black">Male</div>
                        </div>
                    </div>

                    <?php /** First Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                NCAA I
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                331
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> 14</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>9.9</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /** Second Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                NCAA II
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                257
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> 9.9</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>9</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /** Third Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                NCAA III
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                446
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> -</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>-</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /** Forth Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                NAIA
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                204
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> 12</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>12</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /** Fifth Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                NJCAA
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                236
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> 18</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>18</span></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php /** Six Box **/ ?>
                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Oraganization
                            </div>

                            <div class="content">
                                OTHER
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Number of Universities
                            </div>

                            <div class="content">
                                193
                            </div>
                        </div>
                    </div>

                    <div class="Rtable-cell">
                        <div class="body">
                            <div class="header">
                                Maximum Number of Scholarships Offered per Team
                            </div>

                            <div class="content">
                                <div class="row two-box">
                                    <div class="col-6 female"><strong class="d-block d-lg-none">Female</strong> -</div>
                                    <div class="col-6 male"><strong class="d-block d-lg-none">Male</strong> <span>-</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <div class="box --scholarship-box">
                            <h6 class="title">Average Scholarship</h6>
                            p.
                        </div>
                    </div>
                </div>
                
            </article>
        </div>
    </section>


@endsection
