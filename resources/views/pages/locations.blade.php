@extends('layouts.app')

@section('content')
    <section class="hero-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h2 class="subtitle">WELCOME TO</h2>
            <h1 class="title">LaLiga’s Only Official Academy in the UAE</h1>
        </div>
    </section>

    <section class="about-laliga sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <article class="inner-content">
                        <h2 class="maintitle --black">
                            <span>about</span>
                            laliga
                        </h2>

                        <p class="maindesc">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
                    </article>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img data-src="{{ asset('assets-web/images/about-laliga.png') }}" alt="LaLiga Academy" class="lazy about-img">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="gallery-laliga sec-padding" data-img="url({{ asset('assets-web/images/half-beat-png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <h2 class="maintitle --white">
                        <span>our</span>
                        gallery
                    </h2>
                </div>

                <div class="col-md-6">
                    <div class="gallery-tabs">
                        <ul class="unstyled inline">
                            <li data-targettab="tab-photo" class="current">Photo</li>
                            <li data-targettab="tab-video">Video</li>
                        </ul>
                    </div>
                </div>
            </div>

            <article class="tab-photo inner-content hidden show">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img1.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img1.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>

                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img2.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img2.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img3.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img3.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img1.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img1.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img2.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img2.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img3.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img3.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>
                </div>
            </article>

            <article class="tab-video inner-content hidden">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img1.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img1.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>

                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="box --default-box --hover">
                            <figure>
                                <img class="lazy" data-src="{{ asset('assets-web/images/gallery-img2.jpg') }}" alt="LaLiga Academy">

                                <a href="{{ asset('assets-web/images/gallery-img2.jpg') }}" data-fancybox class="xxicon icon-maximize-2"></a>
                            </figure>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>


    <section class="tvc-laliga">

        <video autoplay muted loop class="tvc-video">
            <source src="{{ asset('assets-web/video/LaLiga-Academy.mp4') }}" type="video/mp4">
            Your browser does not support the video tag.
        </video>

        <div class="overlay"></div>
        <h2 class="content"><span class="red">WITHOUT A BEAT</span>, THERE'S NO HEART. WITHOUT A HEART, THERE'S NO PASSION. WITHOUT PASSION, THERE'S NO EMOTION. WITHOUT EMOTION, <sapn class="red">THERE'S NO FOOTBALL.</span></h2>

    </section>

    <section class="category-laliga sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <h2 class="maintitle --black">
                <span>boys &amp; girls</span>
                age categories
            </h2>

            <div class="Rtable --collapse --7cols --category-table">
                <div class="Rtable-cell --head">
                    U6
                </div>

                <div class="Rtable-cell --head">
                    U8
                </div>

                <div class="Rtable-cell --head">
                    U10
                </div>

                <div class="Rtable-cell --head">
                    U12
                </div>

                <div class="Rtable-cell --head">
                    U14
                </div>

                <div class="Rtable-cell --head">
                    U16
                </div>

                <div class="Rtable-cell --head">
                    U18
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U6
                        </div>

                        <div class="content">
                            January 2015 <br> June 2016
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U8
                        </div>

                        <div class="content">
                            January 2013 <br> December 2014
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U10
                        </div>

                        <div class="content">
                            January 2011 <br> December 2012
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U12
                        </div>

                        <div class="content">
                            January 2009 <br> December 2010
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U14
                        </div>

                        <div class="content">
                            January 2007 <br> December 2008
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U16
                        </div>

                        <div class="content">
                            January 2005 <br> December 2006
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            U18
                        </div>

                        <div class="content">
                            January 2003 <br> December 2004
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="training-laliga sec-padding">
        <div class="container-wrapper">
            <h2 class="maintitle --white">
                <span>training</span>
                schedule
            </h2>

            <div class="row">
                <div class="col-md-4">
                    <div class="box --schedule-box">
                        <figure>
                            <img class="lazy" data-src="{{ asset('assets-web/images/program-img1.jpg') }}" alt="LaLiga Academy">
                        </figure>

                        <p class="weeks">15 weeks</p>

                        <div class="content">
                            <p class="term">Term 1</p>
                            <p class="date">30 August  2020 <br> 10 December 2020</p>
                            <p class="detail">Christmas Break <br>13 Dec 2020 - 2 January 2021</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box --schedule-box">
                        <figure>
                            <img class="lazy" data-src="{{ asset('assets-web/images/program-img2.jpg') }}" alt="LaLiga Academy">
                        </figure>

                        <p class="weeks">12 weeks</p>

                        <div class="content">
                            <p class="term">Term 2</p>
                            <p class="date">03 January 2021 <br> 25 March 2021</p>
                            <p class="detail">Spring Break <br>28 March 2021 - 10 April 2021</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box --schedule-box">
                        <figure>
                            <img class="lazy" data-src="{{ asset('assets-web/images/program-img1.jpg') }}" alt="LaLiga Academy">
                        </figure>

                        <p class="weeks">12 weeks</p>

                        <div class="content">
                            <p class="term">Term 3</p>
                            <p class="date">11 April 2021 <br> 08 July 2021</p>
                            <p class="detail">Eid Break <br>09 - 15 May 2021</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
