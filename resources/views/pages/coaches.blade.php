@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/coach-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Coaches</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <h3 class="maintitle lh-large">LaLiga has selected the finest coaches for LaLiga Academy, holding the highest UEFA Pro-certification and a wealth of experience with LaLiga clubs youth teams in Spain and internationally.</h3>

            <div class="row">

                <div class="col-lg-12">
                    <div class="box --coach-box --hover-img">
                        <div class="row no-gutters">
                            <div class="col-lg-4">
                                <figure class="">
                                    <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/yago.jpg') }}" alt="LaLiga Academy" class="lazy">
                                </figure>
                            </div>

                            <div class="col-lg-8">
                                <div class="content">
                                    <h4 class="coach-name">YAGO AGUILAR</h4>
                                    <h6 class="coach-title">Head Coach</h6>
                                    <p class="desc">Coach Yago will be leading our team of coaches this season and brings to our program over 15 years of professional experience.  He is a UEFA Pro Licensed Football Coach and hold a Football Director License from the Spanish FA.  Yago has spent the core of his career coaching youth players in LaLiga clubs and has extensive experience in developing youth players and placing them into professional clubs (including FC Barcelona players such as Sergi Roberto and Aleix Vidal).  International Yago spent 2 years in Georgia as the Youth Football Director at FC Locomotive Tbilisi (on a partnership with FC Barcelona) and 3 years in Hong Kong as the Youth Football Director at Kitchee S.C.  Yago has also worked  with the Nike Academy and the Mediterranean International Cup. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/zarco.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">JOSE MARIA ZARCO</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Coach Zarco has been with LaLiga Academy in the UAE from 2017 prior to that he worked in LaLiga clubs in Spain coaching youth teams at Granada FC and Athletic de Coin FC.  He holds a Masters Degree in Strength, Conditioning & Rehab and a Bacherlors in Sports Science and is UEFA A certified.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/alberto.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">Alberto Pérez de Ciriza</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Coach Alberto is in his 2nd year with LaLiga Academy and is UEFA Pro certified and holds Masters degree in Coaching Football Fitness & Masters in Football Performance.  He has over 9 years of football experience coaching at various LaLiga clubs including Mulier FC and FC Ardoi.  He has spent the last year specialized as Head Coach of the women’s team at Mulier Futbol Club Navarra.  He is also a specialized Goal Keeper coach.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/moreno.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">ANTONIO JESUS MORENO</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Coach Moreno is in his 2nd season with LaLiga academy.  He is a UEAFA Pro certified and has over 13 years of experience in coaching and educating children.  He has trained youth football teams at Athletic Villanueva FB for many years and has international experience coaching in China and teaching in China.  Moreno hold a degree as a Physical Education teacher. </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/javler-dxb.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">JAVIER MARTINEZ GONZALEZ</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Coach Javier has been working in the football industry for over 6 years having trained youth in Spain and in China with LaLiga.  He holds a Masters Degree in Personal Training in the field of Football and a Bachelors degree in Sports and he also holds a UEFA ‘A’ Coaching License.  Javier is also well versed in strength & conditioning training and recovery.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/jorge.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">JORGE HARO LOSADA</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Coach Jorge is a UEFA Pro certified coach and brings over 7 years of coaching experience to LaLiga Academy.  He has trained youth football teams at LaLin Soccer School in Spain working with children from 4 -16.   Jorge has a thorough academic background, holding a degree as a Specialist Teacher in Sports.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/sergio.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">SERGIO HIDALGO MORA</h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Holding the highest qualification with a UEFA Pro License, Sergio possesses 8 years of coaching experience having worked in Spain, Kuwait, Ireland, Denmark and the UAE. Sergio was a professional football in his youth and brings a wealth of personal experience as well as professional coaching experience to LaLiga Academy.  He hold a degree in Sports Science.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/javler-ab.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">JAVIER HERNANDEZ HERVAS </h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Holding a UEFA Pro License, Coach Javier enters his third season with La Liga Academy.  He brings over 8 years of coaching experience with him, the majority of those years in Spain training children in LaLiga Clubs and internationally he spent a year coaching in China with LaLiga. Javier was a professional football player with 15 years of experience 5 of which he was playing in Real Madrid’s youth team. Javier holds a Masters Degree in Sports Management and a Bachelors in the Science of Physical Activity.</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/mato.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">JOSE ANGEL MATO </h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Jose Mato is in his 2nd season with LaLiga Academy and holds a UEFA Pro License.  He has over 10 years of coaching experience having led grass roots youth football initiatives in Spain as well as coaching youth in various Spanish clubs including Santiago CG.  Internationally he has coached in China and Iran. Jose Angle holds a Masters Degree in Coaching and Emotional Intelligence. </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/alejandro.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">ALEJANDRO SIRVENT SERRANO </h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">Alejandro has been a football coach for over 8 years having trained  youth teams in Spanish clubs including A.D. Arganda and C.D. Avance in Madrid.  Internationally he played an integral role in the launch of LaLiga Academy in Cairo last season and has been transferred to the UAE.  He holds a UEFA Pro License, a Masters in Secondary School Education and a Bachelors in the Science of Physical Activity.  </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="box --coach-box --hover-img">
                        <figure>
                            <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/coaches/andrew.jpg') }}" alt="LaLiga Academy" class="lazy">
                        </figure>
                        <div class="content">
                            <h4 class="coach-name">ANDREW FOWLEY </h4>
                            <h6 class="coach-title">Coach</h6>
                            <p class="desc">A professionally licensed coach with fifteen years’ experience working in Europe, the United States, and the United Arab Emirates. Andrew holds a UEFA ‘A’ Coaching License and has been in the UAE for 11 years, with coaching experience with the likes of Arsenal Soccer Schools, Al Jazira FC, and as Head of Football for Zayed Sports City. </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection
