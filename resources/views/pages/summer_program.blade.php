@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/summer-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Summer Camp</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h3 class="mb-20">
                    SUMMER CAMP AT DUBAI SPORTS WORLD (DUBAI WORLD TRADE CENTRE) <br>
                    <small>01 JULY 2020 - 27 AUGUST 2020</small>
                </h3>

                <p class="maindesc">
                    LaLiga Academy will deliver a highly interactive and engaging camp for boys and girls 5-16 with a 360 approach to player development. The camp will run 5 days per week at Dubai Sports World.
                </p>

                <ul class="unstyled check-list">
                    <li>Learn to play football the Spanish way</li>
                    <li>Football training delivered by UEFA Pro-certified LaLiga coaches</li>
                    <li>Improve fitness and stamina to boost your performance</li>
                    <li>Enhance your football IQ</li>
                </ul>

                <p class="maindesc">At LaLiga Academy's Summmer Camp, players will enjoy football while developing their technical skills, fitness, game IQ and leadership qualities.</p>

                <a href="/register" class="btn --btn-primary">Register Now</a>
                <span class="mr-2 ml-2 fw-bold">OR</span>
                <a href="/login" class="btn --btn-primary">Login</a>
            </article>
        </div>
    </section>

    <section class="gallery-laliga sec-padding" data-img="url({{ asset('assets-web/images/half-beat.png') }})">
        <div class="container-wrapper">
            <h2 class="maintitle --white">
                <span>TRAINING</span>
                SCHEDULE
            </h2>

            <div class="Rtable --collapse --2cols --category-table --bg-white">
                <div class="Rtable-cell --head">
                    Age Category
                </div>

                <div class="Rtable-cell --head">
                    Time
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Age Category
                        </div>

                        <div class="content">
                            U12
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Time
                        </div>

                        <div class="content">
                            1:00 pm - 2:00 pm
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Age Category
                        </div>

                        <div class="content">
                            U14
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Time
                        </div>

                        <div class="content">
                            2:00 pm - 3:00 pm
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Age Category
                        </div>

                        <div class="content">
                            U16 / U18
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Time
                        </div>

                        <div class="content">
                            3:00 pm - 4:00 pm
                        </div>
                    </div>
                </div>
            </div>

            <h2 class="maintitle --white">
                Fees
            </h2>

            <h5 class="fc-white">1 JULY 2020 - 27 AUGUST 2020</h5>

            <div class="Rtable --collapse --5cols --category-table --bg-white">
                <div class="Rtable-cell --head">
                    Summer Camp
                </div>

                <div class="Rtable-cell --head">
                    Daily
                </div>

                <div class="Rtable-cell --head">
                    Weekly
                </div>

                <div class="Rtable-cell --head">
                    4 Weeks
                </div>

                <div class="Rtable-cell --head">
                    8 Weeks
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Summer Camp
                        </div>

                        <div class="content">
                            U12, U14, U16, U18
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Daily
                        </div>

                        <div class="content">
                            AED 105
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            Weekly
                        </div>

                        <div class="content">
                            AED 495
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            4 Weeks
                        </div>

                        <div class="content">
                            AED 1795
                        </div>
                    </div>
                </div>

                <div class="Rtable-cell">
                    <div class="body">
                        <div class="header">
                            8 Weeks
                        </div>

                        <div class="content">
                            AED 2795
                        </div>
                    </div>
                </div>
                
            </div>

        </div>
    </section>


@endsection
