@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Terms &amp; Conditions</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">

                <p class="maindesc">APPLICABLE TO PLAYERS OF LALIGA ACADEMY AND THEIR PARENTS / GUARDIANS</p>

                <h2 class="maintitle --black --small">
                    OBJECTIVE
                </h2>

                <p class="maindesc"><strong>Inspiratus Consulting Ltd.</strong> (hereinafter, the <strong>"Organizer"</strong>), with registered office at Office No. 905, Suntech Tower, Dubai Silicon Oasis, P.O. Box 341312, Dubai, United Arab Emirates, organize <strong>"LaLiga Academy"</strong> or the <strong>"Academy"</strong>, which has been contracted by <strong>Liga Nacional de Fútbol Profesional</strong>, Vat no. G-78069762, Calle Hernández de Tejada, 10, 28027 Madrid Spain (hereinafter, <strong>"LaLiga"</strong>). <br><br>

                Subject to what is detailed in the following clauses, all those who want to attend the Academy need to register through this website. The Academy shall be governed by the following clauses:</p>

                <h2 class="maintitle --black --small">
                    1. LALIGA ACADEMY
                </h2>

                <p class="maindesc">LaLiga Academy is a comprehensive sports educational program aimed at promoting football excellence thorough nurturing youth football talents, providing world-class coaching and training services and serving as an advanced scouting platform for football university programs, professional football academies and professional football clubs.</p>


                <h2 class="maintitle --black --small">
                    Registration
                </h2>

                <p class="maindesc">Registration shall be completed once the following have been submitted online via ae.laligacademy.com: <br>
                1. Completed registration form including the acceptance of the terms and conditions. <br>
                2. Payment of a registration fee of AED350. <br>
                3. Payment for the relevant term/season. Payment instalments will be accepted for payments for the full season. <br><br>
                Admission will be subject to the availability of a place at the requested training location. <br>
                The Player’s enrolment will be only considered as confirmed upon a written confirmation sent to the Parent / Guardian. In case there is no availability in the requested age category, a full refund will be given (including the registration fee of AED350). <br> <br>

                The siblings discount of 15 % for the second child and 25% for the third and further children shall apply only when all siblings are registered as part of the same registration.</p>


                <h2 class="maintitle --black --small">
                    Cancelations and Refunds
                </h2>

                <p class="maindesc">The registration fee of AED350 is not refundable under any cancellation circumstances. <br><br>

                A bank charge of 3% will be deducted for approved refunds if payments were made by credit card. <br><br>

                Confirmed registration can be cancelled and the payment minus the registration fee of AED 350 shall be refunded using the original mode of payment if written notice is given to the Organizer at laligacademy@inspirat.us at a minimum 7 days prior to the start of the relevant term. <br><br>

                In case of a cancellation where the Player has enrolled for a full season but cancels during the first term, the Player will be charged the full term fee. <br><br>

                No refunds for single term enrolments will be given after the start of the term. A balance of the remaining sessions will be refunded only on the ground of a serious medical reason preventing the Player from participating for more than six weeks which has been established by providing a medical certificate or based on a proof submitted to the Organizer that the participants is relocating to another country. <br><br>

                Any cancellation is effective upon the receipt of the written cancellation notice and any and all necessary supporting documentation. Refund will be processed in line with the terms and conditions of the payment provider. </p>

                
                <h2 class="maintitle --black --small">
                    General
                </h2>

                <p class="maindesc">The Organizer reserves the right to place Players in the correct age category or in an alternate age category based on the skills level as determined by the Academy coaches. <br><br>

                Should the Player wish to relocate from one location to another, we will do our best to accommodate the request but cannot guarantee that there will be availability. <br><br>

                The Organizer reserves the right to cancel a course, change the term schedule or location should that be necessary for any reason. <br><br>

                No makeup sessions will be offered for missed classes. <br><br>

                Academy training sessions are done in closed sessions reserved for the Players, their parents/guardians and identified friends and family members. Any video recording or photography during the training session is strictly prohibited. <br><br>

                Neither you nor the participant shall disclose or use any aspect of our course for commercial purposes. <br><br>

                All classes will be conducted in English.</p>

                <h2 class="maintitle --black --small">
                    2. ACCEPTANCE
                </h2>

                <p class="maindesc">I as Parent / Guardian of the Player identified herein have read, understood and accept the LaLiga Academy terms and conditions fully and without limitation. I give my consent / grant permission for the Player to participate in LaLiga Academy and any football/soccer event organized by the Academy, which may include LaLiga Academy-related trips by different transportation means (“Academy Activities”). I shall ensure that the Player is physically fit and able to participate in the sporting activities. I understand that participation of the Player in the Academy Activities is done at our own risk. Consequently, I will not hold the Organizer or LaLiga, its officers, agents, employees, or anyone acting in their behalf, responsible or liable for any damage, loss of property, injury and any other harm whatsoever and howsoever big occurring to the named Player or caused by the Player in the course of such Academy Activities. <br><br>

                I authorize the Organizer to transport and to obtain, through a physician of its own choice, any emergency medical care that may become reasonably necessary for the Player in the course of such Academy Activities. <br><br>

                I, authorise the Organizer and LaLiga to take, post, record, broadcast, distribute and communicate publicly any images / videos recorded or photographs taken of the Player while participating in the Academy Activities, and to reproduce, in whole or in part, those images / videos or photographs, whether individually or in conjunction with other images / videos, photographs or pictures, incorporate them into brochures, advertisements, or any other promotional and informational materials or via the Internet, without temporal or geographical restrictions and without the right to any compensation or remuneration from the Organizer or LaLiga.</p>


                <h2 class="maintitle --black --small">
                    3. LIABILITY
                </h2>

                <p class="maindesc">Parents and Players agree to maintain and exercise courtesy at all times while on Academy’s premises and while interacting with the Academy Staff and other Parents and Students. <br><br>

                The Organizer reserves the right not to permit the access and remove any Player who behaves improperly or abusively towards him/herself or other persons, has been involved in a fraudulent activity or otherwise interferes with the normal running of the Academy. No refund will be given should the Player be removed from the Academy for these reasons. <br><br>

                The Organizer shall not be liable the Player and the Player’s Parents / Guardians for any force majeure event or occurrence or any Organizer’s action done based on a legal or regulatory imperative, including the suspension or cancellation of the Academy Activities. <br><br>

                The Organizer and LaLiga disclaims any liability in relation to Players’ claims and / or third parties’ claims that may arise as a result of the views and / or statements expressed and / or the use of images and / or personal data that could arise in relation to the Players in the Academy that may appear in the mass media. <br><br>

                LaLiga is exempt of any liability in regard to the Academy, which is exclusively the responsibility of the Organizer.</p>


                <h2 class="maintitle --black --small">
                    4. DATA PROTECTION. PRIVACY POLICY
                </h2>

                <p class="maindesc">All information Players provide shall be held in the strictest confidence and will be shared only on a “need-to-know” basis. <br><br>

                The Organizer or LaLiga may contact the Player at any time to demand proof of their true age by supplying a photocopy of their official identity card or an equivalent document. In the event that the Player does not provide the aforementioned documentation, LaLiga reserves the right to refuse providing the Academy services to the Player. <br><br>

                Subject to the conditions abovementioned, you are informed that your personal data will be collected by the Organizer and LaLiga (“Collecting Entities”). To this end, you give your consent to Collecting Entities to process your personal data and to manage the services related to your participation in the Academy. <br><br>

                With regard to LaLiga, and where the Player has expressly accepted using the tick-box provided for that purpose, the entrant is aware that their personal data will be used, even after the Player has ended his/her participation at the Academy, to send commercial communications by any means, including electronic commercial communications or equivalent (such as email, SMS, MMS and similar), and involving the use of data mining and profile segmentation ('Commercial Communications') by LaLiga, about activities, products, services, competitions, offers and/or promotions by LaLiga, its affiliates, clubs and companies it comprises, the Organizer and their official sponsors or promotions in partnership with third parties such as international television channels and other media. <br><br>

                You have the right to contact the Entities to exercise the rights of access, rectification, erasure and opposition, by written request sent by e-mail to the address lopd@laliga.es or to the postal address above referred, indicating as subject, "Data Protection". If necessary, we will ask you to provide us with a copy of your ID card, passport or other valid identity document. <br><br>

                Should you wish to cancel the reception of commercial communications, you shall do it at any time by written communication to the foregoing addresses, indicating as subject "Commercial Communications Cancellation".</p>


                <h2 class="maintitle --black --small">
                    5. MODIFICATION OF THE TERMS AND CONDITIONS
                </h2>

                <p class="maindesc">LaLiga and the Organizer reserve the right to modify or add to these Terms and Conditions of the Academy in part or in whole. Equally, LaLiga and the Organizer are entitled to resolve any eventuality not foreseen in the Terms and Conditions as it sees fit, on the basis of the spirit and intention of these Terms and Conditions.</p>

                <h2 class="maintitle --black --small">
                    6. APPLICABLE LAW AND JURISDICTION
                </h2>

                <p class="maindesc">These Terms and Conditions are governed by UAE law. <br><br>

                Any disputes arising out these Terms and Conditions shall be subject to the exclusive jurisdiction of the Courts of the UAE. <br><br>

                The Organizer and LaLiga reserve the right to take legal action against anyone who undertakes any kind of act that could be considered and infringement of the present terms and conditions. </p>

            </article>
        </div>
    </section>

@endsection
