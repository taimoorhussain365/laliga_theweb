@extends('layouts.app')
@section('content')
<section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/schedule-banner.jpg') }}')">
    <div class="inner-wrapper">
        <h1 class="title">Locations</h1>
    </div>
</section>
<section class="sec-padding">
    <div class="container-wrapper">
        @foreach($locations as $location)
        <article class="inner-content">
            <h2 class="maintitle --black --small">
            <span>{{$location->name}}</span>
            </h2>
            <figure class="mt-30">
                <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/gems-academy-map.jpg') }}" alt="LaLiga Academy" class="lazy">
            </figure>
        </article>
        @endforeach
       <!--  <article class="inner-content mt-80">
            <h2 class="maintitle --black --small">
            <span>AL BARSHA SOUTH - DUBAI</span>
            Zayed Sports City
            </h2>
            <figure class="mt-30">
                <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/zayed-academy-map.jpg') }}" alt="LaLiga Academy" class="lazy">
            </figure>
        </article> -->
    </div>
</section>
@endsection