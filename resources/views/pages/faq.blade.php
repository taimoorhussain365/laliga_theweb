@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/homepage-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">FAQs</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <article class="inner-content">
                <h2 class="maintitle --black --small">
                    Who is LaLiga?
                </h2>

                <p class="maindesc">LaLiga is the world’s strongest football league with some of the most loved players. LaLiga Clubs include:</p>

                <ul class="unstyled check-list">
                    <li>Alavés</li>
                    <li>Athletic Bilbao</li>
                    <li>Atlético Madrid</li>
                    <li>Barcelona</li>
                    <li>Celta Vigo</li>
                    <li>Eibar</li>
                    <li>Espanyol</li>
                    <li>Getafe</li>
                    <li>Granada</li>
                    <li>Leganés</li>
                    <li>Levante</li>
                    <li>Mallorca</li>
                    <li>Osasuna</li>
                    <li>Real Betis</li>
                    <li>Real Madrid</li>
                    <li>Real Sociedad</li>
                    <li>Sevilla</li>
                    <li>Valencia</li>
                    <li>Valladolid</li>
                    <li>Villarreal</li>
                </ul>

                <h2 class="maintitle --black --small">
                    What is LaLiga Academy?
                </h2>

                <p class="maindesc">LaLiga Academy is the region’s premier football school open to boys and girls aged 4-17 years who want to learn football the Spanish way. The academy offers players the opportunity to learn unique LaLiga methods, the same football skills and tactics learned by the world’s best footballers.</p>

                <h2 class="maintitle --black --small">
                    What comprises LaLiga training methodology?
                </h2>

                <p class="maindesc">LaLiga Academy focuses on teaching the Spanish football way, which involves a holistic approach that combines physical, tactical, emotional, and strategic training. LaLiga Academy instills in all its players important values like leadership, teamwork, commitment, gratitude and respect. Skills developed through the LaLiga Academy program are transferable across many disciplines on and off the pitch and help its players not only nurture a winning offensive style of play and technical skills but also shape them into well-rounded individuals. These skills include:</p>

                <ul class="unstyled check-list">
                    <li>Strategizing</li>
                    <li>Communication</li>
                    <li>Teamwork</li>
                    <li>Decision making</li>
                    <li>Ball possession</li>
                    <li>Short Passing</li>
                    <li>Player Movement</li>
                    <li>Technical Skills</li>
                </ul>

                <h2 class="maintitle --black --small">
                    How is LaLiga Academy different from other football schools?
                </h2>

                <p class="maindesc">LaLiga Academy grew out of a need for talented youth in the region to find a training ground to further develop their football skills at international standards. LaLiga found the UAE to have great potential and a natural choice to launch its world’s first Academy in Dubai and Abu Dhabi. <br><br>

                    LaLiga Academy is intent on providing the best quality coaching in handpicked locations across the country, and in an effort to create the most compelling football development pathway in the region, also serves as a scouting academy where the most talented players are coached, prepared and guided towards a professional football career.</p>

                <h2 class="maintitle --black --small">
                    What makes LaLiga Academy coaching so impactful and unique in the UAE?
                </h2>

                <p class="maindesc">LaLiga Academy offers the world’s highest professional level training. An all UEFA certified coaching team, holding the highest UEFA certification, ensures that enrolled Academy players receive the same world-class level of coaching that has produced the world’s finest players hailing from the best clubs in Europe. Each Academy location will have a team consisting of a UEFA Pro, as well as UEFA A and B LaLiga coaches.  The value that  LaLiga Coaches bring to the program is their through knowledge of the game of football which they tap into to train each player to reach their highest potential within the context of a player in a team facing an opponent. <br><br>

                Training at the Academy will follow the trademark LaLiga coaching methodology, focusing on player development both on football technique, skills and fitness to hone players for competitive matches, as well as physical, emotional and mental strength development. <br><br>

                This will ensure that players receive the best foundational training, enabling them to pursue either a pro-football career at leading clubs or obtain a full football scholarship to the world’s universities. </p>

                <h2 class="maintitle --black --small">
                    How can my child pursue a professional career through LaLiga Academy?
                </h2>

                <p class="maindesc">LaLiga Academy is not only a training academy but also a professional scouting academy through which the Spanish League can tap into for talent from this region. Players who are recognized for their exceptional talent and trainability by Academy coaches, who are also qualified as professional scouts, can earn their spots on the high performance teams and from there can be selected to participate in the exclusive LaLiga Summer Scouting Camp in Spain. The 2-week intensive camp will include high-level training and competitive matches against top LaLiga youth clubs. These matches will be attended by LaLiga Club scouts on the lookout for top talent to be recruited by their clubs.</p>

                <h2 class="maintitle --black --small">
                    How does LaLiga Academy help enrollees obtain high value football scholarships at top universities in the US and UK?
                </h2>

                <p class="maindesc">At LaLiga Academy, registered players also have the opportunity to obtain a full 4-year football scholarship to study at the world’s best with an extensive network connecting high potential players to over 50 Division 1 and 60 Division 2 universities in the US as well as select top universities in the UK. <br><br>

                    LaLiga Academy offers a comprehensive university application prep program for select players ages 14 and above with good academic standing, with the aim to maximize their chances to secure high-value football scholarships. This program, which is free of charge and managed by dedicated LaLiga Academy counselors, working with the coaching team, will guide registered players throughout the university application process to develop and submit a compelling file to optimize scouting and scholarship opportunities. This includes:</p>

                <ul class="unstyled check-list">
                    <li>Developing an impactful athletic resume and bio to complement student academic records</li>
                    <li>Matching players to the most suitable football teams across Division 1 and Division 2 universities in the US.</li>
                    <li>Building football credentials through video and online profiles to showcase players’ specific talents and skills</li>
                </ul>

                <p class="maindesc">LaLiga Academy partner with AGM Education, an end-to-end solution for players looking to pursue their academic and athletic endeavours in the United States. Through their comprehensive <strong>recruiting and consulting platform</strong>, and their state of the art Digital Mooxye Scholarship Recruitment and Education platform, they have had success with over <strong>1,800 student-athletes</strong> and continuously help more every year.</p>

                <h2 class="maintitle --black --small">
                    Who can register to train with LaLiga Academy?
                </h2>

                <p class="maindesc">LaLiga Academy is open to all boys and girls of all nationalities aged 4 – 17 years old at all skill levels.</p>


                <h2 class="maintitle --black --small">
                    Will training sessions be differentiated according to skill level?
                </h2>

                <p class="maindesc">Differentiation is critical to football development and each child needs to be met at his/her level and challenged appropriately. Here, children of various skill levels will be encouraged to grow and develop and to take their football skills as far as they want. So, whether your child is looking to improve, play with friends and have a great time or is committed fully to developing a serious football career, you will find the appropriate level of training at LaLiga Academy.  LaLiga Academy players begin in development squads and as they improved move into the advanced squads and from there into the high performance squads.</p>

                <h2 class="maintitle --black --small">
                    Once enrolled, will players have the chance to play in tournaments?
                </h2>

                <p class="maindesc">Participation in tournaments and leagues is a key part of LaLiga’s approach to football development, where technical and leadership skills acquired will be tested and honed on the pitch. Registered LaLiga Academy players will have the chance to play on the LaLiga Academy team and participate in the UAE’s most competitive tournaments andsleagues, according to their age groups. Once or twice a year players will also travel to Spain to compete at international tournaments against their counterparts from across the world. Top talents from the high performance teams will also face off in competitive matches against LaLiga’s top youth clubs in Spain, offering them a unique chance to up their game against some of the world’s strongest clubs during the summer scouting camp.</p>

                <h2 class="maintitle --black --small">
                    What are the locations of LaLiga Academy UAE?
                </h2>

                <p class="maindesc">Prime locations, befitting LaLiga’s rigorous standards, have been selected across Dubai and Abu Dhabi which are both convenient and accessible.</p>

                <h2 class="maintitle --black --small">
                    How many participants train per session?
                </h2>

                <p class="maindesc">LaLiga Academy aims for an optimum Coach to player ratio of an average of 1 to 14 for the younger age groups (U6 & U8) and an average of 1 to 18 for the other age groups.</p>

                <h2 class="maintitle --black --small">
                    How do I register my child?
                </h2>

                <p class="maindesc">Parents can register <a href="/register" class="td-underline fw-bold">here</a>.</p>

                <h2 class="maintitle --black --small">
                    What are the fees for enrollment?
                </h2>

                <p class="maindesc">Academy fees are detailed <a href="/fees" class="td-underline fw-bold">here</a>.</p>

                <h2 class="maintitle --black --small">
                    How long are the training sessions and how many times a week do they take place?
                </h2>

                <p class="maindesc">For the U6, U8, U10 age groups there will be up to two, 60 minute training sessions/week. <br><br>
                
                For the U12, U14, U16, U18 age groups there will be three, 75 minute training sessions/week</p>

            </article>
        </div>
    </section>

@endsection
