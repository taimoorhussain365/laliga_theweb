@extends('layouts.app')

@section('content')
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/about-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">About LaLiga</h1>
        </div>
    </section>

    <section class="full-beats" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <article class="inner-content">
                        <h2 class="maintitle --black --small">
                            The Only Official Academy in the UAE
                        </h2>

                        <p class="maindesc">LaLiga Academy UAE has elevated youth football to unprecedented professional levels. <br><br>Designed by the strongest league in the world, the LaLiga Academy delivers the highest standard of Spanish football training to youth across the UAE, while providing a pathway to professional football careers and scholarships to coveted universities.</p>
                    </article>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/about-img1.png') }}" alt="LaLiga Academy" class="lazy w-md-80 float-md-right">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="half-beats bg-blue sec-padding" data-img="url({{ asset('assets-web/images/half-beat.png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6 order-last">
                    <article class="inner-content">
                        <h2 class="maintitle --white --small">
                            SIGNATURE LALIGA TRAINING
                        </h2>

                        <p class="maindesc --white">Training at the Academy follows the trademark LaLiga coaching approach, focusing both on football technique, skills and fitness to hone players for competitive matches, as well as physical, emotional and mental strength development. <br><br>

                        Training is delivered by LaLiga coaches who are all UEFA PRO certified, ensuring the same world-class level of coaching that has created the world’s finest players hailing from the best clubs in Europe and across the world. </p>
                    </article>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/about-img2.jpg') }}" alt="LaLiga Academy" class="lazy w-md-80 float-md-right">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <article class="inner-content">
                        <h2 class="maintitle --black --small">
                            THE FIRST SCOUTING ACADEMY IN THE REGION
                        </h2>

                        <p class="maindesc">Holding the highest level of UEFA certification, which qualifies them as professional scouts, the LaLiga Academy coaching team recognize the most promising talent and prepare them for the opportunity to play professional football with leading international and regional clubs. <br><br>The UAE’s top Academy talents will be selected and enrolled in an exclusive LaLiga Summer Training Camp in Spain. The 3-week intensive camp will include high-level training and competitive matches against top LaLiga youth clubs. These matches will be attended by LaLiga Club scouts on the lookout for top talent to be recruited by their clubs. </p>
                    </article>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/about-img3.png') }}" alt="LaLiga Academy" class="lazy w-md-80 float-md-right">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="half-beats bg-red sec-padding" data-img="url({{ asset('assets-web/images/half-beat.png') }})">
        <div class="container-wrapper">
            <div class="row align-items-center">
                <div class="col-md-6 order-last">
                    <article class="inner-content">
                        <h2 class="maintitle --white --small">
                            FOOTBALL SCHOLARSHIPS TO TOP UNIVERSITIES
                        </h2>

                        <p class="maindesc --white">With LaLiga Academy, registered players also have the opportunity to obtain a full 4-year football scholarship to study at the best universities in the US and the UK. LaLiga Academy connects high potential players aged 14 years and above, who have a good academic record, with over 50 Division 1 and 60 Division 2 universities in the US as well as universities in the UK. <br><br>LaLiga Academy coaches and support team help to build player credentials, including video and online profiles, which effectively showcase player talent to maximize their chance for selection by university coaches and to secure high value scholarships.. </p>
                    </article>
                </div>

                <div class="col-md-6">
                    <figure>
                        <img src="{{ asset('assets-web/images/image-loader.gif') }}" data-src="{{ asset('assets-web/images/about-img4.jpg') }}" alt="LaLiga Academy" class="lazy w-md-80 float-md-right">
                    </figure>
                </div>
            </div>
        </div>
    </section>

    <section class="sec-padding">
        <div class="container-wrapper">
            <h2 class="maintitle --black --small">
                THE LALIGA WAY: BUILDING SKILLS, NURTURING VALUES
            </h2>

            <div class="row mt-60 align-items-center">
                <div class="col-md-6">
                    <div class="mb-md-20">
                        <div class="row align-items-center">
                            <div class="col-lg-2">
                                <figure>
                                    <img src="/assets-web/images/development-skills-icon.png" alt="">
                                </figure>
                            </div>

                            <div class="col-lg-10">
                                <h4 class="title mb-10">Developing Skills</h4>
                                <p class="maindesc">
                                    Training at LaLiga Academy will up the skills, fitness and stamina of aspiring footballers, while instilling in them a thorough understanding of the Game
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="mb-md-20">
                        <div class="row align-items-center">
                            <div class="col-lg-2">
                                <figure>
                                    <img src="/assets-web/images/development-iq-icon.png" alt="">
                                </figure>
                            </div>

                            <div class="col-lg-10">
                                <h4 class="title mb-10">Developing Football IQ</h4>
                                <p class="maindesc">
                                    Training at LaLiga Academy will provide aspiring footballers with the insights and know-how to read the game and solve tactical problems as they apply to themselves and their team within a game context
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
