@extends('layouts.app')

@section('content')
    
    <section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/registration-banner.jpg') }}')">
        <div class="inner-wrapper">
            <h1 class="title">Log In</h1>
        </div>
    </section>

    <section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
        <div class="container-wrapper">

            <article class="inner-content">
                <div class="row">
                    <section class="col-lg-6 offset-lg-3">
                        <?php /* <h2 class="maintitle --black mb-20">
                            Log In
                        </h2> */?>
                        
                        <div class="box --shadow-box --padding-box --bg-yellow">
                            <form method="POST" action="{{ route('login') }}" class="default-form">
                                @csrf

                                <div class="control-group">
                                    <label class="form-label">{{ __('E-Mail Address') }}</label>
                                    <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="control-group">
                                    <label class="form-label">{{ __('Password') }}</label>
                                    <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>

                                <div class="control-group">
                                    <div class="checkbox">
                                        <input type="checkbox" onclick="myFunction()">
                                        <span class="label"></span>
                                        <span class="text">{{ __('Show Password') }}</span>
                                    </div>
                                </div>

                                <div class="control-group mb-10">
                                    <button type="submit" class="btn --btn-primary">
                                        {{ __('Login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn --btn-link" href="{{ route('password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>

                                <p class="maindesc mb-0">Don't have an account? <a href="/register" class="fc-primary td-underline">Register Now</a></p>

                            </form>
                        </div>
                    </section>
                </div>
            </article>
        </div>
    </section>

@endsection

<script>
    function myFunction() {
    var x = document.getElementById("password");
    if (x.type === "password") {
    x.type = "text";
    } else {
    x.type = "password";
    }
    }
</script>
