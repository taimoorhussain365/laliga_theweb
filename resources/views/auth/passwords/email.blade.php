@extends('layouts.app')
@section('content')
<section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/registration-banner.jpg') }}')">
    <div class="inner-wrapper">
        <h1 class="title">Log In</h1>
    </div>
</section>
<section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
    <div class="container-wrapper">
        <article class="inner-content">
            <div class="row">
                <section class="col-lg-6 offset-lg-3">
                    <?php /* <h2 class="maintitle --black mb-20">
                    Log In
                    </h2> */?>
                    
                    <div class="box --shadow-box --padding-box --bg-yellow">
                        <form method="POST" action="{{ route('password.email') }}" class="default-form">
                            @csrf
                            <div class="control-group">
                                <label class="form-label">{{ __('E-Mail Address') }}</label>
                                
                                <input id="email" type="email" class="form-field @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                
                            </div>
                            <div class="control-group mb-10">
                                
                                <button type="submit" class="btn --btn-primary">
                                {{ __('Send Password Reset Link') }}
                                </button>
                                
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </article>
    </div>
</section>
@endsection