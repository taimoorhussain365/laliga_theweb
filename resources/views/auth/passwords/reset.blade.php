@extends('layouts.app')
@section('content')

<section class="hero-banner --inner-banner" style="background-image: url('{{ asset('assets-web/images/banners/registration-banner.jpg') }}')">
    <div class="inner-wrapper">
        <h1 class="title">Reset Passord</h1>
    </div>
</section>
<section class="full-beats sec-padding" data-img="url({{ asset('assets-web/images/full-beat.png') }})">
    <div class="container-wrapper">
        <article class="inner-content">
            <div class="row">
                <section class="col-lg-6 offset-lg-3">
                    <?php /* <h2 class="maintitle --black mb-20">
                    Log In
                    </h2> */?>
                    
                    <div class="box --shadow-box --padding-box --bg-yellow">
                        <form method="POST" action="{{ route('password.update') }}" class="default-form">
                            @csrf
                            <input type="hidden" name="token" value="{{ $token }}">
                            <div class="control-group">
                                <label for="email" class="form-label">{{ __('E-Mail Address') }}</label>
                                 <input id="email" type="email" class="form-field  @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                
                            </div>
                            <div class="control-group">
                                <label for="password" class="form-label">{{ __('Password') }}</label>
                               
                                    <input id="password" type="password" class="form-field @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                
                            </div>
                            <div class="control-group">
                                <label for="password-confirm" class="form-label">{{ __('Confirm Password') }}</label>
                               
                                    <input id="password-confirm" type="password" class="form-field" name="password_confirmation" required autocomplete="new-password">
                            
                            </div>
                            <div class="control-group">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                       </div>
                    </section>
                </div>
            </article>
        </div>
    </section>
    @endsection