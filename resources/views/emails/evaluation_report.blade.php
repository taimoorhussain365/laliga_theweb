@component('mail::message')
<p>Kindly find the Evaluation in attachment for <strong>"{{$player->first_name}} {{ $player->last_name }}"</strong> of Term <strong>{{ $terms->name }}</strong>.</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
