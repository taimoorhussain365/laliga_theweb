<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_eng');
            $table->integer('news_type_id');
            $table->string('image')->nullable();
            $table->string('video_url')->nullable();
            $table->text('details_eng')->nullable();
            $table->string('slug');
            $table->string('news_source_eng')->nullable();
            $table->integer('news_event_id');
            $table->string('name_ar');
            $table->text('details_ar')->nullable();
            $table->string('news_source_ar')->nullable();
            $table->integer('session_id')->nullable();
            $table->string('external_link')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->softDeletes();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
