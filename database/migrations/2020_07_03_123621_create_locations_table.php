<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->unsignedBigInteger('emirate_id');
        $table->tinyInteger('is_active')->default(STATUS_ENABLED);
        $table->softDeletes();
        $table->timestamps();
        $table->foreign('emirate_id')->references('id')->on('emirates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
