<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('invoice_no',50);
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('training_session_id');
            $table->unsignedBigInteger('location_id');
            $table->unsignedBigInteger('day_id');
            $table->integer('discount')->nullable();
            $table->text('discount_details')->nullable();
            $table->float('amount')->nullable();
            $table->integer('installment_id')->nullable();
            $table->float('kit_amount')->nullable();
            $table->float('tax_amount')->nullable();
            $table->float('pay_amount')->nullable();
            $table->float('total_amount')->nullable();
            $table->float('write_off_amount')->nullable();
            $table->text('write_off_amount_reason')->nullable();
            $table->float('partial_amount')->nullable();
            $table->text('partial_amount_reason')->nullable();
            $table->integer('payment_method')->nullable();
            $table->integer('payment_status')->default(STATUS_PENDING);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('training_session_id')->references('id')->on('training_sessions');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('day_id')->references('id')->on('days');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
