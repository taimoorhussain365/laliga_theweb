<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sign_ups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('dob');
            $table->string('email');
            $table->string('mobile');
            $table->text('comments')->nullable();
            $table->unsignedBigInteger('term_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->string('source_id')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->integer('form_type_id')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('term_id')->references('id')->on('terms');
            $table->foreign('category_id')->references('id')->on('age_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sign_ups');
    }
}
