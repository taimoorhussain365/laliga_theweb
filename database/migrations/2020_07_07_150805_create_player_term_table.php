<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerTermTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_term', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('term_id');
            $table->unsignedBigInteger('emirate_id');
            $table->tinyInteger('is_kit_amount_paid');
            $table->integer('kit_size');
            $table->integer('kit_status')->nullable();
            $table->integer('is_sponsored')->nullable();
            $table->integer('match_kit')->nullable();
            $table->integer('payment_status')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('term_id')->references('id')->on('terms');
            $table->foreign('emirate_id')->references('id')->on('emirates');
            $table->timestamps();
           });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_term');
    }
}
