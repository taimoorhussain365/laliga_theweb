<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('training_sessions', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->unsignedBigInteger('season_id');
        $table->unsignedBigInteger('term_id');
        $table->unsignedBigInteger('category_id');
        $table->time('start_time');
        $table->time('end_time');
        $table->float('fees_amount')->nullable();
        $table->integer('max')->default(STATUS_DISABLED);;
        $table->tinyInteger('for_goal_keeper');
        $table->integer('cap_a')->nullable();
        $table->integer('cap_b')->nullable();
        $table->integer('cap_c')->nullable();
        $table->tinyInteger('is_active')->default(STATUS_ENABLED);
        $table->softDeletes();
        $table->timestamps();
        $table->foreign('season_id')->references('id')->on('seasons')->onDelete('cascade');
         $table->foreign('term_id')->references('id')->on('terms')->onDelete('cascade');
        $table->foreign('category_id')->references('id')->on('age_categories');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('training_sessions');
    }
}
