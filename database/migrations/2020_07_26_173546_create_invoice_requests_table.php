<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('serial_no');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('training_session_id');
            $table->unsignedBigInteger('location_id');
            $table->unsignedBigInteger('day_id');
            $table->float('kit_amount')->nullable();
            $table->float('tax_amount')->nullable();
            $table->float('total_amount')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('training_session_id')->references('id')->on('training_sessions');
            $table->foreign('location_id')->references('id')->on('locations');
            $table->foreign('day_id')->references('id')->on('days');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_requests');
    }
}
