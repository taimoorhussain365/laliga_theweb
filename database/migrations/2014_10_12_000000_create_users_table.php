<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->tinyInteger('type')->default(USER_TYPE_PARENT);
            $table->tinyInteger('gender')->default(GENDER_MALE);
            $table->string('email_secondary')->nullable();
            $table->string('mobile')->nullable();
            $table->string('other_contact_no')->nullable();
            $table->string('nationality')->nullable();
            $table->text('address')->nullable();
            $table->string('hear_about_us')->nullable();
            $table->tinyInteger('is_active')->default(STATUS_ENABLED);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
