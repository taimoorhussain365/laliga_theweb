<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlayerTrainingSessionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('player_training_session', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('player_id');
            $table->unsignedBigInteger('training_session_id');
            $table->unsignedBigInteger('location_id');
            $table->unsignedBigInteger('day_id');
            $table->integer('serial_no');
            $table->foreign('player_id')->references('id')->on('players');
            $table->foreign('day_id')->references('id')->on('days');
            $table->foreign('training_session_id')->references('id')->on('training_sessions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('player_training_session');
    }
}
