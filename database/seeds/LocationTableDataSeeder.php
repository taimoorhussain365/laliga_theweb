<?php

use Illuminate\Database\Seeder;
use App\Location;

class LocationTableDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create(['name' => 'Emirates Palace','emirate_id'=>1]);
        Location::create(['name' => 'Sheikh Zayed Grand Mosque','emirate_id'=>1]);
        
    }
}
