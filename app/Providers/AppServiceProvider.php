<?php



namespace App\Providers;



use Illuminate\Support\ServiceProvider;

use Illuminate\Support\Facades\Schema;
use App\User;
use App\Wallet;
use Auth;
use View;

class AppServiceProvider extends ServiceProvider

{

    /**

     * Register any application services.

     *

     * @return void

     */

    public function register()

    {

        //

    }



    /**

     * Bootstrap any application services.

     *

     * @return void

     */

    public function boot(){

     Schema::defaultStringLength(191);
     view()->composer('*', function ($view) {
     if(Auth::check()){
     $user=Auth::user()->id;
     $wallet=Wallet::where('user_id',$user)->orderByDesc('id')->first();
     if(empty($wallet)){
     $wallet=0;
     }
     else{
     $wallet=$wallet->total;
     }
     $view->with('wallet',$wallet); 
     }   
    });  


    }

}

