<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class SignUp extends Model
{
    use SoftDeletes;
    public function category()
    {
       return $this->belongsTo('App\AgeCategory');
    
    }

    public function user()
    {
       return $this->belongsTo('App\User');
    
    }

    public function terms()
    {
       return $this->belongsTo('App\Term');
    
    }
}
