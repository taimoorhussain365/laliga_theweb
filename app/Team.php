<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Team extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];

   public function users()
    {
        return $this->belongsToMany('App\User','coach_teams')->withTimestamps()->withPivot(['location_id','category_id','id','is_active']);
    }
}
