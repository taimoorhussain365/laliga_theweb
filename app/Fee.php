<?php



namespace App;



use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;



class Fee extends Model

{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function location()

    {

        return $this->belongsTo('App\Location');

    }

    public function getCategory($id)

    {

        return AgeCategory::where('id',$id)->pluck('name')->first();

    }

    public function term(){

       return $this->belongsTo('App\Term');

   }

   

}

