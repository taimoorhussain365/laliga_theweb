<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;
use App\Traits\PrivilegedScope;

class City extends Model implements Auditable
{
    use SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $dates = ['deleted_at'];

    use PrivilegedScope;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->initPrivilegedScope();
    }

    public function locations()
    {
        return $this->hasMany('App\Location', 'city_id');
    }

    public function Terms()
    {
        return $this->hasMany('App\Term');
    }
}
