<?php

namespace App\Http\Traits;

use App\Invoice;

trait PDFTrait
{
    public function generatePDF($invoiceNo)
    {
        $invoice = Invoice::withTrashed()->where('invoice_no', $invoiceNo)->first();
        if ($invoice) {
            $fileDestination = public_path('invoices/');
            $wkhtmltopdf     = public_path("generatepdf");
            $fileName        = md5('FC#' . $invoice->id) . ".pdf";
            // Delete Invoice if Exists
            if(file_exists($fileDestination . $fileName)) {
                unlink($fileDestination . $fileName);
            }
            $cmd = $wkhtmltopdf
                . " --lowquality -L 5mm -R 5mm -T 25mm -B 25mm '"
                . route('invoices.view_content', [$invoice->id]) . "' '"
                . $fileDestination . $fileName . "'";
            echo $cmd;
            if (exec($cmd)) {
                return $fileName;
            } else {
                return "nop";
            }
        }
        abort(404);
    }
}
