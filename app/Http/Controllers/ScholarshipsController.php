<?php

namespace App\Http\Controllers;

use App\Scholarships;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ScholarshipsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $scholarships=Scholarships::all();
      return view('admin.scholarships.index',['scholarships'=>$scholarships,'no'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Scholarships  $scholarships
     * @return \Illuminate\Http\Response
     */
    public function show(Scholarships $scholarships)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Scholarships  $scholarships
     * @return \Illuminate\Http\Response
     */
    public function edit(Scholarships $scholarships)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Scholarships  $scholarships
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Scholarships $scholarships)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Scholarships  $scholarships
     * @return \Illuminate\Http\Response
     */
    public function destroy(Scholarships $scholarships)
    {
        //
    }
}
