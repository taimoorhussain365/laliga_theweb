<?php



namespace App\Http\Controllers;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\AgeCategory;
use App\Team;
use App\User;
use App\Term;


class TeamController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {

        $teams=Team::orderBy('id','DESC')->paginate(15);
        return view('admin.teams.index',['teams'=>$teams,'no'=>1]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
      
      $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
      $users=User::where('type',USER_TYPE_COACH)->pluck('name','id');
      $terms=Term::where('is_active',STATUS_ENABLED)->pluck('name','id');
      return view('admin.teams.create',['categories'=>$categories,'users'=>$users,'terms'=>$terms]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
        $terms=Term::where('is_active',STATUS_ENABLED)->get();
        $this->validateTeam($request);

        $team = new Team();

        $team->name = $request->input('name');

        if (Team::where('name', '=', $team->name)->first()) {

            return redirect()->route('admin.team.index')->with('error', 'Data is Already Present');

        }
        $team->category_id = $request->category_id;
        $team->user_id = $request->user_id;
        $team->term_id=$request->term_id;
        $team->is_active = $request->input('is_active') !== null ? 1 : 0;;
          
        $team->save();



        return redirect()->route('admin.teams.index')->with('success', 'Team Added Successfully');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Team $team)

    {
        $terms=Term::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
        $users=User::where('type',USER_TYPE_COACH)->pluck('name','id');

        return view('admin.teams.edit',['team'=>$team,'categories'=>$categories,'users'=>$users,'terms'=>$terms]);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,Team $team)

    {
        $terms=Term::where('is_active',STATUS_ENABLED)->get();
        $this->validateTeam($request);

        $team->name = $request->input('name');

        if (Team::where('name', '=', $team->name)->where('id','!=',$team->id)->first()) {

            return redirect()->route('admin.teams.index')->with('error', 'Data is Already Present');

        }

        $team->is_active = $request->input('is_active') !== null ? 1 : 0;
        $team->category_id = $request->category_id;
        $team->user_id = $request->user_id;
        $team->term_id=$request->term_id;
        $team->save();

        return redirect()->route('admin.teams.index')->with('success', 'Team Updated Successfully');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy(Team $team)

    {

        $team->delete();

         return redirect()->route('admin.teams.index')->with('success', 'Teams Deleted Successfully');

    }



    private function validateTeam(Request $request) {

       

        $request->validate([

            'name'  => 'required|max:100',
            'category_id'  => 'required',
            'user_id'  => 'required',
            'term_id'  => 'required',

            

        ]);

    }



   

}

