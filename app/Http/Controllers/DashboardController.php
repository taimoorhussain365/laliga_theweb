<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Location;
use App\Day;
use App\Invoice;
use App\User;
use App\InvoiceRequest;
use App\Wallet;
use App\Player;
use App\Term;
use App\Emirate;
use App\Season;
use App\Fee;
use Carbon\Carbon;
use Auth;
use App\Discount;
use App\TrainingSession;

class DashboardController extends Controller
{

      public function __construct() 
      {}
      

     public function index() 
      {
       
      if(Auth::user()->type == USER_TYPE_PARENT){
              
            
         return view('account.dashboard');
 
        }

        // if(Auth::user()->type == USER_TYPE_ADMIN){
        elseif(Auth::user()->isadmin == 1){

          $not_paid=Invoice::where('payment_status',STATUS_NOT_PAID)->count();
          $paid=Invoice::where('payment_status',STATUS_PAID)->count();
          $players=Player::count();
          $total_invoices=Invoice::count();
          $seasons=Season::orderByDesc('id')->get();
          $terms=Term::orderByDesc('id')->get();
          $invoices=Invoice::all();
          $data=array();
          $total=0;
      //     foreach($terms as $key => $term) {
      //     	foreach ($invoices  as $key => $invoice) {
      //               $session_details=json_decode($invoice->session_details,true);
      //                 foreach ($session_details  as $key => $detail) {
      //                 if($term->id==$detail['term_id'] && $invoice->payment_status==STATUS_PAID){
      //                 $paid=0;
      //                 $data[$term->id]['paid']=++$paid;
      //                 $data[$term->id]['partial_payment']=0;

      //     	}
      //     	elseif($term->id==$detail['term_id'] && $invoice->payment_status==STATUS_PARTIAL_PAYMENT){
                     
      //                 $partial=0;
      //                 $data[$term->id]['paid']=0;
      //                 $data[$term->id]['partial_payment']=++$partial;
      //     	}
      //     	else{
      //     		$paid=0;
      //             $partial=0;
      //             $data[$term->id]['paid']=0;
      //             $data[$term->id]['partial_payment']=0;
      //     	}
      //     	$total=$paid+$partial;
      //         $data[$term->id]['total']=$total;
      //        }
      //     }
      // }   
             return view('admin.dashboard',['seasons'=>$seasons,'players'=>$players,
          	'paid'=>$paid,'data'=>$data,'not_paid'=>$not_paid,'total_invoices'=>$total_invoices]);
          
          } 
          // elseif(Auth::user()->type == USER_TYPE_PARENT){
              
            
          //    return view('account.dashboard');

          //     }

              elseif(Auth::user()->type == USER_TYPE_MANAGER){

                  $seasons=Season::orderByDesc('id')->get();
                  return view('manager.dashboard',['seasons'=>$seasons]);
              }

               elseif(Auth::user()->type == USER_TYPE_ACCOUNTANT){

                  return view('accountant.dashboard');
              }

              elseif(Auth::user()->type == USER_TYPE_COACH){

                 return view('coach.dashboard');
             }

            // if(Auth::user()->type == USER_TYPE_PARENT){
              
            
            //   return view('account.dashboard');
 
            //    }

      }

      public function index2() 
      {
       
        if(Auth::user()->type == USER_TYPE_PARENT){
              
            
             return view('account.dashboard');

              }
      }
      public function allTerms()

      {

  	$user=Auth::user()->id;

  	$term=Term::where('is_active',STATUS_ENABLED)->get();

  	$emirates =Emirate::where('is_active',STATUS_ENABLED)->get();

  	$players = Player::where('is_active',STATUS_ENABLED)->where('user_id',$user)->get();

  	$kit_size=[1=>'X-Small- Youth Size',2=> 'Small- Youth Size',3=>'Medium- Youth Size',

  	4=>'Large- Youth Size',5=>'Small- Men\s Size',6=>'Medium- Men\s Size ',

  	7=>'Large- Men\s Size',8=>'X-Large - Youth Size'];

      return view('account.terms',['emirates'=>$emirates,'players'=>$players,'terms'=>$term,'kit_size'=>$kit_size]);

      }



      public function getPlayerDetails(Request $request)

      {

  	$id=$request->id;

  	$player = Player::where('id',$id)->where('is_active',STATUS_ENABLED)->where('guardian_id',Auth::user()->id)->first();

  	return response()->json(['data'=>$player]);

      }



      public function storePlayerTerm(Request $request){
      
       $request->validate([
          'kit_size' => ['required'],
          'emirate' => ['required'],
          'term' => ['required'],
        ]);
      $amount=0;
      $player=Player::find($request->player);
      $term=Term::find($request->term);
      $emirate_id=$request->emirate;
      $team_id=$request->team_id;
      $kit_size=$request->kit_size;
      $serial_no=rand(1000,9999);
      if($term->type_id==CAMP_WITHOUTDAYS){
      $fees=Fee::where('term_id',$term->id)->get();
        foreach($fees as $fee){
            $details=json_decode($fee->fee_details,true);
          foreach ($details as $key => $detail){
          if($detail['category_id']==$player->category->id){
                
               $amount=$detail['amount'];
             
              
             }
           }
          }
     }
      $hasTerm = $player->terms()->wherePivot('term_id',$term->id)->wherePivot('deleted_at','=',null)->first();

      if(!empty($hasTerm)){
          return redirect()->route('account.terms')->with('error','This Term is already added for this player'); 
        }
   
       if(Auth::user()->type==USER_TYPE_PARENT){
       $player->terms()->attach($term->id,['emirate_id'=>$emirate_id,'is_kit_amount_paid'=>0,
       'kit_size'=>$kit_size,'serial_no'=>$serial_no,'payment_status'=>STATUS_NOT_PAID,'fee_amount'=>$amount,'match_kit'=>STATUS_ENABLED,'kit_status'=>STATUS_ENABLED]);

         return redirect()->route('account.terms');
       }
      }

       public function destroyPlayerTerm(Player $player,Term $term,$id){
      
       if(Auth::user()->type==USER_TYPE_MANAGER){
          $type="manager.";
         }
         elseif(Auth::user()->type==USER_TYPE_ADMIN){
          $type="admin.";
         }

          if (!empty($player)) {
          
          foreach($player->trainingSessions->where('term_id',$term->id) as $key => $session){
          
           $player->trainingSessions()->wherePivot('training_session_id',$session->id)->detach();
          }
          
          $player->terms()->wherePivot('id',$id)->update(['player_term.deleted_at' => Carbon::now()]);
      }
      
       if(Auth::user()->type==USER_TYPE_PARENT){
           return redirect()->route('account.terms')->with('success', 'Term Deleted Successfully');
       }

  	else {
  	 return redirect()->route($type.'players.term',[$player->id])->with('success', 'Term Deleted Successfully');

          }

      }
    
     public function editTrainingSession(Player $player,Term $term,$emirate){

      if(!empty($player->admin_category_id)){
    		$category_id=$player->admin_category_id;
      	}else{
           $category_id=$player->category_id;
    	}
      $playerdata=array();
    	
      $days=Day::where('is_active',STATUS_ENABLED)->get();
      $locations = location::where('is_active',STATUS_ENABLED)->where('emirate_id',$emirate)->get();
        

    	$sessions=TrainingSession::where('category_id',

    	$category_id)->where('term_id',$term->id)

    	->whereHas('locations',function($q) use ($emirate){

    	$q->where('locations.emirate_id',$emirate);

    	})->orderBy('id', 'DESC')->where('is_active',STATUS_ENABLED)->get();

    	if(Auth::user()->type==USER_TYPE_PARENT){
      
      if($term->type_id==CAMP_WITHDAYS){

    	return view('account.training_session.camp_with_days',['no'=>1,'sessions'=>$sessions,'locations'=>$locations,'player'=>$player,'days'=>$days,'term'=>$term,'emirate'=>$emirate,'playerdata'=>$playerdata]);
        }
      elseif($term->type_id==SEASON_TERM){
     
       return view('account.training_session.season_term',['no'=>1,'sessions'=>$sessions,'locations'=>$locations,'player'=>$player,'days'=>$days,'term'=>$term,'emirate'=>$emirate,'playerdata'=>$playerdata]);
        
      }
      
      }
      
      elseif(Auth::user()->type==USER_TYPE_ADMIN || Auth::user()->type==USER_TYPE_MANAGER){
       
      return view('admin.player_locations.index',['no'=>1,'sessions'=>$sessions,'locations'=>$locations,'player'=>$player,'days'=>$days,'term'=>$term,'emirate'=>$emirate,'playerdata'=>$playerdata]);
        }

      }
    

     public function updatePlayerTrainingSession(Player $player,$term_id,$emirate_id,Request $request){

      $request->validate([
          'days'  => 'required|',
          
        ]);

      $serial_no=rand(1000,9999);
      $term=Term::find($term_id);
      $player->trainingSessions()->detach();
      $fullweek=$request->is_full_week;
      $days=$request->days;
      $total_days_count=count($days);
      $fees=Fee::where('term_id',$term_id)->get();
      $amount=0;
      
      if(!empty($days)){
      $location=0;
      foreach ($days as $day) {
      $record = explode('-', $day);
      $session_id    = $record[0];
      $day_id   = $record[1];
      $location_id   = $record[2];
      $weeks   = $record[3];
      $full_week_status=0;

      if($location_id != $location){

      $serial_no=rand(1000,9999);

      }
      if($term->type_id==CAMP_WITHDAYS ){
      if(!empty($fullweek)){
          $total_days_count=count($fullweek)+1;
          $full_week_status=1;

          foreach($fees as $fee){
            $details=json_decode($fee->fee_details,true);
          foreach ($details as $key => $detail){
          if($detail['category_id']==$player->category->id && $fee->location_id==$location_id 
            && $total_days_count==$detail['no_of_week']){
                
               $amount=$detail['amount'];
             
              
             }
           }
          }
      }
      else{
        $total_days_count=count($days);
         foreach($fees as $fee){

          $details=json_decode($fee->fee_details,true);
          foreach ($details as $key => $detail){
          if($detail['category_id']==$player->category->id && $fee->location_id==$location_id 
            && $detail['no_of_week']==1){
                
               $amount= $total_days_count*$detail['amount'];
             
              
             }
           }
          }
      }
       
      }
      else{
        $total_days_count=count($days);
         foreach($fees as $fee){

          $details=json_decode($fee->fee_details,true);
          foreach ($details as $key => $detail){
          if($detail['category_id']==$player->category->id && $fee->location_id==$location_id 
            && $detail['no_of_week']==$total_days_count){
                
               $amount=$detail['amount'];
             
              
             }
           }
        }
      }
      
      $session = TrainingSession::find($session_id);
      $player->terms()->where('term_id',$term_id)->update(['fee_amount'=>$amount]);
      $session->players()->attach($player->id,['day_id'=>$day_id,'location_id'=>$location_id,'serial_no'=>$serial_no,'weeks'=>$weeks,'is_full_week'=>$full_week_status]);

      $location=$location_id;
        }
      
      }

      if(Auth::user()->type==USER_TYPE_PARENT){
      
       return redirect()->route('account.terms')->with('success',"TrainingSession Added successfully");
      
      }
      
      elseif(Auth::user()->type==USER_TYPE_ADMIN || Auth::user()->type==USER_TYPE_MANAGER){

         return redirect()->route('admin.players.term',[$player->id])->with('success',"TrainingSession Added successfully");
       
        }
      }
  }