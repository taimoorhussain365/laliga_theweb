<?php



namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Wallet;

use App\User;

use Auth;



class WalletController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index(User $parent)

    {

      if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

        $wallets=Wallet::where('user_id',$parent->id)->get();
        $wallet_amount=Wallet::where('user_id',$parent->id)->pluck('total')->last();
        $types=['1'=>'Used','2'=>'Not Used','3'=>'Cancelled','4'=>'Refund'];
        return view($type.'wallets.index',['wallets'=>$wallets,'no'=>1,'parent'=>$parent,'wallet_amount'=>$wallet_amount,'types'=>$types]);

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create($parent)

    {

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
       $amount_type=['1'=>'Debit','2'=>'Credit'];

       $types=['1'=>'Used','2'=>'Not Used','3'=>'Cancelled','4'=>'Refund'];

       return view($type.'.wallets.create',['types'=>$types,'parent'=>$parent,'amount_type'=>$amount_type]);

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {
     
     if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
      

      $amount=$request->amount;

      $amount_type=$request->amount_type;

      $user_id=$request->user_id;

      $status=$request->status;

      $total_amount=Wallet::where('user_id',$user_id)->pluck('total')->last();



      $wallet = new Wallet(); 

      if(empty($total_amount))

      {

        $total_amount=0;

      }

      

      if($amount_type==AMOUNT_TYPE_DEBIT)

      {

        $total=$total_amount-$amount;

        $wallet->debit_amount=$amount;

      

      }

      else if($amount_type==AMOUNT_TYPE_CREDIT){

         $total=$total_amount+$amount;

         $wallet->credit_amount=$amount;

      }

      

      $wallet->user_id=$user_id;

      $wallet->status=$status;

      $wallet->amount_type=$amount_type;

      $wallet->total=$total;

      $wallet->remarks=$request->remarks;

      $wallet->save();

      return redirect()->route($type.'wallets.index',[$request->user_id])->with('success', 'Amount Updated');

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Wallet $wallet,$user)

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

       $parent=$user;

       $amount_type=['1'=>'Debit','2'=>'Credit'];

       $types=['1'=>'Used','2'=>'Not Used','3'=>'Cancelled'];

       return view($type.'wallets.edit',['wallet'=>$wallet,'amount_type'=>$amount_type,'types'=>$types,'parent'=>$parent]);

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,Wallet $wallet)

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

       $wallet->status=$request->status;

       $wallet->remarks=$request->remarks;

       $wallet->save();

       return redirect()->route($type.'.wallets.index',[$request->user_id])->with('success', 'Amount Updated');

    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($user,Wallet $wallet)

    {
      if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
      $wallet->delete();
       return redirect()->route($type.'wallets.index',[$user])->with('success', 'Amount Updated');


    }

}

