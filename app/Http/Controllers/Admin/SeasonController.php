<?php



namespace App\Http\Controllers\Admin;



use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Season;

use App\Term;

use Auth;



class SeasonController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
    //    if(Auth::user()->type==USER_TYPE_ADMIN){
      if(Auth::user()->isadmin == 1){
        $type="admin.";
       }
//    elseif(Auth::user()->type==USER_TYPE_MANAGER){
   elseif(Auth::user()->isadmin == 1){
    $type="manager.";
   }
      $seasons = Season::orderByDesc('created_at')->paginate(15);
      
      return view($type.'seasons.index',['seasons'=>$seasons]);
    
      

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {
         
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
       return view($type.'seasons.create');
      

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

        $this->validateSeason($request);

        $season = new Season();

        $name= $request->input('name');

        $check_exist = Season::where('name',$name)->first();

        if (!empty($check_exist)) {

         return redirect()->route('admin.seasons.index')->with('error', 'Season Already Exist');    

        }

        $season->name = $name;

        $season->is_active = $request->input('is_active') !== null ? 1 : 0;

        $season->save();
        
        if(Auth::user()->type==USER_TYPE_MANAGER){
         return redirect()->route('manager.seasons.index')->with('success', 'Season Added Successfully');
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
          return redirect()->route('admin.seasons.index')->with('success', 'Season Added Successfully');
      }
        

    }



    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)

    {

        //

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit(Season $season)

    {
       
       if(Auth::user()->type==USER_TYPE_MANAGER){
         return view('manager.seasons.edit',['season'=>$season]);
         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
         return view('admin.seasons.edit',['season'=>$season]);
      }
    

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request,Season $season)

    {

        $this->validateSeason($request);

        $name = $request->input('name');

        $check_exist = Season::where('name',$name)->where('id','!=',$season->id)->first();

        if (!empty($check_exist)) {

         return redirect()->route('admin.seasons.index')->with('error', 'Season Already Exist');    

        }

        $season->name = $name;

        $season->is_active = $request->input('is_active') !== null ? 1 : 0;

        $season->save();
        
         Term::where('season_id',$season->id)->update(['is_active'=>$season->is_active]);
        
        if(Auth::user()->type==USER_TYPE_MANAGER){
         return redirect()->route('manager.seasons.index')->with('success', 'Season Updated Successfully');

         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.seasons.index')->with('success', 'Season Updated Successfully');

      }
    
        
    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($season)

    {

     if (!empty($season)) {

     $term = Term::where('season_id', $season)->delete();

      $season = Season::where('id', $season)->delete();


        if(Auth::user()->type==USER_TYPE_MANAGER){
          return redirect()->route('manager.seasons.index')->with('success', 'Season Deleted Successfully');

         }
         if(Auth::user()->type==USER_TYPE_ADMIN){
         return redirect()->route('admin.seasons.index')->with('success', 'Season Deleted Successfully');

      }
            

           

        }

    }



    private function validateSeason(Request $request) {

       

        $request->validate([

            'name'  => 'required|max:100',

        ]);

    }

}

