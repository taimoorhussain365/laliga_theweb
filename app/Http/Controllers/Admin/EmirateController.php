<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Auth;
use App\Emirate;
use App\Location;
use Illuminate\Http\Request;

class EmirateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emirates=Emirate::orderByDesc('created_at')->get();
        return view('admin.emirates.index',['emirates'=>$emirates,'no'=>1]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.emirates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validateEmirate($request);
        $emirate= new Emirate();
        $emirate->name = $request->input('name');
        if (Emirate::where('name','=',$emirate->name)->first()) {

            return redirect()->route('admin.emirates.index')->with('error', 'Data is Already Present');
        }
        $emirate->is_active = $request->input('is_active') !== null ? 1 : 0;
        $emirate->save();

        return redirect()->route('admin.emirates.index')->with('success', 'Emirate Added Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Emirate $emirate)
    {

        return view('admin.emirates.edit',['emirate'=>$emirate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Emirate $emirate)
    {
        $this->validateEmirate($request);
        $name = $request->input('name');
        if(Emirate::where('name', '=',$name)->where('id','!=',$emirate->id)->first()) {

            return redirect()->route('admin.emirates.index')->with('error', 'Data is Already Present');
        }
        $emirate->name = $name;
        $emirate->is_active = $request->input('is_active') !== null ? 1 : 0;
        $emirate->save();

        $locations=$emirate->locations;

        if(!empty($locations)){
            foreach ($locations as $location) {
                $location->is_active = $request->input('is_active') !== null ? 1 : 0;;
                $location->save();
            }
        }

        return redirect()->route('admin.emirates.index')->with('success', 'Emirate Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Emirate $emirate)
    {
        if(empty($emirate)){
            return redirect()->route('admin.emirates.index')->with('error', 'Failed to Delete');
        }else{
         $location=Location::where('emirate_id',$emirate->id)->where('deleted_at','=',NULL)->first();
            if(empty($location))
                {
                $emirate->delete();
                return redirect()->route('admin.emirates.index')->with('success', 'Emirate Deleted Successfully');
                }
                else{
                    return redirect()->route('admin.emirates.index')->with('error', 'Data is in Used');
                }
            }
          
        }
    private function validateEmirate(Request $request) {
       $request->validate([
            'name'  => 'required|max:100',
            
        ]);
    } 
    }

