<?php



namespace App\Http\Controllers;



use App\Player;

use App\User;

use App\AgeCategory;

use App\Emirate;

use App\Team;

use App\Day;

use App\Term;
use App\Discount;

use App\Location;

use App\TrainingSession;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;



class PlayersController extends Controller

{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function index()

    {
       if(Auth::user()->type==USER_TYPE_ADMIN){
    //    if(Auth::user()->isadmin == 1){
            $type="admin.";
           }
       elseif(Auth::user()->type==USER_TYPE_MANAGER){
    //    elseif(Auth::user()->isadmin == 1){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_COACH){
        $type="coach.";
       }

        $players = Player::orderby('id',"desc")->paginate(15);

        $emirates=Emirate::where('is_active',STATUS_ENABLED)->get();

        $locations=location::where('is_active',STATUS_ENABLED)->get();

        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();

        $teams=Team::where('is_active',STATUS_ENABLED)->get();

        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();

        $days=Day::all();

        $coaches=User::where('type',USER_TYPE_COACH)->get();

       
        return view($type.'players.index', ['players'=>$players,'emirates'=>$emirates,'days'=>$days,'coaches'=>$coaches,'locations'=>$locations,'teams'=>$teams,'categories'=>$categories,'terms'=>$terms]);
      

        

    }



    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()

    {

        // if(Auth::user()->type==USER_TYPE_ADMIN){
        //   return view('admin.players.create');
        // }
        // elseif(Auth::user()->type==USER_TYPE_PARENT){
        //  return view('account.players.create');   
        // }

        if (Auth::user()->type == USER_TYPE_ADMIN) 
        {
            return view('admin.players.create');
        } 
        elseif (Auth::user()->type == USER_TYPE_PARENT) 
        {
            return view('account.players.create');
        }

    }



    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)

    {

    if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
       elseif(Auth::user()->type==USER_TYPE_PARENT){
        $type="account.";
       }
        $this->validatePlayer($request);
        $dob = Carbon::createFromFormat('d/m/Y', $request->dob)->format('Y-m-d');
        $gender = $request->gender;
        $countSiblingNo=Player::where('user_id',Auth::user()->id)->count();
        $siblingNo=$countSiblingNo+1;
        $category_id=$this->ageCategoryValidate($dob,$gender);
       
        if(empty($category_id)){
        return redirect()->route('account.players.create')->with('error','Category Not Found According to DOB');
        }
        else{
        $player=new Player();
        $player->user_id=Auth::user()->id;
        $player->category_id=$category_id;
        $player->name = $request->name;
        $player->dob = $dob;
        $player->gender = $gender;
        $player->nationality = $request->nationality;
        $player->previous_football_academy = $request->previous_football_academy;
        $player->sibling_no=$siblingNo;
        $player->save();

        
        return redirect()->route($type.'players.index')->with('success', 'Player has been updated successfully.');

        }
        

    }



    /**

     * Display the specified resource.

     *

     * @param  \App\Player  $player

     * @return \Illuminate\Http\Response

     */

    public function show(Player $player)

    {

//        return view('account.players.show', compact('player'));

    }



    /**

     * Show the form for editing the specified resource.

     *

     * @param  \App\Player  $player

     * @return \Illuminate\Http\Response

     */

    public function edit(Player $player)

    {

        if(Auth::user()->type==USER_TYPE_ADMIN){

        $teams=Team::where('is_active',STATUS_ENABLED)->get();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
        return view('admin.players.edit',['player'=>$player,'teams'=>$teams,
            'categories'=>$categories]);

    }

        elseif(Auth::user()->type==USER_TYPE_MANAGER){

        $teams=Team::where('is_active',STATUS_ENABLED)->get();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->pluck('name','id');
        return view('manager.players.edit',['player'=>$player,'teams'=>$teams,
            'categories'=>$categories]);

    }

    else{

        return view('account.players.edit',['player'=>$player]);

    }

    }



    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  \App\Player  $player

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, Player $player)

    {
        $this->validatePlayer($request);
        $dob = date('Y-m-d',strtotime($request->dob));
        $gender = $request->gender;
        
        $category_id=$this->ageCategoryValidate($dob,$gender);
       
        if(empty($category_id)){
         return redirect()->route('account.players.edit',[$player->id])->with('error','Category Not Found According to DOB');
        }
        else{

        $guardian_id=Auth::user()->id;
        $player->category_id=$category_id;
        $player->name = $request->name;
        $player->dob = $dob;
        $player->gender = $gender;
        $player->nationality = $request->nationality;
        $player->previous_football_academy = $request->previous_football_academy;

        if($request->photo){

            $rules['photo'] = ['max:5048', 'image', 'mimes:jpg,jpeg,bmp,png', 'mimetypes:image/jpeg,image/bmp,image/png'];

        }



        if($request->medical_insurance){

            $rules['medical_insurance'] = ['max:5048', 'file', 'mimes:jpg,jpeg,bmp,png,pdf', 'mimetypes:image/jpeg,image/bmp,image/png,application/pdf'];

        }



        if($request->emirates_id_front){

            $rules['emirates_id_front'] = ['max:5048', 'file', 'mimes:jpg,jpeg,bmp,png,pdf', 'mimetypes:image/jpeg,image/bmp,image/png,application/pdf'];

        }



        if($request->emirates_id_back){

            $rules['emirates_id_back'] = ['max:5048', 'file', 'mimes:jpg,jpeg,bmp,png,pdf', 'mimetypes:image/jpeg,image/bmp,image/png,application/pdf'];

        }

        $folder_name = str_pad($guardian_id,5,'0', STR_PAD_LEFT) . '_' . str_pad($player->id,5,'0', STR_PAD_LEFT);



        if($request->photo) {

            $extension  = $request->photo->getClientOriginalExtension();

            $photo      = 'photo_' . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;

            $photo_path = $request->photo->storeAs('public/players/'.$folder_name, $photo);

            $photo_path = substr($photo_path, 7);

            $player->photo = $photo_path;

        }



        if($request->medical_insurance) {

            $extension  = $request->medical_insurance->getClientOriginalExtension();

            $medical_insurance = 'medical_insurance_' . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;

            $file_path = $request->medical_insurance->storeAs('public/players/'.$folder_name, $medical_insurance);

            $file_path = substr($file_path, 7);

            $player->medical_insurance = $file_path;

        }



        if($request->emirates_id_front) {

            $extension  = $request->emirates_id_front->getClientOriginalExtension();

            $emirates_id_front = 'emirates_id_front_' . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;

            $file_path = $request->emirates_id_front->storeAs('public/players/'.$folder_name, $emirates_id_front);

            $file_path = substr($file_path, 7);

            $player->emirates_id_front = $file_path;

        }



        if($request->emirates_id_back) {

            $extension  = $request->emirates_id_back->getClientOriginalExtension();

            $emirates_id_back = 'emirates_id_back_' . mt_rand(11, 99) . mt_rand(11, 99). '.' . $extension;

            $file_path = $request->emirates_id_back->storeAs('public/players/'.$folder_name, $emirates_id_back);

            $file_path = substr($file_path, 7);

            $player->emirates_id_back = $file_path;

        }

        $player->save();
        
        return redirect()->route('account.players.index')->with('success', 'Player has been updated successfully.');

        }
    }

    public function updatePlayerAdmin(Request $request, Player $player)
    {
       $this->validatePlayer($request);
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
        $data=[];
        $dob = Carbon::createFromFormat('Y-m-d',$request->dob);
        $gender = $request->gender;
        
        $category_id=$this->ageCategoryValidate($dob,$gender);
        if(empty($category_id)){
         return redirect()->route('admin.players.edit',[$player->id])->with('error','Category Not Found According to DOB');
        }
        
        else{

         if(!empty($player->admin_comments)){
         $comments=json_decode($player->admin_comments,true);
         foreach($comments as $key=>$commentAdmin) {
            $data[]=['comments'=>$commentAdmin['comments'],'admin_name'=>$commentAdmin['admin_name'],'posted_on'=>$commentAdmin['posted_on'],'id'=>$commentAdmin['id']];
         }
        }
        if(!empty($request->admin_comment)){
        $data[]=['comments'=>$request->admin_comment,'admin_name'=>Auth::user()->name,'posted_on'=>date('Y-m-d'),'id'=>$player->id
        ]; 
        }	
       
        $player->admin_category_id=$request->admin_category_id;
        $player->category_id=$category_id;
        $player->name = $request->name;
        $player->dob = $dob;
        $player->gender = $gender;
        $player->nationality = $request->nationality;
        $player->previous_football_academy = $request->previous_football_academy;
        $player->admin_comments = $request->admin_comments;
        $player->is_active = $request->is_active !== null ? 1 : 0;
        $player->is_hpc_player = $request->is_hpc_player !== null ? 1 : 0;
        $player->is_goal_keeper = $request->is_goal_keeper !== null ? 1 : 0;
        $player->admin_comments = json_encode($data);
        $player->save();
        

        return redirect()->route($type.'players.index')->with('success', 'Player has been updated successfully.');
    
        

        } 
    }



    /**

     * Remove the specified resource from storage.

     *

     * @param  \App\Player  $player

     * @return \Illuminate\Http\Response

     */

    public function destroy(Player $player)

    {
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
        
       if($player->trainingSessions()->exists()){
         $player->trainingSessions()->update(['player_training_session.deleted_at' => Carbon::now()]);  
        }
        if($player->terms()->exists()){
         $player->terms()->update(['player_term.deleted_at' => Carbon::now()]);
        }
        if($player->invoices()->exists()){
             $player->invoices()->delete();
        }
        if($player->discounts()->exists()){
           $player->discounts()->update(['player_discount.deleted_at' => Carbon::now()]);  
        }
        if($player->evaluations()->exists()){
             $player->evaluations()->delete();
        }
        $player->delete();
        
        
       
        return redirect()->route($type.'players.index')->with('success', 'Player has been Deleted successfully.');
         
      }

    public function playerTermEdit($playerId,$id)

    {

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

       $players=Player::find($playerId);

       $term= $players->terms()->wherePivot('id',$id)->first();
       $terms= Term::where('is_show_to_admin',STATUS_ENABLED)->pluck('name','id');

       $emirates=Emirate::where('is_active', STATUS_ENABLED)->pluck('name','id');;

       $kit_size=[1=>'X-Small- Youth Size',2=> 'Small- Youth Size',3=>'Medium- Youth Size',

       4=>'Large- Youth Size',5=>'Small- Men\s Size',6=>'Medium- Men\s Size ',

       7=>'Large- Men\s Size',8=>'X-Large - Youth Size'];

    

       $kit_status=[1=>'No Update',2=> 'Request For Kit',3=>'Kit Received'];

     

       $payment_status=[1=>'Not Paid',2=> 'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup'];


       $teams=Team::where('is_active',STATUS_ENABLED)->pluck('name','id');

      
         return view($type.'player_terms.edit',['players'=>$players,'term'=>$term,

        'kit_size'=>$kit_size,'kit_status'=>$kit_status,'payment_status'=>$payment_status,

        'emirates'=>$emirates,'id'=>$id,'terms'=>$terms,'teams'=>$teams]);
     
      


     

    }



    public function playerTermUpdate($player,$id,Request $request)

    {

       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

       $kit_status=$request->kit_status;

       $kit_size=$request->kit_size;

       $is_sponsored=$request->is_sponsored;

       $match_kit=$request->match_kit;

       $payment_status=$request->payment_status;

       $emirate_id=$request->emirate_id;
       $team_id=$request->team_id;
       $term_id=$request->term_id;

       $player=Player::find($player);

         $player->terms()->wherePivot('id', '=', $id)->update([

        'kit_status'=>$kit_status,'is_sponsored'=>$is_sponsored,
        'term_id'=>$term_id,'emirate_id'=>$emirate_id,'payment_status' =>$payment_status,'match_kit'=>$match_kit,'kit_size'=>$kit_size,'team_id'=>$team_id]);

       return redirect()->route($type.'players.term',[$player->id])->with('success', 'Player Term has been Updated successfully.');

    }



    public function accountIndex()

    {

        $players = User::getAuthenticatedUser()->players()->get();



        return view('account.players.index', compact('players'));

    }



    public function playersSearch(Request $request)

    {
       if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }
        
        $terms=Term::where('is_show_to_admin',STATUS_ENABLED)->get();
        $teams=Team::where('is_active',STATUS_ENABLED)->get();
        $days=Day::where('is_active',STATUS_ENABLED)->get();
        $categories=AgeCategory::where('is_active',STATUS_ENABLED)->get();
        $coaches=User::where('type',USER_TYPE_COACH)->get();
        
       
       
        $keyword = $request->keyword;
        $player_name = $request->player_name;
        $player_email = $request->player_email;
        $nationality = $request->nationality;
        $team_id = $request->team_id;
        $parent_no = $request->parent_no;
        $player_mobile = $request->player_mobile;
        $mobile = $request->player_mobile;
        $gender= $request->gender;
        $category_id = $request->category_id;
        $player_no = $request->player_no;
        $admin_category_id = $request->admin_category_id;
        $is_hpc_player = $request->is_hpc_player;
        $is_goal_keeper = $request->is_goal_keeper;
        $coach_id = $request->coach_id;
        $day_id = $request->day_id;
        $term_id = $request->term_id;
        $register_from=$request->register_from;
        $register_to=$request->register_to;
        $coach_id=$request->coach_id;

        $search_query = Player::where('is_active', '=', STATUS_ENABLED);

        // if(!empty($register_to) && !empty($register_from)){
         
        //  $register_from= Carbon::createFromFormat('m/d/Y', $register_from)->format('Y-m-d');
        //  $register_to = Carbon::createFromFormat('m/d/Y',$register_to)->format('Y-m-d');
        //  $search_query->where('created_at','>=',$register_from)
        //  ->where('created_at','<=',$register_to);
        // }

        if (!empty($register_to) && !empty($register_from)) {            
            $register_from = Carbon::createFromFormat('m/d/Y', $register_from)->format('Y-m-d');
            $register_to = Carbon::createFromFormat('m/d/Y', $register_to)->format('Y-m-d');
            $search_query->where('created_at', '>=', $register_from)->where('created_at', '<=', $register_to);
        }

    //     if(!empty($coach_id) && !empty($coach_id)){
         
    //     $search_query->whereHas('teams',function($search_query) use ($term_id) {
    //     $search_query->where('coach_teams.user_id',$coach_id);
    //     });
    // }
       
       if(!empty($term_id)){
        $search_query->whereHas('terms',function($search_query) use ($term_id) {
        $search_query->where('player_term.term_id',$term_id);
    
       });
       }
        if(!empty($team_id)){
        $search_query->whereHas('terms',function($search_query) use ($team_id) {
            
              $search_query->where('player_term.team_id',$team_id);
        });
    }
      if(!empty($day_id)){
       $search_query->whereHas('trainingSessions',function($search_query) use ($day_id) {
             $search_query->where('player_training_session.day_id',$day_id);
        });
      }
        if (!empty($keyword)) {
        // $search_query = $search_query->where('name', 'LIKE', '%'.$keyword.'%');
        $search_query = $search_query->where(function ($query) use ($keyword) {
            $query->where('name', 'LIKE', '%'.$keyword.'%')
                ->orWhere('email', 'LIKE', '%'.$keyword.'%')
                ->orWhere('mobile', 'LIKE', '%'.$keyword.'%')
                ->orWhere('nationality', 'LIKE', '%'.$keyword.'%');
        });


        }
        if (!empty($player_name)) {
        $search_query = $search_query->where('name', 'LIKE', '%'.$player_name.'%');
        }
        if(!empty($player_email)) {
        $search_query = $search_query->where('email',$player_email);
        }
        if(!empty($nationality)) {
        $search_query = $search_query->whereHas('user', function($q) use($nationality){
          $q->where('nationality',$nationality);
         });
        }
         if(!empty($team_id)) {
        $search_query = $search_query->whereHas('terms', function($q) use($team_id){
          $q->where('player_term.team_id',$team_id);
         });
        }
         if(!empty($parent_no)) {
        $search_query = $search_query->where('user_id',$parent_no);
        }
         if(!empty($mobile)) {
        $search_query = $search_query->where('mobile',$mobile);
        }
         if(!empty($gender)) {
        $search_query = $search_query->where('gender',$gender);
        }
         if(!empty($category_id)) {
        $search_query = $search_query->where('category_id',$category_id);
        }
         if(!empty($admin_category_id)) {
        $search_query = $search_query->where('admin_category_id',$admin_category_id);
        }

        if(!empty($is_hpc_player)) {
        $search_query = $search_query->where('is_hpc_player',$is_hpc_player);
        }
         if(!empty($is_goal_keeper)) {
        $search_query = $search_query->where('is_goal_keeper',$is_goal_keeper);
        }
        
        $players = $search_query->paginate(15);
         return view($type.'players.index', ['players'=>$players,
            'coaches'=>$coaches,'teams'=>$teams,'categories'=>$categories,
        'is_goal_keeper'=>$is_goal_keeper,'is_hpc_player'=>$is_hpc_player,
        'admin_category_id'=>$admin_category_id,'category_id'=>$category_id,
        'gender'=>$gender,'mobile'=>$mobile,'parent_no'=>$parent_no,'team_id'=>$team_id,
        'nationality'=>$nationality,'player_email'=>$player_email,'player_name'=>$player_name,'keyword'=>$keyword,'terms'=>$terms,'term_id'=>$term_id,'days'=>$days,'day_id'=>$day_id,'register_from'=>$register_from,'register_to'=>$register_to

        ]);

    }



    public function ageCategoryValidate($dob, $gender) {
     if(date('Y-m-d', strtotime($dob)) > '2009-01-01'){
     $ageCategory = AgeCategory::where('start_date','<=',date('Y-m-d', strtotime($dob)))->where('end_date','>=', date('Y-m-d', strtotime($dob)))->
        where('is_active',STATUS_ENABLED)->pluck('id')->first();
    }
    else{
        $ageCategory = AgeCategory::where('start_date','<=',date('Y-m-d', strtotime($dob)))->where('end_date','>=', date('Y-m-d', strtotime($dob)))->where('gender',$gender)->where('is_active',STATUS_ENABLED)->pluck('id')->first();
    }
    
       return $ageCategory;

    }



     public function playerTerm($player) {
     
     if(Auth::user()->type==USER_TYPE_MANAGER){
        $type="manager.";
       }
       elseif(Auth::user()->type==USER_TYPE_ADMIN){
        $type="admin.";
       }

     $player=Player::find($player);

     $terms = $player->terms()->wherePivot('is_active',STATUS_ENABLED)->get();

    $kit_size=[1=>'X-Small- Youth Size',2=> 'Small- Youth Size',3=>'Medium- Youth Size',

     4=>'Large- Youth Size',5=>'Small- Men\s Size',6=>'Medium- Men\s Size ',

     7=>'Large- Men\s Size',8=>'X-Large - Youth Size'];

    $kit_status=[1=>'No Update',2=> 'Request For Kit',3=>'Kit Received'];

     

     $payment_status=[1=>'Not Paid',2=> 'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup'];

     return view($type.'player_terms.index',['player'=>$player,'terms'=>$terms,'no'=>1,

        'kit_size'=>$kit_size,'kit_status'=>$kit_status,'payment_status'=>$payment_status]);

       

    }

   public function getPlayerCategory(Request $request)
  {
     $dob=$request->date;
     $gender = $request->gender;
     $categoryId=$this->ageCategoryValidate($dob,$gender);
     $categoryName=AgeCategory::where('id',$categoryId)->pluck('name')->first();
     return json_encode(array('dataResult'=>$categoryName));
     
     
  }

  public function playerTermCreate(Player $player)
  {
     $terms= Term::where('is_show_to_admin',STATUS_ENABLED)->pluck('name','id');
     $emirates=Emirate::where('is_active', STATUS_ENABLED)->pluck('name','id');
     $kit_size=[1=>'X-Small- Youth Size',2=> 'Small- Youth Size',3=>'Medium- Youth Size',

     4=>'Large- Youth Size',5=>'Small- Men\s Size',6=>'Medium- Men\s Size ',

     7=>'Large- Men\s Size',8=>'X-Large - Youth Size'];
      $kit_status=[1=>'No Update',2=> 'Request For Kit',3=>'Kit Received'];

     $payment_status=[1=>'Not Paid',2=> 'Partial Payment',3=>'Paid',4=>'Ready to Paid',5=> 'Invoice Accepted ',6=>'Refunded',7=>'Partial Payment Pending',8=> 'Payment Defaulted ',9=>'Sponsored',10=>'Makeup'];

      $teams=Team::where('is_active',STATUS_ENABLED)->pluck('name','id');

      if(Auth::user()->type==USER_TYPE_MANAGER){
       return view('manager.player_terms.create',['player'=>$player,'terms'=>$terms,'kit_size'=>$kit_size,'emirates'=>$emirates,'teams'=>$teams,'kit_status'=>$kit_status,'payment_status'=>$payment_status]);
         }
         elseif(Auth::user()->type==USER_TYPE_ADMIN){
        return view('admin.player_terms.create',['player'=>$player,'terms'=>$terms,'kit_size'=>$kit_size,'emirates'=>$emirates,'teams'=>$teams,'kit_status'=>$kit_status,'payment_status'=>$payment_status]);
      }
     
     
     
  }

  public function storePlayerTerm(Request $request,Player $player)

    {

       $kit_status=$request->kit_status;

       $kit_size=$request->kit_size;

       $is_sponsored=$request->is_sponsored;

       $match_kit=$request->match_kit;

       //$payment_status=$request->payment_status;
       $payment_status=STATUS_NOT_PAID;
       $emirate_id=$request->emirate_id;
       $team_id=$request->team_id;
       $term_id=$request->term_id;

     $hasTerm = $player->terms()->wherePivot('term_id',$term_id)
     ->wherePivot('deleted_at','=',null)->first();

     if(!empty($hasTerm) > 0){
     if(Auth::user()->type==USER_TYPE_MANAGER){
         return redirect()->route('manager.players.term',[$player->id])->with('error','This Term is already added for this player');
         }
         elseif(Auth::user()->type==USER_TYPE_ADMIN){
          return redirect()->route('admin.players.term',[$player->id])->with('error','This Term is already added for this player');
      }
  
    }
    else
    {
     $player->terms()->attach($term_id,[

        'kit_status'=>$kit_status,'is_sponsored'=>$is_sponsored,
        'term_id'=>$term_id,'emirate_id'=>$emirate_id,'payment_status' =>$payment_status,'match_kit'=>$match_kit,'kit_size'=>$kit_size,'team_id'=>$team_id]);
    }

    if(Auth::user()->type==USER_TYPE_MANAGER){
       return redirect()->route('manager.players.term',[$player->id])->with('success', 'Player Term has been Updated successfully.');
         }
        elseif(Auth::user()->type==USER_TYPE_ADMIN){
        return redirect()->route('admin.players.term',[$player->id])->with('success', 'Player Term has been Updated successfully.');
      }
    

    }


    private function validatePlayer(Request $request) {

       $request->validate([

            'name' => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
            'gender' => ['required'],
            'dob' => ['required'],
            'category_id' => ['required'],

            ]);

    }






}

