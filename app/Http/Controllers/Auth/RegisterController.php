<?php



namespace App\Http\Controllers\Auth;



use App\Http\Controllers\Controller;

use App\Providers\RouteServiceProvider;

use App\User;

use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Validator;



class RegisterController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Register Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users as well as their

    | validation and creation. By default this controller uses a trait to

    | provide this functionality without requiring any additional code.

    |

    */



    use RegistersUsers;



    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = 'account/players/create';



    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest');

    }



    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */

    protected function validator(array $data)

    {

       return Validator::make($data, [

            'name' => ['required','max:50','regex:/^[\pL\s\-]+$/u'],
            'parent_1_relationship' => ['required','max:50'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'parent_2_email' => ['nullable','string', 'email'],
            'mobile' => ['required','digits:10'],
            'parent_2_mobile' => ['nullable','digits:10'],
            'password' => ['required','confirmed'],
            'nationality' => ['required'],
            'liga_tnc' => ['required'],
            'hear_about_us' => ['required'],
             'g-recaptcha-response'=>'required'
           
            

        ]);

    }



    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return \App\User

     */

    protected function create(array $data)

    {
       
        return User::create([

            'name' => $data['name'],

            'email' => $data['email'],

            'password' => Hash::make($data['password']),

            'parent_1_relationship'=>$data['parent_1_relationship'],

            'parent_2_email' => $data['parent_2_email'],

            'mobile' => $data['mobile'],

            'parent_2_mobile' => $data['parent_2_mobile'],

            'nationality' => $data['nationality'],

            'hear_about_us' => $data['hear_about_us'],

        ]);

    }

}

