<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Player;
use App\Invoice;
use App\DailyReport;
use Carbon\Carbon;


use Auth;

class DailyReportController extends Controller
{
    public function index()
    {
        $current_date=date('Y-m-d');
        $daily_report=DailyReport::where('date',$current_date)->first();
        $total_player=Player::count();
        $total_parent=User::where('type',USER_TYPE_PARENT)->count();
        $total_partial_paid=Invoice::where('payment_status',STATUS_PARTIAL_PAYMENT)->count();
        $total_paid=Invoice::where('payment_status',STATUS_PAID)->count();
        $total_refund=Invoice::where('payment_status',STATUS_REFUNDED)->count();
        $reports=DailyReport::paginate(15);
        
        if(empty($daily_report)){
        
        $daily_report=new DailyReport();
        $daily_report->date=$current_date;
        $daily_report->total_parent=$total_parent;
        $daily_report->total_player=$total_player;
        $daily_report->total_paid=$total_paid;
        $daily_report->total_refund=$total_refund;
        $daily_report->save();
        }
        elseif($current_date==$daily_report->date){
        $daily_report->date=$current_date;
        $daily_report->total_parent=$total_parent;
        $daily_report->total_player=$total_player;
        $daily_report->total_paid=$total_paid;
        $daily_report->total_refund=$total_refund;
        $daily_report->save();

        }
    	
        return view('admin.daily_report.index',['reports'=>$reports,'no'=>1]);
    }


    public function reportSearch(Request $request){
     
     $payment_from=$request->payment_date_from;
     $payment_to=$request->payment_date_to;
     
     $payment_date_from= Carbon::createFromFormat('m/d/Y', $request->payment_date_from)->format('Y-m-d');
     $payment_date_to= Carbon::createFromFormat('m/d/Y', $request->payment_date_to)->format('Y-m-d');
     $reports=DailyReport::where('date','>=',$payment_date_from)
         ->where('date','<=',$payment_date_to,)->paginate(15);

     return view('admin.daily_report.index',['reports'=>$reports,'payment_from'=>$payment_from,'payment_to'=>$payment_to]);

}

}
