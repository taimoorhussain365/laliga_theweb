<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Invoice;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Utilities\Payfort;

class PaymentsController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function checkout(Invoice $invoice)
    {

        // Save Payment Details in DB
        $payment = new Payment;
        $payment->invoice_id = $invoice->id;
        $payment->amount = $invoice->total_amount;
        $payment->user_id = Auth::user()->id;
        $payment->save();


       // Setting variables
       session([
           'payment_id' => $payment->id,
           'user_email' => Auth::user()->email,
           'user_name' => Auth::user()->name,
           'total_amount' => $invoice->total_amount,
       ]);

        // $payment_id = $payment->id;

        return view('payments.checkout',['invoice'=>$invoice]);
    }

    public function redirectRoutes(Request $request,$action){
      $paymentMethod='cc_merchantpage2';
       
        if(!isset($action)) {
            echo 'Page Not Found!';
            exit;
        }

        $objFort = new Payfort();
        if($action == 'getPaymentPage') {

            $objFort->processRequest(htmlspecialchars($paymentMethod, ENT_QUOTES, 'UTF-8'));
           
        }
        elseif($action == 'merchantPageReturn') {
            $objFort->processMerchantPageResponse();
        }
        elseif($action == 'processResponse') {
            $objFort->processResponse();
        }
        else{
            echo 'Page Not Found!';
            exit;
        }

    }

    public function success(Request $request)
    {
//        dd($request->all());
        $payment = Payment::find($this->getPaymentIdFromMerchant($request['merchant_extra']));
        $course = null;
        if($payment) {
            $course = $payment->course;
            if(empty($payment->transaction_reference)){
                $payment->transaction_details = json_encode(request()->all());
                $payment->status = 1;
                $payment->transaction_reference = json_encode($request['fort_id']);
                $payment->save();
            }
        }
        $session_id = session('session_id');
        return view('payments.success', ['response' => $request->all(), 'course' => $course, 'session_id' => $session_id]);
    }

    public function failure(Request $request)
    {
//        dd($request->all());
        $payment = Payment::find($this->getPaymentIdFromMerchant($request['merchant_extra']));
        $course = null;
        if($payment) {
            $course = $payment->course;
            if(empty($payment->transaction_reference)){
                $payment->transaction_details = json_encode(request()->all());
                $payment->save();
            }
        }
        $session_id = session('session_id');
        return view('payments.failure', ['response' => $request->all(), 'course' => $course, 'session_id' => $session_id]);
    }

    protected function getPaymentIdFromMerchant($merchant_extra)
    {
        return (int)substr($merchant_extra, 4);
    }
}
