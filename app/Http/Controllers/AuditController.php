<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Audit;
class AuditController extends Controller
{
    
    public function index()
    {
       
      $audits=Audit::all();
      return view('admin.audit.index',['audits'=>$audits]);
        
    }
   
}


