<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
//            return redirect(RouteServiceProvider::HOME);
            // if(Auth::user()->type == USER_TYPE_ADMIN){
            if(Auth::user()->isadmin == 1){
                $dashboard_route = 'admin.dashboard';
            } 
            elseif(Auth::user()->type == USER_TYPE_PARENT){
                $dashboard_route = 'account.dashboard';
            }
            elseif(Auth::user()->type == USER_TYPE_COACH){
                $dashboard_route = 'coach.dashboard';           
            } 

            return redirect()->route($dashboard_route);
        }

        return $next($request);
    }
}
