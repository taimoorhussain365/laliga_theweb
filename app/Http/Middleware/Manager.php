<?php

namespace App\Http\Middleware;
use Auth;
use Closure;

class Manager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->type==USER_TYPE_MANAGER){

            return $next($request);

        }

        return $next($request);
    }
}
