<?php



namespace App;



use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;



class TrainingSession extends Model

{

 use SoftDeletes;

 protected $dates = ['deleted_at'];   

 

 public function getDayLiteralAttribute($id)

    {

        

        switch ($id) {

            case '0':

                $day = "Sunday";

                break;

            case '1':

                $day = "Monday";

                break;    

            case '2':

                $day = "Tuesday";

                break;

            case '3':

                $day = "Wednesday";

                break;

            case '4':

                $day = "Thursday";

                break;

            case '5':

                $day = "Friday";

                break;

            case '6':

                $day = "Saturday";

                break;

            default:

                break;

        }

        return $day;

    }

   public static function getStartTimeFormattedAttribute($start_time)

    {

        return Carbon::parse($start_time)->format('H:i');

    }



    public static function getEndTimeFormattedAttribute($end_time)

    {

        return Carbon::parse($end_time)->format('H:i');

    }



    public function term()

    {

        return $this->belongsTo('App\Term');

    } 



    public function locations()

    {

        return $this->belongsTo('App\Location','location_id');

    }



     public function day()

    {

        return $this->belongsTo('App\Day');

    }



     public function category()

    {

        return $this->belongsTo('App\AgeCategory');

    }

    

    public function players()

    {

        return $this->belongsToMany('App\Player')->withTimestamps()->withPivot(['location_id','day_id','serial_no','weeks','is_full_week']);

    }

    public function getEmirate($id)

    {

      return Emirate::where('id',$id)->pluck('name')->first();   

    }



    public function getLocation($id)

    {

      return Location::where('id',$id)->pluck('name')->first();   

    }

}

