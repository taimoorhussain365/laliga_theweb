<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TermBreak extends Model
{
	 use SoftDeletes;

  protected $dates = ['deleted_at'];

    public function term()

    {

       return $this->belongsTo('App\Term');
    }
}
